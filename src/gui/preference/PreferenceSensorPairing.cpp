/* セクタ数 */
#include "PreferenceSensorPairing.h"
#include "DataMem.h"
#include "PageObj.h"

PreferenceSensorPairing Pref_SensorPairing("SensorPairing", &page_set_sensmac);

/* 初期化 */
void PreferenceSensorPairing::init(void){
    PreferenceMenuItem::init();

    DataMem::read(MEM_BT0, &mac_adrs[0]);
    DataMem::read(MEM_BT1, &mac_adrs[1]);
    DataMem::read(MEM_BT2, &mac_adrs[2]);
    DataMem::read(MEM_BT3, &mac_adrs[3]);
    DataMem::read(MEM_BT4, &mac_adrs[4]);
    DataMem::read(MEM_BT5, &mac_adrs[5]);
}

/* 周期実行 */
bool PreferenceSensorPairing::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[30];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        sprintf(c_tmp, "%02X:%02X:%02X:%02X:%02X:%02X", mac_adrs[0], mac_adrs[1], mac_adrs[2], mac_adrs[3], mac_adrs[4], mac_adrs[5]);

        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceSensorPairing::select(void){
    return NEXT_PAGE;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceSensorPairing::cancel(void){
    return NORMAL;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceSensorPairing::valNext(void){
    return NORMAL;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceSensorPairing::valPrev(void){
    return NORMAL;
}


