/* セクタ数 */
#include "PreferenceEngPulseDiv.h"
#include "DataMem.h"
#include "PageObj.h"
#include "EngineCAN.h"

PreferenceEngPulseDiv Pref_EngPulseDiv("EngPulseDiv", nullptr);

/* 初期化 */
void PreferenceEngPulseDiv::init(void){
    PreferenceMenuItem::init();

    DataMem::read(ENG_PULSE_DIV, &pulse_div);
}

/* 周期実行 */
bool PreferenceEngPulseDiv::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[30];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        sprintf(c_tmp, "%d", pulse_div);

        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceEngPulseDiv::select(void){
    
    ++pulse_div;
    if(pulse_div > eng_pulse_div.Max){
        pulse_div = eng_pulse_div.Min;
    }

    DataMem::write(ENG_PULSE_DIV, pulse_div);
    update = true;
    
    EngineCAN::init();

    return NORMAL;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceEngPulseDiv::cancel(void){
    return NORMAL;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceEngPulseDiv::valNext(void){
    return NORMAL;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceEngPulseDiv::valPrev(void){
    return NORMAL;
}


