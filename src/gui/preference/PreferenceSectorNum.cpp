/* NTP時刻設定 */
#include "PreferenceSectorNum.h"
#include "DataMem.h"

PreferenceSectorNum Pref_SectorNum("SectorNum", nullptr);

/* 初期化 */
void PreferenceSectorNum::init(void){
    PreferenceMenuItem::init();

    DataMem::read(MEM_SEC, &sector_num);
}

/* 周期実行 */
bool PreferenceSectorNum::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[10];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        if(SETTING == state){
            sprintf(c_tmp, "[%3d]", sector_num);
        }else{
            sprintf(c_tmp, " %3d ", sector_num);
        }
        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceSectorNum::select(void){
    if(SETTING != state){
        state = SETTING;    /* 設定変更状態へ */

        /* 記録しているセクタ数設定読み出し */
        DataMem::read(MEM_SEC, &sector_num);

    }else if(SETTING == state){
        state = NORMAL;     /* 値を確定して戻る */

        /* 確定値書き込み */
        DataMem::write(MEM_SEC, sector_num);
    }

    update = true;
    return state;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceSectorNum::cancel(void){
    state = NORMAL;     /* 値を戻して戻る */
    return state;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceSectorNum::valNext(void){
    if(SETTING == state){
        if(mem_sec.Max > sector_num){
            ++sector_num;
        }else{
            sector_num = mem_sec.Min;
        }
        update = true;
    }
    return state;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceSectorNum::valPrev(void){
    if(SETTING == state){
        if(mem_sec.Min < sector_num){
            --sector_num;
        }else{
            sector_num = mem_sec.Max;
        }
        update = true;
    }
    return state;
}


