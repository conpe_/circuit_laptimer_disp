/* ドリブンスプロケ */
#include "PreferenceGearDriven.h"
#include "DataMem.h"

PreferenceGearDriven Pref_GearDriven("DrivenGear", nullptr);

/* 初期化 */
void PreferenceGearDriven::init(void){
    PreferenceMenuItem::init();

    DataMem::read(GEAR_T_DRIVEN, &val);
}

/* 周期実行 */
bool PreferenceGearDriven::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[10];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        if(SETTING == state){
            sprintf(c_tmp, "[%3dT]", val);
        }else{
            sprintf(c_tmp, " %3dT ", val);
        }
        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceGearDriven::select(void){
    if(SETTING != state){
        state = SETTING;    /* 設定変更状態へ */

        /* 記録しているセクタ数設定読み出し */
        DataMem::read(GEAR_T_DRIVEN, &val);

    }else if(SETTING == state){
        state = NORMAL;     /* 値を確定して戻る */

        /* 確定値書き込み */
        DataMem::write(GEAR_T_DRIVEN, val);
    }

    update = true;
    return state;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceGearDriven::cancel(void){
    state = NORMAL;     /* 値を戻して戻る */
    
    if(SETTING == state){
        /* 記録しているセクタ数設定読み出し */
        DataMem::read(GEAR_T_DRIVEN, &val);
        update = true;
    }

    return state;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceGearDriven::valNext(void){
    if(SETTING == state){
        if(gear_t_driven.Max > val){
            ++val;
        }else{
            val = gear_t_driven.Min;
        }
        update = true;
    }
    return state;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceGearDriven::valPrev(void){
    if(SETTING == state){
        if(gear_t_driven.Min < val){
            --val;
        }else{
            val = gear_t_driven.Max;
        }
        update = true;
    }
    return state;
}


