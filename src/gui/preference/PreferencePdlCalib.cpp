/* ペダルのキャリブ */
#include "PreferencePdlCalib.h"
#include "mdlCan.h"
#include "mdlCanDataConfig_B0.h"
extern mdlCan *mdl_can;

PreferencePdlCalib Pref_PdlCalib("Pedal calibration", nullptr);

/* 初期化 */
void PreferencePdlCalib::init(void){
    PreferenceMenuItem::init();

    sta = 0x00;
    target = 0x00;
    sta_time = millis();
}

/* 周期実行 */
bool PreferencePdlCalib::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[30];
    uint8_t reel_status;

    *draw_canvas = &canvas;


    switch(sta){
    case 0x00:  /* idle */
        break;
    case 0x01:  /* start */
        
        sta_time = millis();
        sta = 0x02;

        /* キャリブコマンドを送信 */
        if(0x01==target){   /* アクセル */
            CanMsg_B0_Tx[8].dlc = 2;
            stMdlCan_B0_TxVal.CommandPdlAcc[0] = 0x03;  /* キャリブコマンド */
            stMdlCan_B0_TxVal.CommandPdlAcc[1] = 0x00;  /* データ長 */
            mdl_can->send(&CanMsg_B0_Tx[8]);
        }else if(0x02==target){   /* ブレーキ */
            CanMsg_B0_Tx[9].dlc = 2;
            stMdlCan_B0_TxVal.CommandPdlBrk[0] = 0x03;  /* キャリブコマンド */
            stMdlCan_B0_TxVal.CommandPdlBrk[1] = 0x00;  /* データ長 */
            mdl_can->send(&CanMsg_B0_Tx[9]);
        }else if(0x03==target){   /* ステアリング */
            CanMsg_B0_Tx[10].dlc = 2;
            stMdlCan_B0_TxVal.CommandStr[0] = 0x03;  /* キャリブコマンド */
            stMdlCan_B0_TxVal.CommandStr[1] = 0x00;  /* データ長 */
            mdl_can->send(&CanMsg_B0_Tx[10]);
        }else{
            sta = 0xFD;
        }

        break;
    case 0x02:  /* キャリブ中 */
        if(sta_time+200 < millis()){    /* CAN受信遅れを考慮してちょっと遅れて判定開始 */
            if(0x01==target){   /* アクセル */
                reel_status = stMdlCan_B0_RxVal.PdlAccCalibSta;
            }else if(0x02==target){   /* ブレーキ */
                reel_status = stMdlCan_B0_RxVal.PdlBrkCalibSta;
            }else if(0x03==target){   /* ステアリング */
                reel_status = stMdlCan_B0_RxVal.StrCalibSta;
            }

            if((0x01 != reel_status) && (sta_time+300 > millis())){
                sta = 0xFD;     /* キャリブ失敗 */
                state = NORMAL; /* 戻る */
            }else if(0xFF == reel_status){
                sta = 0xFE;     /* キャリブ完了 */
                state = NORMAL; /* 戻る */
            }
            
        }else if(sta_time+10000 < millis()){
            /* タイムアウト */
            sta = 0xFD;
            state = NORMAL; /* 戻る */
        }

        break;
    case 0xFD:  /* キャリブ失敗 */
    case 0xFE:  /* キャリブ終了 */
    default:
        break;
    }

    if(sta_last != sta){
        update = true;
        sta_last = sta;
    }

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());


        switch(sta){
        case 0x00:  /* idle */
            if(SETTING == state){
                /* キャリブ開始する対象を選択する */
                switch(target){
                case 0x00:  /* 戻る */
                    sprintf(c_tmp, "[cancel]");
                    break;
                case 0x01:  /* アクセル */
                    sprintf(c_tmp, "[Accel]");
                    break;
                case 0x02:  /* ブレーキ */
                    sprintf(c_tmp, "[Brake]");
                    break;
                case 0x03:  /* ステアリング */
                    sprintf(c_tmp, "[Steer]");
                    break;
                }
            }else{
                sprintf(c_tmp, "target...");
            }
            break;
        case 0x02:  /* キャリブ開始した */
            switch(target){
            case 0x01:  /* アクセル */
                sprintf(c_tmp, "...Calib Acc");
                break;
            case 0x02:  /* ブレーキ */
                sprintf(c_tmp, "...Calib Brk");
                break;
            case 0x03:  /* ステアリング */
                sprintf(c_tmp, "...Calib Str");
                break;
            }
            break;
        case 0xFD:  /* キャリブ失敗 */
            sprintf(c_tmp, "Faild");
            break;
        case 0xFE:  /* キャリブ終了 */
            sprintf(c_tmp, "Done");
            break;
        default:
            sprintf(c_tmp, "err");
            break;
        }

        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }

}

/* 決定 */
PreferenceMenuItem::State PreferencePdlCalib::select(void){
    if(0xF0 <= sta){    /* なにかボタン押したらキャリブ終了→idle表示へ */
        sta = 0x00;
        update = true;
    }


    if(SETTING == state){
        /* キャリブ開始 */
        if(0x00 == target){
            state = NORMAL;
        }else{
            if(0x00 == sta){
                sta = 0x01;
            }
        }
    }else{
        state = SETTING;
    }

    update = true;
    return state;
}
/* キャンセル */
PreferenceMenuItem::State PreferencePdlCalib::cancel(void){
    if(0xF0 <= sta){    /* なにかボタン押したらキャリブ終了→idle表示へ */
        sta = 0x00;
        update = true;
    }

    if(state = SETTING){
        state = NORMAL;
    }

    return state;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferencePdlCalib::valNext(void){
    if(0xF0 <= sta){    /* なにかボタン押したらキャリブ終了→idle表示へ */
        sta = 0x00;
        update = true;
    }

    if(SETTING == state){
        if(0x00 == sta){    /* 測定中でない */
            ++target;
            if(3 < target){
                target = 0;
            }

            update = true;
        }
    }

    return state;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferencePdlCalib::valPrev(void){
    if(0xF0 <= sta){    /* なにかボタン押したらキャリブ終了→idle表示へ */
        sta = 0x00;
        update = true;
    }

    if(SETTING == state){
        if(0x00 == sta){    /* 測定中でない */
            if(0 >= target){
                target = 3;
            }else{
                --target;
            }
            update = true;
        }
    }

    return state;
}


