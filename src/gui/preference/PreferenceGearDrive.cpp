/* ドライブスプロケ */
#include "PreferenceGearDrive.h"
#include "DataMem.h"

PreferenceGearDrive Pref_GearDrive("DriveGear", nullptr);

/* 初期化 */
void PreferenceGearDrive::init(void){
    PreferenceMenuItem::init();

    DataMem::read(GEAR_T_DRIVE, &val);
}

/* 周期実行 */
bool PreferenceGearDrive::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[10];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        if(SETTING == state){
            sprintf(c_tmp, "[%3dT]", val);
        }else{
            sprintf(c_tmp, " %3dT ", val);
        }
        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceGearDrive::select(void){
    if(SETTING != state){
        state = SETTING;    /* 設定変更状態へ */

        /* 記録しているセクタ数設定読み出し */
        DataMem::read(GEAR_T_DRIVE, &val);

    }else if(SETTING == state){
        state = NORMAL;     /* 値を確定して戻る */

        /* 確定値書き込み */
        DataMem::write(GEAR_T_DRIVE, val);
    }

    update = true;
    return state;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceGearDrive::cancel(void){
    state = NORMAL;     /* 値を戻して戻る */
    
    if(SETTING == state){
        /* 記録しているセクタ数設定読み出し */
        DataMem::read(GEAR_T_DRIVE, &val);
        update = true;
    }

    return state;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceGearDrive::valNext(void){
    if(SETTING == state){
        if(gear_t_drive.Max > val){
            ++val;
        }else{
            val = gear_t_drive.Min;
        }
        update = true;
    }
    return state;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceGearDrive::valPrev(void){
    if(SETTING == state){
        if(gear_t_drive.Min < val){
            --val;
        }else{
            val = gear_t_drive.Max;
        }
        update = true;
    }
    return state;
}


