/* 設定メニュー項目テンプレート */

#pragma once

#include "PreferenceMenuItem.h"

/* 設定メニュー項目のテンプレート */
class PreferenceNtp:public PreferenceMenuItem{
public:
    PreferenceNtp(const char* title = "Ntp", Page* n_page = nullptr, uint16_t w=PREF_W, uint16_t h=PREF_H)
            :PreferenceMenuItem(title, n_page, w, h)
    {

    };
    ~PreferenceNtp(){};

    virtual void init(void);                            /* 初期化 */
    virtual bool draw(M5EPD_Canvas** draw_canvas);           /* 周期実行 更新なければfalse */
    virtual PreferenceMenuItem::State select(void);     /* 決定 */
    virtual PreferenceMenuItem::State cancel(void);     /* キャンセル */
    virtual PreferenceMenuItem::State valNext(void);    /* 設定値変更 次 */
    virtual PreferenceMenuItem::State valPrev(void);    /* 設定値変更 前 */

protected:
    std::string ssid;
    uint8_t disp_sta;   /* 0:idle表示, 1:状態表示 */
    uint8_t disp_sta_last;
    uint8_t ntpsta_last;
};

extern PreferenceNtp Pref_Ntp;
