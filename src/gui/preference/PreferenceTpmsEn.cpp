/* セクタ数 */
#include "PreferenceTpmsEn.h"
#include "DataMem.h"
#include "PageObj.h"
#include "Tire.h"

PreferenceTpmsEn Pref_TpmsEn("TpmsEnable", nullptr);

/* 初期化 */
void PreferenceTpmsEn::init(void){
    PreferenceMenuItem::init();

    DataMem::read(TPMS_EN, &enable);
}

/* 周期実行 */
bool PreferenceTpmsEn::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[30];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());
        
        sprintf(c_tmp, "%s", enable?"Enable":"Disable");

        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceTpmsEn::select(void){
    
    ++enable;
    if(enable > 1){
        enable = 0;
    }

    DataMem::write(TPMS_EN, enable);
    update = true;
    
    if(enable){
        Tire::createTireObj();
    }else{
        Tire::deleteTireObj();
    }

    return NORMAL;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceTpmsEn::cancel(void){
    return NORMAL;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceTpmsEn::valNext(void){
    return NORMAL;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceTpmsEn::valPrev(void){
    return NORMAL;
}


