/* 設定メニュー項目テンプレート */

#pragma once

#include "PreferenceMenuItem.h"

/* 設定メニュー項目のテンプレート */
class PreferencePdlCalib:public PreferenceMenuItem{
public:
    PreferencePdlCalib(const char* title = "PdlCalib", Page* n_page = nullptr, uint16_t w=PREF_W, uint16_t h=PREF_H)
            :PreferenceMenuItem(title, n_page, w, h)
    {

    };
    ~PreferencePdlCalib(){};

    virtual void init(void);                            /* 初期化 */
    virtual bool draw(M5EPD_Canvas** draw_canvas);           /* 周期実行 更新なければfalse */
    virtual PreferenceMenuItem::State select(void);     /* 決定 */
    virtual PreferenceMenuItem::State cancel(void);     /* キャンセル */
    virtual PreferenceMenuItem::State valNext(void);    /* 設定値変更 次 */
    virtual PreferenceMenuItem::State valPrev(void);    /* 設定値変更 前 */

protected:
    uint8_t sta;        /* 0x00:キャリブ開始, 0x01:キャリブ中, 0xFE:キャリブ完了,0xFF:idle */
    uint8_t sta_last;
    uint8_t target;     /* 0x00:キャンセル, 0x01:アクセルペダル, 0x02:ブレーキペダル, 0x03:ステアリング */
    uint32_t sta_time;  /* その状態に入った時間 */
};

extern PreferencePdlCalib Pref_PdlCalib;
