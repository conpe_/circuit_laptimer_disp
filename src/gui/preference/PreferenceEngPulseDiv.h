/* 設定メニュー項目 エンジン回転数パルス分周 */

#pragma once

#include "PreferenceMenuItem.h"

class PreferenceEngPulseDiv:public PreferenceMenuItem{
public:
    PreferenceEngPulseDiv(const char* title = "SectorNum", Page* n_page = nullptr, uint16_t w=PREF_W, uint16_t h=PREF_H)
            :PreferenceMenuItem(title, n_page, w, h)
    {

    };
    ~PreferenceEngPulseDiv(){};

    virtual void init(void);                            /* 初期化 */
    virtual bool draw(M5EPD_Canvas** draw_canvas);           /* 周期実行 更新なければfalse */
    virtual PreferenceMenuItem::State select(void);     /* 決定 */
    virtual PreferenceMenuItem::State cancel(void);     /* キャンセル */
    virtual PreferenceMenuItem::State valNext(void);    /* 設定値変更 次 */
    virtual PreferenceMenuItem::State valPrev(void);    /* 設定値変更 前 */

protected:
    uint8_t pulse_div;

};

extern PreferenceEngPulseDiv Pref_EngPulseDiv;
