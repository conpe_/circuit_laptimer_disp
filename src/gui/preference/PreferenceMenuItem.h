/* 設定メニュー項目 */

#pragma once

#include <vector>
#include <string>

#include "Page.h"

#define PREF_W (LCD_MAIN_W-40)
#define PREF_H 60

/* 設定メニュー項目のテンプレート */
class PreferenceMenuItem{
public:
    enum State{
        NORMAL,
        SETTING,
        NEXT_PAGE
    };

    PreferenceMenuItem(const char* title = "PREFERENCES", Page* n_page = nullptr, uint16_t w=PREF_W, uint16_t h=PREF_H)
                                        :canvas(&M5.EPD), next_page(n_page), state(NORMAL)
    {
        this->title = title;
        this->w = w;
        this->h = h;
    };
    ~PreferenceMenuItem(void){};

    virtual void init(void){                /* 初期化 */
        update = true;
        state = NORMAL;
        canvas.createCanvas(w, h);
    };
    virtual bool draw(M5EPD_Canvas** draw_canvas) = 0;   /* 周期実行 更新なければfalse */
    virtual State select(void) = 0;         /* 決定 */
    virtual State cancel(void) = 0;         /* キャンセル */
    virtual State valNext(void) = 0;        /* 設定値変更 次 */
    virtual State valPrev(void) = 0;        /* 設定値変更 前 */

    std::string* prefTitle(void){return &title;};
    Page* nextPage(void){return next_page;};
    State currentState(void){return state;};

protected:
    uint16_t w, h;
    bool update;
    std::string title;
    M5EPD_Canvas_Semaphore canvas;
    Page* next_page;
    State state;

};

