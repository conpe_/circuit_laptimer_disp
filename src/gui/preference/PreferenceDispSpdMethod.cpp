/* セクタ数 */
#include "PreferenceDispSpdMethod.h"
#include "DataMem.h"
#include "PageObj.h"

PreferenceDispSpdMethod Pref_DispSpdMethod("DispSpdMethod", nullptr);

/* 初期化 */
void PreferenceDispSpdMethod::init(void){
    PreferenceMenuItem::init();

    DataMem::read(DISP_SPD_METHOD, &val);
}

/* 周期実行 */
bool PreferenceDispSpdMethod::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[30];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        switch(val){
        case 0:
            sprintf(c_tmp, "GPS+ENG");
            break;
        case 1:
            sprintf(c_tmp, "GPS");
            break;
        default:
            break;
        }

        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceDispSpdMethod::select(void){
    if(val){
        val = 0;
    }else{
        val = 1;
    }

    DataMem::write(DISP_SPD_METHOD, val);
    update = true;
    return NORMAL;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceDispSpdMethod::cancel(void){
    return NORMAL;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceDispSpdMethod::valNext(void){
    return NORMAL;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceDispSpdMethod::valPrev(void){
    return NORMAL;
}


