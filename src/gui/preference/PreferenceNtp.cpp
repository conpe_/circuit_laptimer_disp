/* セクタ数 */
#include "PreferenceNtp.h"
#include "DataMem.h"
#include "TimeAdjust.h"

PreferenceNtp Pref_Ntp("WiFi Time Adjust", nullptr);

/* 初期化 */
void PreferenceNtp::init(void){
    WiFiInfo wifiinfo("/LapTimer.ini");

    PreferenceMenuItem::init();
    //ssid
    if(0==wifiinfo.load()){
        ssid = wifiinfo.ssid;
    }

    disp_sta = 0;
    ntpsta_last = 0xFF;
}

/* 周期実行 */
bool PreferenceNtp::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[30];
    uint8_t ntpsta = TimeAdjust::getStatus();

    *draw_canvas = &canvas;

    if(ntpsta_last != ntpsta){
        /* NTP時刻取得状態が変わった */
        update = true;
        ntpsta_last = ntpsta;
    }

    if(disp_sta_last != disp_sta){
        update = true;
        disp_sta_last = disp_sta;
    }

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        if(0 == disp_sta){  /* 通常表示 */
            sprintf(c_tmp, "%s", ssid.c_str());
        }else{      /* 状態表示 */
            /* 0x01:WiFi接続待ち, 0x02:取得待ち, 0xFE:失敗, 0xFF:idle */
            switch(ntpsta){
            case 0x00:
            case 0x01:
                sprintf(c_tmp, "connecting...");
                break;
            case 0x02:
            case 0x10:
                sprintf(c_tmp, "connecting..");
                break;
            case 0xFE:
                sprintf(c_tmp, "failed");
                break;
            default:
                sprintf(c_tmp, "complete");
                break;
            }

        }

        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceNtp::select(void){
    if(0xF0 <= disp_sta){
        disp_sta = 0;
    }
    
    /* 接続開始 */
    TimeAdjust::startGetTimeNtp();
    disp_sta = 1;

    return NORMAL;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceNtp::cancel(void){
    if(0xF0 <= disp_sta){
        disp_sta = 0;
    }
    return NORMAL;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceNtp::valNext(void){
    if(0xF0 <= disp_sta){
        disp_sta = 0;
    }
    return NORMAL;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceNtp::valPrev(void){
    if(0xF0 <= disp_sta){
        disp_sta = 0;
    }
    return NORMAL;
}


