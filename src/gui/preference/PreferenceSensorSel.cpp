/* センサ選択 */
#include "PreferenceSensorSel.h"
#include "DataMem.h"
#include "PageObj.h"

#include "LapSensorMgr.h"

PreferenceSensorSel Pref_SensorSel("Sensor", nullptr);

/* 初期化 */
void PreferenceSensorSel::init(void){
    PreferenceMenuItem::init();

    DataMem::read(LAP_SENS_SEL, &selsens);
}

/* 周期実行 */
bool PreferenceSensorSel::draw(M5EPD_Canvas** draw_canvas){   /* 周期実行 更新なければfalse */
    char c_tmp[30];

    *draw_canvas = &canvas;

    if(update){
        update = false;

        canvas.clear();
        canvas.fillCanvas(G0);

        canvas.setTextFont(4);  /* きれいなフォント */
        canvas.setTextSize(2);
        canvas.setCursor(0, 5);
        canvas.printf("%s", title.c_str());

        LapSensor *sens_obj = LapSensorMgr::sensObj((LapSensorMgr::TYPE)selsens);
        sprintf(c_tmp, sens_obj->name.c_str());
        
        canvas.setTextDatum(2);
        canvas.drawString(c_tmp, w-30, 5);

        return true;
    }else{
        return false;
    }
}

/* 決定 */
PreferenceMenuItem::State PreferenceSensorSel::select(void){
    ++selsens;
    if(selsens>2){
        selsens = 0;
    }

    DataMem::write(LAP_SENS_SEL, selsens);
    update = true;
    return NORMAL;
}
/* キャンセル */
PreferenceMenuItem::State PreferenceSensorSel::cancel(void){
    return NORMAL;
}

/* 設定値変更 次 */
PreferenceMenuItem::State PreferenceSensorSel::valNext(void){
    return NORMAL;
}

/* 設定値変更 前 */
PreferenceMenuItem::State PreferenceSensorSel::valPrev(void){
    return NORMAL;
}


