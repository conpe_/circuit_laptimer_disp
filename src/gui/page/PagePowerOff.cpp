/* ページのテンプレート */

#include "PagePowerOff.h"
#include "LapTimer.h"

/* コンストラクタ */
PagePowerOff::PagePowerOff(const char* title){
    this->title = title;   /* タイトルをセット */
    
    button[0]->setLabel("");
    button[1]->setLabel("                    ON   for 3sec");
    button[2]->setLabel("");
}
/* デストラクタ */
PagePowerOff::~PagePowerOff(void){
}

/* メニュー入ったときの処理 */
int PagePowerOff::entryProc(void){
    enter_time = millis();

    canvas_main.setTextFont(4);
    canvas_main.setTextSize(1);
    canvas_main.setTextArea(LCD_W/2+80, LCD_H-30, 600, 100);
    //canvas_main.printf("for 3sec");

    canvas_main.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_DU4);

    /* 計測停止 */
    lap_timer.stop();

    return 0;
}

/* メニューを抜けるときの処理 */
int PagePowerOff::exitProc(void){

    return 0;
}

/* メニュー中の処理 */
Page::e_state PagePowerOff::updateProc(Page** next_page){
    int32_t until_time = POWER_DOWN_TIME + enter_time - millis();      /* シャットダウンまでの時間 */
    
    if(0 >= until_time){
        
        M5EPD_Canvas_Semaphore c_G0(&M5.EPD);
        c_G0.createCanvas(LCD_W, LCD_H+LCD_HEADER_H);
        c_G0.fillCanvas(G0);
        c_G0.pushCanvas(0, 0, UPDATE_MODE_GC16);

        delay(1000);    /* 表示待ち */

        M5.shutdown();              /* 電源つながっていると切れないので注意 */
        /* 電源つながっているとshutdown効かないので代わりにリセット */
        ESP.restart();
    }

    return e_state::CONT;
}