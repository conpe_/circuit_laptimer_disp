/* GUI template */

//#pragma once

#ifndef PAGE_H
#define PAGE_H

#include <Arduino.h>
#include <M5EPD.h>
#include <utility/Button.h>

#include <string>
#include <vector>
using namespace std;

#include "CmdSrvDl.h"

/* 注意 */
/* M5stackに依存 */
/* どこかでM5.update()が実行されていること */

#define LCD_W M5EPD_PANEL_W

#define LCD_HEADER_W LCD_W
#define LCD_HEADER_H 45

#define LCD_FOOTER_W LCD_W
#define LCD_FOOTER_H 30

#define LCD_H (M5EPD_PANEL_H - LCD_HEADER_H - LCD_FOOTER_H)

#define LCD_MAIN_W M5EPD_PANEL_W
#define LCD_MAIN_H (M5EPD_PANEL_H - LCD_HEADER_H - LCD_FOOTER_H)


#define PAGE_BUTTON_NUM 3

#define SCROLLBAR_POS_TOP 0     /* canvas_main内での座標 */
#define SCROLLBAR_H 168
#define SCROLLBAR_W 18

#define BUTTON_W 80
#define BUTTON_H 22


/* 画面更新関数 SPI用セマフォ(EPD、SDカード)付 */
class M5EPD_Canvas_Semaphore : public M5EPD_Canvas{
public:
    M5EPD_Canvas_Semaphore(M5EPD_Driver *driver):M5EPD_Canvas(driver){};
    M5EPD_Canvas_Semaphore():M5EPD_Canvas(){};
    void pushCanvas(int32_t x, int32_t y, m5epd_update_mode_t mode);
};


/* センサバッテリ情報取得用 */
class CmdProcGetSensBatt:public CmdProc{
public:
    uint8_t SocPct;
    uint16_t Voltage;

    void update(void);
    uint8_t CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len);
};


/* ページのテンプレート */
class Page{
public:
    string title;
    Page(const char* title = "MENU");
    virtual ~Page(void);

    enum e_state{
        CONT,
        BACK,
        NEXT
    };

    /* メニュー入ったときの処理 */
    virtual int entry(void);
    /* メニュー処理(ここは継承しない) */
    e_state update(Page** next_page);       /* 継続，戻る，進む(指定メニューへ) */
    /* メニューを抜けるときの処理 */
    virtual int exit(void);


    /* ボタン */
    class DispButton:public Button{
    public:
        DispButton(const char* label, uint8_t pin, uint8_t invert, uint32_t dbTime);
        int draw(void);
        int setLabel(const char* label);
        int setPos(int x, int y, int w, int h);
        void reset(void);
        bool isLongPress(void);

        /* override */
        uint8_t wasReleased(void);
        uint8_t pressedFor(uint32_t ms);

    protected:
        string label;
        int _x,_y,_w,_h;
        bool initialized_wasReleased;   /* 一旦ボタンを放すまでは押下判定を無効にするフラグ */
        bool initialized_pressedFor;
    };

    vector<DispButton*> button;   /* ボタン3つ */


protected:
    /* メニュー入ったときの処理 */
    virtual int entryProc(void) = 0;
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page) = 0;
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void) = 0;

    static uint8_t guruguru_cnt;
    static bool blink;
    static uint32_t blink_time;

    static CmdProcGetSensBatt SensBatt;    /* センサ電源情報 */

    static uint32_t last_update_time_main;       /* 画面更新時間 */
    static M5EPD_Canvas_Semaphore canvas_main;
    bool disp_init;

    static std::vector<std::string> data_to_nextpage;  /* 次のページへ渡すデータ */
private:
    virtual int dispMenuTitle(uint16_t pos_x, uint16_t pos_y);
    virtual int dispTitleBar(void);                   /* 共通の描画 */

    static void dispGnssSta(uint16_t pos_x, uint16_t pos_y);              /* GNSS状態 */
    static void dispSector(uint16_t pos_x, uint16_t pos_y);               /* セクター表示 */
    static void dispMesState(uint16_t pos_x, uint16_t pos_y);             /* 計測状態 */
    static void dispSdState(uint16_t pos_x, uint16_t pos_y);              /* SDカード挿入状態 */
    static void dispBattState(uint16_t pos_x, uint16_t pos_y, uint8_t batt_level);            /* バッテリ状態 */
    static void dispAntenna(uint16_t pos_x, uint16_t pos_y, uint8_t bari);      /* 接続状態表示 */
    static void dispTime(uint16_t pos_x, uint16_t pos_y);

    static uint32_t last_update_time_header;       /* ヘッダ更新時間 */
    static uint32_t last_update_time_footer;       /* フッタ更新時間 */

    static M5EPD_Canvas_Semaphore canvas_header;
    static M5EPD_Canvas_Semaphore canvas_footer;

};

#endif
