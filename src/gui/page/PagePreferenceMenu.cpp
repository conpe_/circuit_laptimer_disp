#include "PagePreferenceMenu.h"

#include "PageObj.h"

#include "PreferenceSectorNum.h"
#include "PreferenceSensorSel.h"
#include "PreferenceSensorPairing.h"
#include "PreferenceNtp.h"
#include "PreferenceGearDrive.h"
#include "PreferenceGearDriven.h"
#include "PreferenceDispSpdMethod.h"
#include "PreferenceTpmsEn.h"
#include "PreferenceEngPulseDiv.h"
#include "PreferencePdlCalib.h"


PagePreferenceMenu::PagePreferenceMenu(const char* title):
                                        Page(title),
                                        canvas_sel(&M5.EPD),
                                        canvas_BlankItem(&M5.EPD)
{
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("OK");
    button[2]->setLabel("Next");
}

PagePreferenceMenu::~PagePreferenceMenu(void){

}

/* メニュー入ったときの処理 */
int PagePreferenceMenu::entryProc(void){
    sel_idx_last = 255;
    disp_offset_last = 255;

    canvas_sel.createCanvas(400, LCD_MAIN_H);
    canvas_sel.fillCanvas(G0);
    canvas_sel.setTextWrap(false, false);

    canvas_sel.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_A2);    /* DU4: 120ms, 4階調 */
    canvas_sel.deleteCanvas();

    canvas_sel.createCanvas(30, LCD_MAIN_H);    /* カーソル表示分の幅 */
    canvas_sel.fillCanvas(G0);

    canvas_BlankItem.createCanvas(PREF_W, PREF_H);
    canvas_BlankItem.fillCanvas(G0);
    
    state = State::SEL_ITEM;

    /* 設定項目リスト */
    pref_list.clear();
    pref_list.push_back(&Pref_SectorNum);
    pref_list.push_back(&Pref_DispSpdMethod);
    pref_list.push_back(&Pref_SensorSel);
    pref_list.push_back(&Pref_SensorPairing);
    pref_list.push_back(&Pref_GearDrive);
    pref_list.push_back(&Pref_GearDriven);
    pref_list.push_back(&Pref_EngPulseDiv);
    pref_list.push_back(&Pref_TpmsEn);
    pref_list.push_back(&Pref_Ntp);
    pref_list.push_back(&Pref_PdlCalib);
    
    for(auto p=pref_list.begin(); pref_list.end()!=p; ++p){
        (*p)->init();
    }

    updateDisp();   /* 初回描画 */

    return 0;
}

/* メニューを抜けるときの処理 */
int PagePreferenceMenu::exitProc(void){
    canvas_sel.deleteCanvas();

    return 0;
}

/* メニュー中の処理 */
Page::e_state PagePreferenceMenu::updateProc(Page** next_page){
    uint8_t sel_idx_last = sel_idx;

    PreferenceMenuItem::State item_state = pref_list[sel_idx]->currentState();

    if(state==State::SEL_ITEM){
        if(button[0]->wasPressed() || button[0]->pressedFor(1000)){
            if(sel_idx>0){
                --sel_idx;
            }else{
                sel_idx = pref_list.size()-1;
            }
        }

        if(button[1]->pressedFor(1000)){
            return Page::e_state::BACK;
        }

        if(button[1]->wasReleased()){
            item_state = pref_list[sel_idx]->select();

            switch(item_state){
            case PreferenceMenuItem::State::NORMAL:
                /* 何もしない */
                break;
            case PreferenceMenuItem::State::SETTING:
                /* 値変更状態 */
                state = State::CHANGE_VAL;
                break;
            case PreferenceMenuItem::State::NEXT_PAGE:
                /* 次のページへ */
                *next_page = pref_list[sel_idx]->nextPage();
                return NEXT;
            }
        }

        if(button[2]->wasPressed() || button[2]->pressedFor(1000)){
            if(sel_idx < pref_list.size()-1){
                ++sel_idx;
            }else{
                sel_idx = 0;
            }
        }
    }else if(state==State::CHANGE_VAL){
        if(button[0]->wasPressed() || button[0]->pressedFor(1000)){
            item_state = pref_list[sel_idx]->valPrev();
        }

        if(button[1]->pressedFor(1000)){
            item_state = pref_list[sel_idx]->cancel();
            return Page::e_state::BACK;
        }

        if(button[1]->wasReleased()){
            item_state = pref_list[sel_idx]->select();
        }

        if(button[2]->wasPressed() || button[2]->pressedFor(1000)){
            item_state = pref_list[sel_idx]->valNext();
        }

        /* 状態遷移 */
        switch(item_state){
        case PreferenceMenuItem::State::NORMAL:
            /* 項目選択状態へ */
            state = State::SEL_ITEM;
            break;
        case PreferenceMenuItem::State::SETTING:
            /* 値変更状態 */
            state = State::CHANGE_VAL;
            break;
        case PreferenceMenuItem::State::NEXT_PAGE:
            /* 次のページへ */
            *next_page = pref_list[sel_idx]->nextPage();
            return NEXT;
        }

    }

    /* 選択変化 */
    updateDisp();   /* 描画更新 */
    
    return CONT;
}

/* 描画更新 */
void PagePreferenceMenu::updateDisp(void){

    /* 各項目の描画 */
    M5EPD_Canvas* canvas;

    /* リスト表示 */
    #define MENU_ROW (PREF_ROW)
    canvas_sel.setTextArea(0, 42, LCD_H, LCD_W);

    uint8_t disp_sel_row;
    uint8_t disp_offset;
    #if 0   /* スクロール風 */
    if((sel_idx < MENU_ROW/2) || (pref_list.size()<=MENU_ROW)){
        disp_sel_row = sel_idx;
        disp_offset = 0;
    }else if(sel_idx >= pref_list.size()-MENU_ROW/2){
        disp_sel_row = MENU_ROW-(pref_list.size()-sel_idx);
        disp_offset = pref_list.size() - MENU_ROW;
    }else{
        disp_sel_row = MENU_ROW/2;
        disp_offset = sel_idx - MENU_ROW/2;
    }
    #else   /* ページ切り替え風 */
        disp_sel_row = sel_idx%MENU_ROW;
        disp_offset = (sel_idx/MENU_ROW)*MENU_ROW;
    #endif
    
    canvas_sel.fillCanvas(G0);
    canvas_sel.setTextColor(G15, G0);
    canvas_sel.setTextFont(1);  /* 等幅フォント */
    canvas_sel.setTextSize(6);
    canvas_sel.setCursor(0, PREF_H*disp_sel_row);
    canvas_sel.setTextDatum(0);
    canvas_sel.printf(">");

    if(sel_idx_last != sel_idx){
        canvas_sel.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_A2);    /* DU4: 120ms, 4階調 */
    }


    for(uint8_t row=0; ((MENU_ROW>pref_list.size())?pref_list.size():MENU_ROW) > row; ++row){
        if((row+disp_offset)<pref_list.size()){
            /* 各項目描画(表示内容が変わったか、表示位置が変わった) */
            if(pref_list[row + disp_offset]->draw(&canvas) || (disp_offset_last != disp_offset)){
                canvas->pushCanvas(30, LCD_HEADER_H + PREF_H*row, UPDATE_MODE_A2);
                log_i("draw pref%d=%s", row, pref_list[row + disp_offset]->prefTitle()->c_str());
            }
        }else{
            /* 空き項目 */
            if(disp_offset_last != disp_offset){
                canvas_BlankItem.pushCanvas(30, LCD_HEADER_H + PREF_H*row, UPDATE_MODE_A2);
            }
        }
    }


    sel_idx_last = sel_idx;
    disp_offset_last = disp_offset;
}

