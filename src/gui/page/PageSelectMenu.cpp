#include "PageSelectMenu.h"

#include "PageObj.h"


PageSelectMenu::PageSelectMenu(const char* title):
                                        Page(title),
                                        canvas_sel(&M5.EPD)
{
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("OK");
    button[2]->setLabel("Next");
}

PageSelectMenu::~PageSelectMenu(void){

}

/* メニュー入ったときの処理 */
int PageSelectMenu::entryProc(void){
    canvas_sel.createCanvas(400, LCD_MAIN_H);
    canvas_sel.fillCanvas(G0);
    canvas_sel.setTextWrap(false, false);

    updateDisp();   /* 初回描画 */

    return 0;
}

/* メニューを抜けるときの処理 */
int PageSelectMenu::exitProc(void){
    canvas_sel.deleteCanvas();

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageSelectMenu::updateProc(Page** next_page){
    uint8_t sel_idx_last = sel_idx;

    if(button[0]->wasPressed() || button[0]->pressedFor(1000)){
        if(sel_idx>0){
            --sel_idx;
        }else{
            sel_idx = menus.size()-1;
        }
    }

    if(button[1]->pressedFor(1000)){
        return Page::e_state::BACK;
    }

    if(button[1]->wasReleased()){
        *next_page = menus[sel_idx].next_page;
        return menus[sel_idx].next;
    }

    if(button[2]->wasPressed() || button[2]->pressedFor(1000)){
        if(sel_idx < menus.size()-1){
            ++sel_idx;
        }else{
            sel_idx = 0;
        }
    }

    /* 選択変化 */
    if(sel_idx_last != sel_idx){
        updateDisp();   /* 描画更新 */
    }
    
    return CONT;
}

/* 描画更新 */
void PageSelectMenu::updateDisp(void){
    
    //canvas_sel.fillRect(0, 35, LCD_W-SCROLLBAR_W-2, (LCD_H-BUTTON_H)-35-2, G0);

    /* リスト表示 */
    #define MENU_ROW 7 
    canvas_sel.setTextArea(0, 42, LCD_H, LCD_W);

    uint8_t disp_sel_row;
    uint8_t disp_offset;
    if((sel_idx < MENU_ROW/2) || (menus.size()<=MENU_ROW)){
        disp_sel_row = sel_idx;
        disp_offset = 0;
    }else if(sel_idx >= menus.size()-MENU_ROW/2){
        disp_sel_row = MENU_ROW-(menus.size()-sel_idx);
        disp_offset = menus.size() - MENU_ROW;
    }else{
        disp_sel_row = MENU_ROW/2;
        disp_offset = sel_idx - MENU_ROW/2;
    }

    for(uint8_t row=0; ((MENU_ROW>menus.size())?menus.size():MENU_ROW) > row; ++row){
        if(row<menus.size()){
            canvas_sel.setTextColor(G15, G0);
            
            canvas_sel.setTextFont(1);  /* 等幅フォント */
            canvas_sel.setTextSize(6);
            canvas_sel.printf("%s", (row == disp_sel_row)?">":" ");
            canvas_sel.setTextFont(4);  /* きれいなフォント */
            canvas_sel.setTextSize(2);
            canvas_sel.printf("%s\r\n", menus[row + disp_offset].label.c_str());
        }
    }


    /* スクロールバー */
    #if 0
    if(menus.size()>MENU_ROW){
        canvas_sel.fillRect(
            LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
            SCROLLBAR_W, SCROLLBAR_H,
            G0
        );
        canvas_sel.drawRect(
            LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
            SCROLLBAR_W, SCROLLBAR_H,
            G15
        );
        canvas_sel.fillRect(
            LCD_W-SCROLLBAR_W-1, (int)((float)SCROLLBAR_POS_TOP + (float)SCROLLBAR_H * disp_offset / (menus.size()-MENU_ROW+1)), 
            SCROLLBAR_W, (int)( ((float)SCROLLBAR_H * (disp_offset+1) / (menus.size()-MENU_ROW+1)) - ((float)SCROLLBAR_H * disp_offset / (menus.size()-MENU_ROW+1)) ),
            DARKGREY
        );
    }else{
        canvas_sel.fillRect(
            LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
            SCROLLBAR_W, SCROLLBAR_H,
            DARKGREY
        );
    }
    #endif

    canvas_sel.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_A2);    /* DU4: 120ms, 4階調 */
    
}

