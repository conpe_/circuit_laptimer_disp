/* GUI template */

//#pragma once

#ifndef PAGE_OBJ_H
#define PAGE_OBJ_H

#include "PageSelectMenuTop.h"
#include "PageLapTimeCnt.h"
#include "PageLapTimeLog.h"
#include "PageLog.h"
#include "PageSetSensMac.h"
#include "PagePreferenceMenu.h"
#include "PageInfo.h"
#include "PagePowerOff.h"

extern PageLapTimeCnt page_laptime_cnt;
extern PageLapTimeLog page_laptime_log;
extern PageLog page_log;
extern PageInfo page_info;
extern PagePowerOff page_power_off;

// menu
extern PageSetSensMac page_set_sensmac;
extern PagePreferenceMenu page_menu_preference; /* 設定メニュー */

extern PageSelectMenuTop page_menu;
#endif
