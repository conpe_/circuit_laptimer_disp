#include "PageLapTimeCnt.h"
#include "PageObj.h"
#include "Tire.h"
#include "Engine.h"
#include "Gnss.h"
#include "DataMem.h"
#include "mdlCanDataConfig_B0.h"

#include "img_formula_body.h"
#include "img_formula_fr.h"
#include "img_formula_fl.h"
#include "img_formula_rr.h"
#include "img_formula_rl.h"

#define LAPMODE 1		/* 0:次のラインから計測開始，1:今のセクターが第1セクター */
#define DISP_CURRENT_LAP 0  /* 今のラップを表示する？ */

#define CANVAS_ENGINE_W (380)
#define CANVAS_ENGINE_H (100)
#define CANVAS_SPD_W (250)
#define CANVAS_SPD_H (135)

PageLapTimeCnt::PageLapTimeCnt(const char* title):Page(title)
                                ,canvas_currenttime(&M5.EPD)
                                ,canvas_lasttime(&M5.EPD)
                                ,canvas_besttime(&M5.EPD)
                                ,canvas_Tire(&M5.EPD)
                                ,canvas_engine(&M5.EPD)
                                ,canvas_spd(&M5.EPD)
                                ,canvas_sta(&M5.EPD)
                                ,last_sec(0)
                                ,eng_running(true)
                                ,clutch_bite(false)
{
    
    button[0]->setLabel("Menu");
    button[1]->setLabel("");
    button[2]->setLabel("Lap");
}

PageLapTimeCnt::~PageLapTimeCnt(void){

}

/* メニュー入ったときの処理 */
int PageLapTimeCnt::entryProc(void){
    LapTimer::TimState current_timer_state = lap_timer.getState();
    reject_start_flg = true;
    last_sector = 0;
    eng_running = true;
    timer_state = LapTimer::TimState::NONE;

    /* 現在LAP表示にするためカウント初期化 */
    lasttime_disp_cnt = millis() - DISP_LOCK_TIME;
    currentlap_disp_cnt = millis() - DISP_CURRENTTIME_UPDATE_CYCLE;
    engine_disp_cnt = millis() - DISP_ENGINE_UPDATE_CYCLE;

    canvas_currenttime.createCanvas(820, 150);
    canvas_currenttime.fillCanvas(G0);
    canvas_currenttime.setTextWrap(false, false);

    canvas_lasttime.createCanvas(820, 150);
    canvas_lasttime.fillCanvas(G0);
    canvas_lasttime.setTextWrap(false, false);

    canvas_besttime.createCanvas(650, 100);
    canvas_besttime.fillCanvas(G0);
    canvas_besttime.setTextWrap(false, false);

    canvas_Tire.createCanvas(200, 309);

    canvas_engine.createCanvas(CANVAS_ENGINE_W, CANVAS_ENGINE_H);
    canvas_engine.fillCanvas(G0);
    canvas_engine.setTextWrap(false, false);

    canvas_spd.createCanvas(CANVAS_SPD_W, CANVAS_SPD_H);
    canvas_spd.fillCanvas(G0);
    canvas_spd.setTextWrap(false, false);

    canvas_sta.createCanvas(80, 80);
    canvas_sta.fillCanvas(G0);

    DataMem::read(DISP_SPD_METHOD, &disp_spd_method);

    return 0;
}

/* メニューを抜けるときの処理 */
int PageLapTimeCnt::exitProc(void){
    canvas_lasttime.deleteCanvas();
    canvas_besttime.deleteCanvas();
    canvas_Tire.deleteCanvas();
    canvas_engine.deleteCanvas();
    canvas_sta.deleteCanvas();

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageLapTimeCnt::updateProc(Page** next_page){
    bool update_lastlap = false;
    bool update_bestlap = false;
    uint32_t current_time = millis();
    
    LapTimer::TimState current_timer_state = lap_timer.getState();

    uint8_t sec = lap_timer.getSector();

    if(LapTimer::TimState::STOP == current_timer_state){
        sec = 0;
    }


    // 情報取得
    uint8_t lap = 0;
    uint32_t currentlap_time = 0;
    lap_timer.getLap(0, &lap, &currentlap_time);
    uint32_t last_time = 0;
    uint8_t lastlap_tmp = 0;
    lap_timer.getLap(-1, &lastlap_tmp, &last_time);
    uint8_t bestlap_tmp = 0;
    uint32_t best_time_tmp = 0;
    lap_timer.getBestLap(&bestlap_tmp, &best_time_tmp);

    if(LapTimer::TimState::STOP == lap_timer.getState()){   /* 停止中は表示を0 */
        lastlap_tmp = 0;
        bestlap_tmp = 0;
    }
    /* 更新 */
    if(last_lap != lastlap_tmp){
        update_lastlap = true;
    }
    last_lap = lastlap_tmp;
    if(best_lap != bestlap_tmp){
        update_bestlap = true;
    }
    best_lap = bestlap_tmp;
    best_time = best_time_tmp;

    if(disp_init){
        if(!DISP_CURRENT_LAP){
            update_lastlap = true;
        }
        update_bestlap = true;
    }

    // 状態遷移
    // left
    if(button[0]->wasPressed()){
        *next_page = &page_menu;   /* 設定画面へ */
        return Page::e_state::NEXT;
    }
    
    // center
    if(button[1]->wasPressed()){
        if(LapTimer::TimState::COUNTING == current_timer_state){
            //lap_timer.lap(LAPMODE);        /* ラップ頭出し(一時停止 or 1stセクターセット) */
            lap_timer.stop();
        }
    }

    // right
    if(button[2]->wasPressed()){
        *next_page = &page_laptime_log;     /* ラップ確認画面へ */
        return Page::e_state::NEXT;
    }


    // ボタン表示
    if(timer_state != current_timer_state){
        if(LapTimer::TimState::COUNTING == current_timer_state){
            button[1]->setLabel("Stop");
        }else{
            button[1]->setLabel("");
        }
        
        button[1]->draw();
    }

    static uint32_t last_timer5sec = 0;
    uint32_t pit_time = lap_timer.pitTime();

    bool timer_5sec = false;
    if( ((last_timer5sec+5000) <= pit_time) || ((0==last_timer5sec) && (0!=pit_time)) ){
        last_timer5sec = pit_time;
        timer_5sec = true;
        log_i("pit_time=%d", pit_time);
    }
    if(0==pit_time){
        /* ピットアウトしたらラストラップ表示に戻す */
        if(0<last_timer5sec){
            update_lastlap = true;
        }
        last_timer5sec = 0;
    }

    /* ピット時間 */
    if((0!=pit_time) && timer_5sec){  /* ピット中 5秒ごと */
        canvas_lasttime.fillCanvas(G0);
        /* 左領域 */
        canvas_lasttime.setTextFont(1);
        canvas_lasttime.setTextSize(3);
        canvas_lasttime.setTextColor(G0, G15);
        canvas_lasttime.fillRoundRect(0, 15, 90, 45, 15, G15);
        canvas_lasttime.drawString("Pit", 15, 25);
        
        canvas_lasttime.setTextFont(1);
        canvas_lasttime.setTextSize(6);
        canvas_lasttime.setTextColor(G15, G0);
        canvas_lasttime.setTextArea(10, 70, 100, 48);
        if(last_lap>0){
            canvas_lasttime.printf("%2d", last_lap);
        }else{
            canvas_lasttime.printf("--");
        }

        uint8_t sector_num = lap_timer.getSectorNum();
        if(1 < sector_num){
            char str_tmp[10];

            if(sec){
                sprintf(str_tmp, "%01d/%01d", sec, sector_num);
            }else{
                sprintf(str_tmp, "-/%01d", sector_num);
            }
            canvas_lasttime.setTextFont(1);
            canvas_lasttime.setTextSize(3);
            canvas_lasttime.setTextColor(G15, G0);
            canvas_lasttime.drawString(str_tmp, 10, 120);
        }


        /* タイム */
        canvas_lasttime.setTextFont(7);
        canvas_lasttime.setTextSize(3);
        canvas_lasttime.setTextColor(G0, G15);   /* 白抜き */
        canvas_lasttime.setTextArea(300, 0, LCD_W, LCD_H);
        canvas_lasttime.printf("%02d:%02d", (pit_time/1000/60)%100, (pit_time/1000)%60);

        canvas_lasttime.setTextFont(4);
        canvas_lasttime.setTextSize(6);
        canvas_lasttime.setTextColor(G15, G0);   /* 白抜き */
        canvas_lasttime.setTextArea(150, 15, LCD_W, LCD_H);
        canvas_lasttime.printf("P");
        
        /* 描画 */
        canvas_lasttime.pushCanvas(20, LCD_HEADER_H + 10, UPDATE_MODE_A2);
    }

    /* 直前ラップ */
    if((update_lastlap) || (last_sec != sec)){  /* ラストラップが更新された or 走ってるセクタが更新された */
        canvas_lasttime.fillCanvas(G0);
        /* 左領域 */
        canvas_lasttime.setTextFont(1);
        canvas_lasttime.setTextSize(3);
        canvas_lasttime.setTextColor(G0, G15);
        canvas_lasttime.fillRoundRect(0, 15, 90, 45, 15, G15);
        canvas_lasttime.drawString("Last", 8, 25);
        
        canvas_lasttime.setTextFont(1);
        canvas_lasttime.setTextSize(6);
        canvas_lasttime.setTextColor(G15, G0);
        canvas_lasttime.setTextArea(10, 70, 100, 48);
        if(last_lap>0){
            canvas_lasttime.printf("%2d", last_lap);
        }else{
            canvas_lasttime.printf("--");
        }

        uint8_t sector_num = lap_timer.getSectorNum();
        if(1 < sector_num){
            char str_tmp[10];

            if(sec){
                sprintf(str_tmp, "%01d/%01d", sec, sector_num);
            }else{
                sprintf(str_tmp, "-/%01d", sector_num);
            }
            canvas_lasttime.setTextFont(1);
            canvas_lasttime.setTextSize(3);
            canvas_lasttime.setTextColor(G15, G0);
            canvas_lasttime.drawString(str_tmp, 10, 120);
        }


        /* タイム */
        canvas_lasttime.setTextFont(7);
        canvas_lasttime.setTextSize(3);
        if((last_time == best_time) && (last_time!=0) && (1<last_lap)){
            canvas_lasttime.setTextColor(G0, G15);   // ベスト更新
        }else{
            canvas_lasttime.setTextColor(G15, G0);
        }
        canvas_lasttime.setTextArea(110, 0, LCD_W, LCD_H);
        if(0<last_lap){
            canvas_lasttime.printf("%01d:%02d:%03d",(last_time/1000/60)%10,(last_time/1000)%60,last_time%1000);
        }else{
            canvas_lasttime.printf("-:--:---");
        }


        /* 描画 */
        canvas_lasttime.pushCanvas(20, LCD_HEADER_H + 10, UPDATE_MODE_A2);
    
        lasttime_disp_cnt = current_time;
    }

    /* 現在ラップ */
    #if DISP_CURRENT_LAP
    if( lasttime_disp_cnt + DISP_LOCK_TIME < current_time ){   /* ラストラップ更新から5sec以上たった */
        if( (currentlap_disp_cnt + DISP_CURRENTTIME_UPDATE_CYCLE < current_time) || (last_sec != sec) ){
            canvas_currenttime.fillCanvas(G0);
            /* 左領域 */
            canvas_currenttime.setTextFont(1);
            canvas_currenttime.setTextSize(3);
            canvas_currenttime.setTextColor(G0, G15);
            canvas_currenttime.fillRoundRect(0, 15, 90, 45, 15, G15);
            canvas_currenttime.drawString("Lap", 8, 25);
            
            canvas_currenttime.setTextFont(1);
            canvas_currenttime.setTextSize(6);
            canvas_currenttime.setTextColor(G15, G0);
            canvas_currenttime.setTextArea(10, 70, 100, 48);
            if(lap>0){
                canvas_currenttime.printf("%2d", lap);
            }else{
                canvas_currenttime.printf("--");
            }

            uint8_t sector_num = lap_timer.getSectorNum();
            if(1 < sector_num){
                char str_tmp[10];

                if(sec){
                    sprintf(str_tmp, "%01d/%01d", sec, sector_num);
                }else{
                    sprintf(str_tmp, "-/%01d", sector_num);
                }
                canvas_currenttime.setTextFont(1);
                canvas_currenttime.setTextSize(3);
                canvas_currenttime.setTextColor(G15, G0);
                canvas_currenttime.drawString(str_tmp, 10, 120);
            }
            /* タイム */
            canvas_currenttime.setTextFont(7);
            canvas_currenttime.setTextSize(3);
            canvas_currenttime.setTextColor(G15, G0);
            canvas_currenttime.setTextArea(110, 0, LCD_W, LCD_H);
            if(0<lap){
                canvas_currenttime.printf("%01d:%02d:%03d",(currentlap_time/1000/60)%10,(currentlap_time/1000)%60,currentlap_time%1000);
            }else{
                canvas_currenttime.printf("-:--:---");
            }

            /* 描画 */
            canvas_currenttime.pushCanvas(20, LCD_HEADER_H + 10, UPDATE_MODE_A2);

            currentlap_disp_cnt = current_time;
        }

    }
    #endif

    /* ベストラップ */

    if(update_bestlap){
        canvas_besttime.fillCanvas(G0);
        /* 左領域 */
        canvas_besttime.setTextFont(1);
        canvas_besttime.setTextSize(3);
        canvas_besttime.setTextColor(G0, G15);
        canvas_besttime.fillRoundRect(0, 0, 90, 45, 15, G15);
        canvas_besttime.drawString("Best", 8, 10);
        
        canvas_besttime.setTextFont(1);
        canvas_besttime.setTextSize(6);
        canvas_besttime.setTextColor(G15, G0);
        canvas_besttime.setTextArea(10, 55, 100, 48);
        if(best_lap>0){
            canvas_besttime.printf("%2d", best_lap);
        }else{
            canvas_besttime.printf("--");
        }
        /* タイム */
        canvas_besttime.setTextFont(7);
        canvas_besttime.setTextSize(2);
        canvas_besttime.setTextColor(G15, G0);
        canvas_besttime.setTextArea(110, 0, LCD_W, LCD_H);
        if(0<best_lap){
            canvas_besttime.printf("%01d:%02d:%03d",(best_time/1000/60)%10,(best_time/1000)%60,best_time%1000);
        }else{
            canvas_besttime.printf("-:--:---");
        }

        /* 描画 */
        canvas_besttime.pushCanvas(20, LCD_HEADER_H + 180, UPDATE_MODE_A2);
    }

    static uint32_t t = 0;

    if(t + 10000 < current_time){
        M5.SHT30.UpdateData();
        float humi = M5.SHT30.GetRelHumidity();
        float temp = M5.SHT30.GetTemperature();
        Tire *tire;

        canvas_Tire.fillCanvas(G0);
        canvas_Tire.drawJpg(img_formula_body, IMG_FORMULA_BODY_LEN);
        canvas_Tire.drawJpg(img_formula_fl, IMG_FORMULA_FL_LEN, 0, 58);
        canvas_Tire.drawJpg(img_formula_fr, IMG_FORMULA_FR_LEN, 127, 58);
        canvas_Tire.drawJpg(img_formula_rl, IMG_FORMULA_RL_LEN, 0, 245);
        canvas_Tire.drawJpg(img_formula_rr, IMG_FORMULA_RR_LEN, 142, 245);


        canvas_Tire.setTextFont(1);
        canvas_Tire.setTextSize(2);
        canvas_Tire.setTextColor(G0,G15);
        char strtmp[10];
        /* 本体温度 */
        sprintf(strtmp, "%.1f%", temp);
        canvas_Tire.drawString(strtmp, 76, 190);
        sprintf(strtmp, "%2.0f%%", humi);
        canvas_Tire.drawString(strtmp, 84, 210);
        /* FL */
        tire = Tire::find(std::string("TireFL"));
        if(tire){
            canvas_Tire.setTextDatum(TL_DATUM);    /* 左上 */
            sprintf(strtmp, "%2.1f", (float)tire->Pressure_kpa/10.0f);
            canvas_Tire.drawString(strtmp, 5, 70);
            sprintf(strtmp, "%2d", tire->Temp_deg);
            canvas_Tire.drawString(strtmp, 5, 90);
        }
        /* FR */
        tire = Tire::find(std::string("TireFR"));
        if(tire){
            canvas_Tire.setTextDatum(TR_DATUM);    /* 右上 */
            sprintf(strtmp, "%2.1f", (float)tire->Pressure_kpa/10.0f);
            canvas_Tire.drawString(strtmp, 195, 70);
            sprintf(strtmp, "%2d", tire->Temp_deg);
            canvas_Tire.drawString(strtmp, 195, 90);
        }
        /* RL */
        tire = Tire::find(std::string("TireRL"));
        if(tire){
            canvas_Tire.setTextDatum(TL_DATUM);    /* 左上 */
            sprintf(strtmp, "%2.1f", (float)tire->Pressure_kpa/10.0f);
            canvas_Tire.drawString(strtmp, 0, 260);
            sprintf(strtmp, "%2d", tire->Temp_deg);
            canvas_Tire.drawString(strtmp, 0, 280);
        }
        /* RR */
        tire = Tire::find(std::string("TireRR"));
        if(tire){
            canvas_Tire.setTextDatum(TR_DATUM);    /* 右上 */
            sprintf(strtmp, "%2.1f", (float)tire->Pressure_kpa/10.0f);
            canvas_Tire.drawString(strtmp, 195, 260);
            sprintf(strtmp, "%2d", tire->Temp_deg);
            canvas_Tire.drawString(strtmp, 195, 280);
        }
        canvas_Tire.setTextDatum(TL_DATUM);    /* 左上(デフォルト)に戻す */

        canvas_Tire.pushCanvas(750, 200, UPDATE_MODE_A2);

        t = current_time;
    }


    if((engine_disp_cnt + DISP_ENGINE_UPDATE_CYCLE < current_time) || disp_init ){
        uint16_t current_rpm = 0;
        uint16_t peak_rpm = 0;

        if(eng){    /* エンジンオブジェクトから情報取得 */
            current_rpm = eng->spd_rpm;
            peak_rpm = eng->spd_rpm_peak;
        }

        /* エンジン動いてる */
        if(0 < peak_rpm){
            dispEngRpm(current_rpm, peak_rpm);
            eng_running = true;
        }else{
            /* エンジン動いてない */
            if(eng_running){    /* 0rpm表示にして以降表示更新しない */
                eng_running = false;
                dispEngRpm(0, 0);
            }
        }


        engine_disp_cnt = current_time;
    }

    /* 速度表示 */
    if(0 == disp_spd_method){       /* GPS+ENG */
        /* 速度が低いときはGPS, 高くなったらENG */
        if(clutch_bite){
            if(gnss.available()){
                if(40.0f > gnss.gps_module.speed.kmph()){
                    clutch_bite = false;    /* 切断 */
                }
            }else if(2250 > eng->spd_rpm){    /* 回転数低くなった */
                clutch_bite = false;    /* 切断 */
            }
        }else{
            if(gnss.available()){
                if(45.0f <= gnss.gps_module.speed.kmph()){
                    /* GPS速度が高ければ */
                    clutch_bite = true;     /* 接続 */
                }
            }else if(2800 <= eng->spd_rpm){     /* GPS取れなくても流石にこの回転数を超えたら */
                clutch_bite = true;     /* 接続 */
            }


            if((2300 <= eng->spd_rpm) && gnss.available() && (45.0f <= gnss.gps_module.speed.kmph())){
                clutch_bite = true;     /* 接続 */
            }else if(2800 <= eng->spd_rpm){     /* GPS取れなくても流石にこの回転数を超えたら */
                clutch_bite = true;     /* 接続 */
            }
        }

        if(!eng->available()){
            clutch_bite = false;
        }
    }else{      /* GPS */

        clutch_bite = false;

    }

    uint8_t spd;
    if(clutch_bite){    /* クラッチ繋がっているのでエンジン回転数速度 */
        spd = (uint8_t)eng->spd_kmph;
    }else{  /* クラッチ繋がっていないのでGPS速度 */
        if(gnss.available()){
            spd = (uint8_t)gnss.currentval.speed_kmph;
        }else{
            spd = 0;
        }
    }

    uint8_t pdl_ratio_a = stMdlCan_B0_RxVal.PdlAcc/2;
    uint8_t pdl_ratio_b = stMdlCan_B0_RxVal.PdlBrk/2;
    uint8_t str_ratio = stMdlCan_B0_RxVal.Str/2;

    if(((((disp_spd != spd)||(disp_pdl[0] != pdl_ratio_a)||(disp_pdl[1] != pdl_ratio_b)||(disp_pdl[2] != str_ratio)) && (spd_disp_cnt + DISP_SPD_UPDATE_CYCLE < current_time))||disp_init) ){
        canvas_spd.fillCanvas(G0);

        canvas_spd.setTextFont(7);
        canvas_spd.setTextSize(2);
        canvas_spd.setTextColor(G15, G0);
        
        char str_tmp[30];
        sprintf(str_tmp, "%d", spd);
        
        canvas_spd.setTextDatum(TR_DATUM);
        canvas_spd.drawString(str_tmp, CANVAS_SPD_W-80, 0);
        
        canvas_spd.setTextFont(2);
        canvas_spd.setTextSize(2);
        canvas_spd.drawString("km/h", CANVAS_SPD_W-10, CANVAS_SPD_H-65);
        canvas_spd.setTextDatum(TL_DATUM);


        /* アクセル */
        canvas_spd.fillRect(CANVAS_SPD_W/2+10, CANVAS_SPD_H-30, CANVAS_SPD_W/2-10, 15, G0);     /* 白クリア */
        canvas_spd.drawRect(CANVAS_SPD_W/2+10, CANVAS_SPD_H-30, CANVAS_SPD_W/2-10, 15, G15);    /* 枠 */
        canvas_spd.fillRect(CANVAS_SPD_W/2+10, CANVAS_SPD_H-30, (int32_t)((float)pdl_ratio_a/100.0f*(float)((float)CANVAS_SPD_W/2.0f-10.0f)), 15, G15);

        /* ブレーキ */
        canvas_spd.fillRect(0, CANVAS_SPD_H-30, CANVAS_SPD_W/2-10, 15, G0);     /* 白クリア */
        canvas_spd.drawRect(0, CANVAS_SPD_H-30, CANVAS_SPD_W/2-10, 15, G15);    /* 枠 */
        canvas_spd.fillRect((int32_t)(((float)CANVAS_SPD_W/2.0f-10.0f) - (float)pdl_ratio_b/100.0f*((float)CANVAS_SPD_W/2.0f-10.0f)), CANVAS_SPD_H-30, (int32_t)((float)pdl_ratio_b/100.0f*((float)CANVAS_SPD_W/2.0f-10.0f)), 15, G15);
        
        /* ステアリング */
        canvas_spd.fillRect(0, CANVAS_SPD_H-15, CANVAS_SPD_W, 15, G0);     /* 白クリア */
        canvas_spd.drawLine(0, CANVAS_SPD_H-7, CANVAS_SPD_W, CANVAS_SPD_H-7, 2, G15);    /* 枠 */
        uint16_t cnt = CANVAS_SPD_W - (uint8_t)((float)CANVAS_SPD_W*(float)str_ratio/100.0f);
        uint8_t th = 5;
        if(th > cnt){
            cnt = th;
        }else if((CANVAS_SPD_W - th) < cnt){
            cnt = CANVAS_SPD_W - th;
        }
        canvas_spd.drawLine(cnt, CANVAS_SPD_H-7-5, cnt, CANVAS_SPD_H-7+5, th, G15);

        /* 描画実行 */
        canvas_spd.pushCanvas(40+CANVAS_ENGINE_W+10, LCD_HEADER_H + 320, UPDATE_MODE_DU4);

        disp_spd = spd;
        disp_pdl[0] = pdl_ratio_a;
        disp_pdl[1] = pdl_ratio_b;
        disp_pdl[2] = str_ratio;

        spd_disp_cnt = current_time;
    }



    if((timer_state != current_timer_state) || disp_init){
        canvas_sta.fillCanvas(G0);

        switch(current_timer_state){
        case LapTimer::TimState::STOP :
            canvas_sta.fillRect(5, 5, 70, 70, G15);   /* 停止 */
            break;
        case LapTimer::TimState::STANDBY :
            canvas_sta.fillRect(15, 5, 20, 70, G15);   /* 一時停止 */
            canvas_sta.fillRect(45, 5, 20, 70, G15);
            break;
        case LapTimer::TimState::COUNTING :
            canvas_sta.fillTriangle(10, 10, 10, 70, 70, 40, G15);   /* 計測中 */
            break;
        }

        canvas_sta.pushCanvas(850, LCD_HEADER_H + 30, UPDATE_MODE_A2);
    }


    timer_state = current_timer_state;

    last_sec = sec;

    return CONT;
}

void PageLapTimeCnt::dispEngRpm(uint16_t current_rpm, uint16_t peak_rpm){

    canvas_engine.fillCanvas(G0);

    canvas_engine.setTextFont(7);
    canvas_engine.setTextSize(2);
    canvas_engine.setTextColor(G15, G0);
    
    char str_tmp[30];
    sprintf(str_tmp, "%5d", current_rpm);
    
    canvas_engine.setTextDatum(TR_DATUM);
    canvas_engine.drawString(str_tmp, CANVAS_ENGINE_W-80, 0);
    
    canvas_engine.setTextFont(2);
    canvas_engine.setTextSize(2);
    canvas_engine.drawString("rpm", CANVAS_ENGINE_W-10, CANVAS_ENGINE_H);
    canvas_engine.setTextDatum(TL_DATUM);

    canvas_engine.fillRect(30, CANVAS_ENGINE_H-10, CANVAS_ENGINE_W-90, 10, G0);     /* 白クリア */
    canvas_engine.drawRect(30, CANVAS_ENGINE_H-10, CANVAS_ENGINE_W-90, 10, G15);    /* 枠 */
    if(0 < current_rpm){
        uint8_t spd_ratio = current_rpm*100/eng->getMaxSpd();    /* 最大回転数と比較した割合 0～100 */
        canvas_engine.fillRect(20, CANVAS_ENGINE_H-10, spd_ratio*(CANVAS_ENGINE_W-80)/100, 10, G15);
    }

    canvas_engine.pushCanvas(40, LCD_HEADER_H + 320, UPDATE_MODE_DU4);
}
