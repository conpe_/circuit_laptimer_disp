/*  */

#include "PageObj.h"

/* 計測画面 */
PageLapTimeCnt page_laptime_cnt("LapTimer");
PageLapTimeLog page_laptime_log("LapTimer-Log");

/* メニュー内容 */
PageLog page_log("Log");                                /* ログ画面 */
PageInfo page_info("Info");
PagePowerOff page_power_off("PowerOff");

PageSetSensMac page_set_sensmac("Sensor pairing");      /* センサのMACアドレス設定 */
PagePreferenceMenu page_menu_preference("Preference"); /* 設定メニュー */

/* メニュー */
/* (タイトルを取得するため、他のメニューオブジェクトの生成後に生成する) */
PageSelectMenuTop page_menu("Menu");            /* トップメニュー */

