#include "PageLog.h"

#include "PageObj.h"
#include "Session.h"

#define DISP_MAX_LAP 99     /* 表示最大ラップ数 */
#define DISP_NUM_POS_X 62   /* 数字表示位置 */


PageLog::PageLog(const char* title):
                            Page(title),
                            select_state(0),
                            select_idx_date(0),
                            select_idx_session(0),
                            canvas_date(&M5.EPD),
                            canvas_session(&M5.EPD),
                            canvas_info_date(&M5.EPD),
                            canvas_info_session(&M5.EPD){
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("Sel");
    button[2]->setLabel("Next");
}

PageLog::~PageLog(void){

}

/* メニュー入ったときの処理 */
int PageLog::entryProc(void){
    data_to_nextpage.clear();

    session_log_date_list = LapTimerLogMgr::listSessionLogDate();
    
    canvas_date.createCanvas(250, LCD_MAIN_H);
    canvas_date.fillCanvas(G0);
    canvas_session.createCanvas(540, LCD_MAIN_H);
    canvas_session.fillCanvas(G0);
    canvas_info_date.createCanvas(400, LCD_MAIN_H/3);
    canvas_info_date.fillCanvas(G0);
    canvas_info_session.createCanvas(400, LCD_MAIN_H*2/3);
    canvas_info_session.fillCanvas(G0);

    return 0;
}

/* メニューを抜けるときの処理 */
int PageLog::exitProc(void){

    canvas_date.deleteCanvas();
    canvas_session.deleteCanvas();
    canvas_info_date.deleteCanvas();
    canvas_info_session.deleteCanvas();

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageLog::updateProc(Page** next_page){
    bool update_date = false;
    bool update_session = false;

    // 状態遷移
    switch(select_state){
    case 0: /* 日付選択 */
        if(button[0]->wasPressed() || button[0]->pressedFor(1000)){
            if(select_idx_date>0){
                --select_idx_date;
            }else{
                select_idx_date = session_log_date_list.size() - 1;
            }
            update_date = true;
        }
        if(button[1]->pressedFor(1000)){
            button[1]->reset();
            return Page::e_state::BACK;
        }
        if(button[1]->wasReleased()){
            select_state = 1;   /* セッション選択へ */
            update_session = true;
        }
        if(button[2]->wasPressed() || button[2]->pressedFor(1000)){
            if((session_log_date_list.size() - 1) > select_idx_date){
                ++select_idx_date;
            }else{
                select_idx_date = 0;
            }
            update_date = true;
        }
        break;
    case 1: /* セッション選択 */

        if(button[0]->wasPressed() || button[0]->pressedFor(1000) ){
            if(select_idx_session>0){
                --select_idx_session;
            }else{
                select_idx_session = sessions_str.size() - 1;
            }
            update_session = true;
        }
        if(button[1]->pressedFor(1500)){
            button[1]->reset();
            select_state = 0;
            update_session = true;      /* カーソルを消すために表示更新 */
        }
        if(button[1]->wasReleased()){
            data_to_nextpage.clear();

            /* 次の画面にログのフォルダとファイル名を渡す */
            std::string selected_date;
            if(session_log_date_list.size() > select_idx_date){
                selected_date = std::string(session_log_date_list[session_log_date_list.size()-1-select_idx_date]);
                data_to_nextpage.push_back(selected_date);
            }
            std::string selected_log;
            if(sessions_str.size() > select_idx_session){
                selected_log = std::string(sessions_str[sessions_str.size() - select_idx_session - 1]);
                data_to_nextpage.push_back(selected_log);
            }

            /* データ選択が正しければラップタイム表示へ */
            if(2 == data_to_nextpage.size()){
                *next_page = &page_laptime_log;
                return Page::e_state::NEXT;
            }
        }
        if(button[2]->wasPressed() || button[2]->pressedFor(1000)){
            if((sessions_str.size() - 1) > select_idx_session){
                ++select_idx_session;
            }else{
                select_idx_session = 0;
            }
            update_session = true;
        }
        break;
    }

    if(disp_init){
        update_date = true;
    }

    #define LIST_ROW_NUM 8
    #define LIST_STRING_FONT 2
    #define LIST_STRING_SIZE 3
    uint16_t print_start_idx;   /* リストの表示開始位置 */
    uint16_t cursor_idx;        /* カーソル位置 */

    /* 日付リスト表示更新 */
    if(update_date){

        if((LIST_ROW_NUM/2 >= select_idx_date) || (LIST_ROW_NUM>=session_log_date_list.size())){
            print_start_idx = 0;
            cursor_idx = select_idx_date;
        }else if(session_log_date_list.size()-LIST_ROW_NUM/2-1 < select_idx_date){
            print_start_idx = session_log_date_list.size() - LIST_ROW_NUM;
            cursor_idx = select_idx_date - (session_log_date_list.size() - LIST_ROW_NUM);
        }else{
            print_start_idx = select_idx_date - LIST_ROW_NUM/2;
            cursor_idx = LIST_ROW_NUM/2;
        }

        canvas_date.fillCanvas(G0);
        canvas_date.setTextFont(LIST_STRING_FONT);
        canvas_date.setTextColor(G15, G0);
        
        canvas_date.setTextFont(4);
        canvas_date.setTextSize(2);
        canvas_date.drawString("Date", 10, 10);

        canvas_date.setTextFont(LIST_STRING_FONT);
        canvas_date.setTextSize(LIST_STRING_SIZE);

        log_i("<Dates>");
        for(int i=0; (LIST_ROW_NUM>i) && (session_log_date_list.size()>i); ++i){
            char tmp[32];
            if(i==cursor_idx){
                tmp[0] = '>';
            }else{
                tmp[0] = ' ';
            }
            sprintf(&tmp[1], "%s", session_log_date_list[session_log_date_list.size()-1-(i+print_start_idx)].c_str());
            canvas_date.drawString(tmp, 20, 60 + i*50);
            log_i("> %d %s", i, tmp);
        }

        canvas_date.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_A2);

        laps.clear();
        best_times.clear();
        if(session_log_date_list.size() > select_idx_date){
            sessions_str = LapTimerLogMgr::listSession(session_log_date_list[session_log_date_list.size()-1-select_idx_date]);
            std::vector<Session> ss = LapTimerLogMgr::loadSessionLogsInADay(session_log_date_list[session_log_date_list.size()-1-select_idx_date]);
            uint8_t lap_tmp;
            uint32_t besttime_tmp;
            for(auto itr=ss.begin(); itr!=ss.end(); ++itr){
                itr->sessionInfo(nullptr, nullptr, &lap_tmp);
                if(0==itr->bestLapInfo(&besttime_tmp)){
                    besttime_tmp = 0;
                }

                laps.push_back(lap_tmp);
                best_times.push_back(besttime_tmp);
            }
        }
        select_idx_session = 0;
        update_session = true;
    }
    /* セッションリスト表示更新 */
    if(update_session){

        if((LIST_ROW_NUM/2 >= select_idx_session) || (LIST_ROW_NUM>=sessions_str.size())){
            print_start_idx = 0;
            cursor_idx = select_idx_session;
        }else if(sessions_str.size()-LIST_ROW_NUM/2-1 < select_idx_session){
            print_start_idx = sessions_str.size() - LIST_ROW_NUM;
            cursor_idx = select_idx_session - (sessions_str.size() - LIST_ROW_NUM);
        }else{
            print_start_idx = select_idx_session - LIST_ROW_NUM/2;
            cursor_idx = LIST_ROW_NUM/2;
        }

        canvas_session.fillCanvas(G0);
        canvas_session.setTextColor(G15, G0);
        
        canvas_session.setTextFont(4);
        canvas_session.setTextSize(2);
        canvas_session.drawString("Session", 10, 10);

        canvas_session.setTextFont(LIST_STRING_FONT);
        canvas_session.setTextSize(LIST_STRING_SIZE);
        canvas_session.drawString("Lap", 280, 10);
        canvas_session.drawString("Best", 380, 10);

        canvas_session.setTextFont(LIST_STRING_FONT);
        canvas_session.setTextSize(LIST_STRING_SIZE);

        log_i("<Sessions>");
        for(int i=0; (LIST_ROW_NUM>i) && (sessions_str.size()>i); ++i){
            char tmp[32];
            tm start_time;
            uint8_t sector_num;

            String s_str = sessions_str[sessions_str.size()-1-(i+print_start_idx)].c_str();
            s_str = s_str.substring(0,2) + ":" + s_str.substring(2,4) + ":" + s_str.substring(4,6);
            const char* session_name = s_str.c_str();

            if(i==cursor_idx && 1==select_state){
                tmp[0] = '>';
            }else{
                tmp[0] = ' ';
            }

            char best_time_str[10];
            
            uint32_t bt = best_times[sessions_str.size()-1-(i+print_start_idx)];
            if(0!=bt){
                sprintf(best_time_str, "%01d:%02d:%03d", (bt/1000)/60, (bt/1000)%60, bt%1000);
            }else{
                sprintf(best_time_str, "-:--:---");
            }        
            sprintf(&tmp[1], "%2d %s %3d %s", sessions_str.size() - (i+print_start_idx), session_name, laps[sessions_str.size()-1-(i+print_start_idx)], best_time_str);
            canvas_session.drawString(tmp, 10, 60 + i*50);

            log_i("%d %s", i, tmp);
        }

        canvas_session.pushCanvas(250, LCD_HEADER_H, UPDATE_MODE_A2);
    }





    return Page::e_state::CONT;
}

