/* GUI */

//#pragma once
#ifndef LAPTIMECURRENTLOG_H
#define LAPTIMECURRENTLOG_H

#include "Page.h"
#include "LapTimer.h"
#include "Session.h"

/* ページのテンプレート */
class PageLapTimeLog:public Page{
public:
    PageLapTimeLog(const char* title = "LAP TIMER");
    ~PageLapTimeLog(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);   /* こっちを継承 */

private:
    uint8_t disp_offset;
    uint8_t logged_lap;
    Session session;
    Session *psession;
};


#endif
