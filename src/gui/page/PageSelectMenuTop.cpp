#include "PageSelectMenuTop.h"

#include "PageObj.h"


PageSelectMenuTop::PageSelectMenuTop(const char* title):PageSelectMenu(title){

    menus.push_back(MenuItem(&page_laptime_cnt, "[Back]"));
    menus.push_back(MenuItem(&page_menu_preference));
    menus.push_back(MenuItem(&page_log));
    menus.push_back(MenuItem(&page_info));
    
}

PageSelectMenuTop::~PageSelectMenuTop(void){

}


