/* GUI */

#pragma once

#include "Page.h"
#include <vector>
#include <string>

#include "PreferenceMenuItem.h"

#define PREF_ROW (LCD_MAIN_H/PREF_H)

/* ページのテンプレート */
class PagePreferenceMenu:public Page{
public:
    PagePreferenceMenu(const char* title = "PREFERENCES");
    ~PagePreferenceMenu(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);

protected:
    std::vector<PreferenceMenuItem*> pref_list;
    uint8_t sel_idx;
    uint8_t sel_idx_last;
    uint8_t disp_offset_last;

    enum State{
        SEL_ITEM,   /* アイテム選択中 */
        CHANGE_VAL  /* 値変更中 */
    };
    State state;

    void updateDisp(void);  /* 描画更新 */

    M5EPD_Canvas_Semaphore canvas_sel;
    M5EPD_Canvas_Semaphore canvas_BlankItem;
};

