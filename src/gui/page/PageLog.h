/* GUI */

//#pragma once
#ifndef PAGELOG_H
#define PAGELOG_H

#include "Page.h"
#include "LapTimerLogMgr.h"

/* フラッシュに保存されているログの表示 */
/* SDカードへの書き出しも */
class PageLog:public Page{
public:
    PageLog(const char* title = "LAP TIMER LOG");
    ~PageLog(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);   /* こっちを継承 */

private:
    std::vector<std::string> session_log_date_list; /* ログのある日付リスト(フォルダ) */
    std::vector<std::string> sessions_str;          /* セッションリスト(選択した日のセッション) */
    std::vector<uint8_t> laps;                     /* ベストラップリスト(選択した日のセッション) */
    std::vector<uint32_t> best_times;               /* ベストタイムリスト(選択した日のセッション) */

    uint8_t select_state;               /* 0:日付選択 1:セッション選択 */
    uint16_t select_idx_date;
    uint16_t select_idx_session;

    M5EPD_Canvas_Semaphore canvas_date;
    M5EPD_Canvas_Semaphore canvas_session;
    M5EPD_Canvas_Semaphore canvas_info_date;
    M5EPD_Canvas_Semaphore canvas_info_session;
    
};


#endif
