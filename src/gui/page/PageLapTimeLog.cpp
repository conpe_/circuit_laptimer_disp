#include "PageLapTimeLog.h"

#include "PageObj.h"


PageLapTimeLog::PageLapTimeLog(const char* title):Page(title){
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("Back");
    button[2]->setLabel("Next");
}

PageLapTimeLog::~PageLapTimeLog(void){

}

/* メニュー入ったときの処理 */
int PageLapTimeLog::entryProc(void){
    logged_lap = 0;
    disp_offset = 0;

    log_i("passed data num %d", data_to_nextpage.size());
    
    /* 引き継ぎデータ2つあり */
    if(2 <= data_to_nextpage.size()){
        log_i("passed data 0: %s", data_to_nextpage[0].c_str());
        log_i("passed data 1: %s", data_to_nextpage[1].c_str());

        session.clear();
        if(0 == LapTimerLogMgr::loadSession(data_to_nextpage[0], data_to_nextpage[1], &session)){
            psession = &session;
        }else{
            psession = nullptr;
        }
    }else{
        /* 引数なしなら現在セッション */
        psession = lap_timer.getSession();
    }

    return 0;
}

/* メニューを抜けるときの処理 */
int PageLapTimeLog::exitProc(void){

    data_to_nextpage.clear();

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageLapTimeLog::updateProc(Page** next_page){
    bool page_update = false;

    // 情報取得
    uint8_t logged_lap_tmp = 0;     /* 記録されているラップ数 */
    uint32_t best_time = 0;
    
    /* 画面開いてからセッションが始まった時用 */
    if(0 == data_to_nextpage.size()){
        /* 引数なしなら現在セッション */
        psession = lap_timer.getSession();
    }

    if(psession){
        psession->sessionInfo(nullptr, nullptr, &logged_lap_tmp);
        psession->bestLapInfo(&best_time);
    }else{
        logged_lap_tmp = 0;
        best_time = 0;
    }
    /* 6Lapずつ3列で表示 */
    #define DISP_ROWS (8)
    #define DISP_COLS (3)
    uint8_t lines = (logged_lap_tmp + DISP_ROWS - 1)/DISP_ROWS;   /* 列数 */
    uint8_t disp_offset_max = ((lines>DISP_COLS)?(lines-DISP_COLS):0);
    uint8_t lines_last = (logged_lap + DISP_ROWS - 1)/DISP_ROWS;   /* 列数 */
    uint8_t disp_offset_max_last = ((lines_last>DISP_COLS)?(lines_last-DISP_COLS):0);

    /* 表示更新 */
    if(logged_lap != logged_lap_tmp){
        page_update = true;

        /* 表示位置変わった */
        if(
            (lines != lines_last)    /* 列数増えた */
            && (disp_offset == disp_offset_max_last)    /* 最新ラップ位置を表示してた */
        ){
            disp_offset = disp_offset_max;  /* 新しい最終位置へ */
        }

        logged_lap = logged_lap_tmp;
    }


    // 状態遷移
    // left
    if(button[0]->wasPressed()){
        if(0<disp_offset){
            disp_offset--;
        }else{
            disp_offset = disp_offset_max;
        }

        page_update = true;
    }

    // center
    if(button[1]->wasPressed()){
        return Page::e_state::BACK; /* 前の画面 = 計測画面へ戻る */
    }

    // right
    if(button[2]->wasPressed()){
        if(disp_offset_max > disp_offset){
            disp_offset++;
        }else{
            disp_offset = 0;
        }
        page_update = true;
    }


    if(page_update || disp_init){
        uint8_t disp_lap;
        uint32_t disp_time;

        // 表示頭出し
        disp_lap = disp_offset*DISP_ROWS + 1;

        // 表示
        for(uint8_t col=0; DISP_COLS>col; ++col){
            for(uint8_t row=0; DISP_ROWS>row; ++row){
                bool disp_ok = false;
                if(psession){
                    disp_ok = (0 != psession->lapInfo(disp_lap, &disp_time));
                }
                if(disp_ok){
                    char str_tmp[20];

                    /* ラップ数 */
                    canvas_main.setTextFont(1);
                    canvas_main.setTextSize(3);
                    canvas_main.setTextColor(G15, G0);
                    sprintf(str_tmp, "%3d", disp_lap);
                    canvas_main.drawString(str_tmp, col*310+15, row*58+5);

                    /* タイム */
                    canvas_main.setTextFont(7);
                    canvas_main.setTextSize(1);
                    if(best_time == disp_time){
                        canvas_main.setTextColor(G0, G15);      /* ベストタイムは強調表示 */
                    }else{
                        canvas_main.setTextColor(G15, G0);
                    }
                    if(10>(disp_time/1000/60)){
                        sprintf(str_tmp, "%01d:%02d:%03d",(disp_time/1000/60),(disp_time/1000)%60,disp_time%1000);
                    }else{
                        sprintf(str_tmp, "%01d:%02d:%03d",9,99,999);
                    }
                    canvas_main.drawString(str_tmp, col*310+15+70, row*58+5);
                    
                    ++disp_lap;
                }else{
                    /* ラップ数 */
                    canvas_main.setTextFont(1);
                    canvas_main.setTextSize(3);
                    canvas_main.setTextColor(G15, G0);
                    canvas_main.drawString(" --", col*310+15, row*58+5);

                    /* タイム */
                    canvas_main.setTextFont(7);
                    canvas_main.setTextSize(1);
                    canvas_main.setTextColor(G15, G0);
                    canvas_main.drawString("-:--:---", col*310+15+70, row*58+5);
                }
            }
        }


        /* スクロールバー */
        #if 0
        if(logged_lap>2){
            canvas_main.fillRect(
                LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
                SCROLLBAR_W, SCROLLBAR_H,
                G0
            );
            canvas_main.drawRect(
                LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
                SCROLLBAR_W, SCROLLBAR_H,
                G15
            );
            canvas_main.fillRect(
                LCD_W-SCROLLBAR_W-1, (int)((float)SCROLLBAR_POS_TOP + (float)SCROLLBAR_H * disp_lap_offset / (logged_lap-1)), 
                SCROLLBAR_W, (int)( ((float)SCROLLBAR_H * (disp_lap_offset+1) / (logged_lap-1)) - ((float)SCROLLBAR_H * disp_lap_offset / (logged_lap-1)) ),
                G15
            );
        }else{
            canvas_main.fillRect(
                LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
                SCROLLBAR_W, SCROLLBAR_H,
                G15
            );
        }
        #endif


        canvas_main.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_A2);    /* DU4: 120ms, 4階調 */
    }
    
    return CONT;
}

