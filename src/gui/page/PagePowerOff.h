#pragma once

#include "Page.h"

/* ページのテンプレート */
class PagePowerOff:public Page{
public:
    PagePowerOff(const char* title = "PowerOff");
    virtual ~PagePowerOff(void);
protected:
    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);

    static const uint32_t POWER_DOWN_TIME = 2000;
    uint32_t enter_time;
};

