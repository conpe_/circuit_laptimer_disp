#include "PageInfo.h"
#include <M5EPD.h>

#include "PageObj.h"
#include "BtSerialCmdMaster.h"


PageInfo::PageInfo(const char* title):Page(title){
    
    button[0]->setLabel("");
    button[1]->setLabel("BACK");
    button[2]->setLabel("OFF");
}

PageInfo::~PageInfo(void){

}

/* メニュー入ったときの処理 */
int PageInfo::entryProc(void){

    return 0;
}

/* メニューを抜けるときの処理 */
int PageInfo::exitProc(void){

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageInfo::updateProc(Page** next_page){

    if(button[0]->wasPressed()){
    }

    if(button[1]->wasPressed()){
        return e_state::BACK;
    }

    if(button[2]->wasPressed()){
        /* 電源OFF */
        *next_page = &page_power_off;
        return e_state::NEXT;
    }

    /* 選択変化 */
    updateDisp();   /* 描画更新 */
    

    
    uint32_t current_time = millis();
    if((last_update_time_main + 200 < current_time) || disp_init){
        canvas_main.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_A2);    /* DU4: 120ms, 4階調 */
        last_update_time_main = current_time;
    }

    return e_state::CONT;
}

/* 描画更新 */
void PageInfo::updateDisp(void){
    canvas_main.setTextFont(4);
    canvas_main.setTextSize(2);
    canvas_main.setTextColor(G15, G0);
    canvas_main.setTextArea(10, 20, LCD_W, LCD_H);

    canvas_main.printf("getBatteryVoltage = %d[mV]\n", M5.getBatteryVoltage());
    canvas_main.printf("SensConn = %d\n", bt_cmd.isConnected());
    canvas_main.printf("SensBatt = %1.3fv\n", (float)SensBatt.Voltage/1000.0f);

}
