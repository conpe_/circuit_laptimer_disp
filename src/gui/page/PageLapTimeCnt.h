/* GUI */

//#pragma once
#ifndef LAPTIMECNT_H
#define LAPTIMECNT_H

#include "Page.h"
#include "LapTimer.h"

/* ページのテンプレート */
class PageLapTimeCnt:public Page{
public:
    PageLapTimeCnt(const char* title = "LAP TIMER");
    ~PageLapTimeCnt(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);   /* こっちを継承 */

private:
    uint8_t disp_lap_offset;
    bool reject_start_flg;
    LapTimer::TimState timer_state;
    static const uint32_t DISP_LOCK_TIME = 4000;        /* 表示を固定する時間 */
    static const uint32_t DISP_CURRENTTIME_UPDATE_CYCLE = 250;  /* 表示を更新する間隔 */
    static const uint32_t DISP_ENGINE_UPDATE_CYCLE = 300;
    static const uint32_t DISP_SPD_UPDATE_CYCLE = 300;
    uint8_t last_sector;            /* セクター */

    /* 表示 */
    M5EPD_Canvas_Semaphore canvas_currenttime;
    M5EPD_Canvas_Semaphore canvas_lasttime;
    M5EPD_Canvas_Semaphore canvas_besttime;
    M5EPD_Canvas_Semaphore canvas_Tire;
    M5EPD_Canvas_Semaphore canvas_engine;
    M5EPD_Canvas_Semaphore canvas_spd;
    M5EPD_Canvas_Semaphore canvas_sta;

    uint8_t last_lap;
    uint8_t best_lap;
    uint32_t best_time;
    uint8_t last_sec;

    uint32_t currentlap_disp_cnt;
    uint32_t lasttime_disp_cnt;
    uint32_t engine_disp_cnt;
    uint32_t spd_disp_cnt;

    uint8_t disp_spd;
    uint8_t disp_spd_method;
    uint8_t disp_pdl[3];

    bool clutch_bite;

    void dispEngRpm(uint16_t current_rpm, uint16_t peak_rpm);
    bool eng_running;
};


#endif
