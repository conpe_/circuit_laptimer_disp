#include "PageSetSensMac.h"
#include "PageObj.h"

#include "DataMem.h"

PageSetSensMac::PageSetSensMac(const char* title):
                Page(title),
                canvas(&M5.EPD){
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("OK");
    button[2]->setLabel("Next");
}

PageSetSensMac::~PageSetSensMac(void){

}

/* メニュー入ったときの処理 */
int PageSetSensMac::entryProc(void){
    char c_tmp[30];

    canvas.createCanvas(600, 200);
    canvas.fillCanvas(G0);

    DataMem::read(MEM_BT0, &current_val[0]);
    DataMem::read(MEM_BT1, &current_val[1]);
    DataMem::read(MEM_BT2, &current_val[2]);
    DataMem::read(MEM_BT3, &current_val[3]);
    DataMem::read(MEM_BT4, &current_val[4]);
    DataMem::read(MEM_BT5, &current_val[5]);

    memcpy(set_val, current_val, 6);

    canvas_main.fillCanvas(G0);
    canvas_main.setTextFont(4);  /* きれいなフォント */
    canvas_main.setTextSize(2);
    canvas_main.setTextColor(G15, G0);
    canvas_main.drawString("MAC address", 5, 30);

    canvas_main.setTextFont(1);
    canvas_main.setTextSize(6);
    canvas_main.setTextDatum(0);
    /* 現在値 */
    for(int i=0; 12>i; ++i){
        canvas_main.setTextColor(G15, G0);
        sprintf(c_tmp, "%01X", (current_val[i/2]>>(i%2?0:4))&0x0F );
        canvas_main.drawString(c_tmp, 20 + i*36 + i/2*16, 100);
    }
    canvas_main.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_A2);    /* DU4: 120ms, 4階調 */


    mode = 0;
    sel_menu = 13;

    return 0;
}

/* メニューを抜けるときの処理 */
int PageSetSensMac::exitProc(void){
    canvas.deleteCanvas();

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageSetSensMac::updateProc(Page** next_page){
    bool page_update = disp_init;
    char c_tmp[30];
    
    if(button[0]->wasPressed() || button[0]->pressedFor(1000)){
        page_update = true;
        switch(mode){
        case 0: /* カーソル移動 */
            if(0<sel_menu){
                --sel_menu;
            }else{
                sel_menu = 13;
            }
            break;
        case 1: /* 数値変更 */
            uint8_t tmp;
            tmp = (set_val[sel_menu/2]>>((sel_menu%2)?0:4))&0x0F;

            if(0<tmp){
                --tmp;
            }else{
                tmp = 15;
            }
            
            set_val[sel_menu/2] = (set_val[sel_menu/2]& ~(0x0F<<((sel_menu%2)?0:4))) | (tmp<<((sel_menu%2)?0:4));

            break;
        }
    }

    if(button[1]->pressedFor(1000)){
        return BACK;
    }
    if(button[1]->wasPressed()){
        page_update = true;
        switch(mode){
        case 0: /* カーソル移動 */
            /* 選択によって確定して戻るか，確定せず戻るか，数値変更モードに移行するか */
            switch(sel_menu){
            case 12:     /* set */
                DataMem::write(MEM_BT0, set_val[0]);
                DataMem::write(MEM_BT1, set_val[1]);
                DataMem::write(MEM_BT2, set_val[2]);
                DataMem::write(MEM_BT3, set_val[3]);
                DataMem::write(MEM_BT4, set_val[4]);
                DataMem::write(MEM_BT5, set_val[5]);
                return BACK;
            case 13:     /* back */
                return BACK;
            default:     /* 数値 */
                mode = 1;
            }

            break;
        case 1: /* 数値変更 */
            mode = 0;
            break;
        }
    }

    if(button[2]->wasPressed() || button[2]->pressedFor(1000)){
        page_update = true;
        switch(mode){
        case 0: /* カーソル移動 */
            if(13>sel_menu){
                ++sel_menu;
            }else{
                sel_menu = 0;
            }
            break;
        case 1: /* 数値変更 */
            uint8_t tmp;
            tmp = (set_val[sel_menu/2]>>((sel_menu%2)?0:4))&0x0F;

            if(15>tmp){
                ++tmp;
            }else{
                tmp = 0;
            }
            
            set_val[sel_menu/2] = (set_val[sel_menu/2]& ~(0x0F<<((sel_menu%2)?0:4))) | (tmp<<((sel_menu%2)?0:4));

            break;
        }
    }



    /* 変更値 */
    canvas.setTextFont(1);
    canvas.setTextSize(6);
    canvas.setTextDatum(0);
    for(int i=0; 12>i; ++i){
        canvas.setTextColor((sel_menu==i)?G0:G15, (sel_menu==i)?G15:G0);
        sprintf(c_tmp, "%01X", (set_val[i/2]>>(i%2?0:4))&0x0F );
        canvas.drawString(c_tmp, 20 + i*36 + i/2*16, 0);
    }

    /* セット */
    canvas.setTextFont(4);  /* きれいなフォント */
    canvas.setTextSize(2);
    canvas.setTextColor((12==sel_menu)?G0:G15, (12==sel_menu)?G15:G0);
    canvas.setTextDatum(4);
    canvas.drawString("[set]", LCD_W/2 - 150, 100);
    /* 戻る */
    canvas.setTextFont(4);  /* きれいなフォント */
    canvas.setTextSize(2);
    canvas.setTextColor((13==sel_menu)?G0:G15, (13==sel_menu)?G15:G0);
    canvas.setTextDatum(4);
    canvas.drawString("[back]", LCD_W/2 + 150, 100);


    if(page_update){
        canvas.pushCanvas(0, LCD_HEADER_H + 150, UPDATE_MODE_A2);    /* DU4: 120ms, 4階調 */
    }

    return CONT;
}
