/* ページのテンプレート */

#include <M5EPD.h>
#include "Page.h"
#include "PageObj.h"
#include "buzz.h"
#include "LapTimer.h"
#include "BtSerialCmdMaster.h"
#include "LapTimerLogMgr.h"
#include "Gnss.h"
#include "LapSensorMgr.h"



#define CHAR_W 15        // 1文字の幅

#define LCD_BUTTON_LONGPRESSTIME 1500    /* 長押し判定時間[ms] */


extern SemaphoreHandle_t   semaphore_spi;

void M5EPD_Canvas_Semaphore::pushCanvas(int32_t x, int32_t y, m5epd_update_mode_t mode){
    xSemaphoreTake(semaphore_spi, portMAX_DELAY);
    M5EPD_Canvas::pushCanvas(x, y, mode);
    xSemaphoreGive(semaphore_spi);
}


uint8_t Page::guruguru_cnt = 0;
bool Page::blink;
uint32_t Page::blink_time;
CmdProcGetSensBatt Page::SensBatt;    /* センサ電源情報 */
uint32_t Page::last_update_time_header = 0;
uint32_t Page::last_update_time_main = 0;
uint32_t Page::last_update_time_footer = 0;
std::vector<std::string> Page::data_to_nextpage;

M5EPD_Canvas_Semaphore Page::canvas_main(&M5.EPD);
M5EPD_Canvas_Semaphore Page::canvas_header(&M5.EPD);
M5EPD_Canvas_Semaphore Page::canvas_footer(&M5.EPD);

/* コンストラクタ */
Page::Page(const char* title):button(PAGE_BUTTON_NUM){
    this->title = title;   /* タイトルをセット */
    button[0] = new DispButton("A", M5EPD_KEY_RIGHT_PIN, true, 10);
    button[1] = new DispButton("B", M5EPD_KEY_PUSH_PIN, true, 10);
    button[2] = new DispButton("C", M5EPD_KEY_LEFT_PIN, true, 10);

    button[0]->setPos(25, LCD_FOOTER_H-BUTTON_H, BUTTON_W, BUTTON_H);
    button[1]->setPos(LCD_W/2-BUTTON_W/2, LCD_FOOTER_H-BUTTON_H, BUTTON_W, BUTTON_H);
    button[2]->setPos(LCD_W-25-BUTTON_W, LCD_FOOTER_H-BUTTON_H, BUTTON_W, BUTTON_H);

}
/* デストラクタ */
Page::~Page(void){
    for(int i=0; PAGE_BUTTON_NUM>i; ++i){
        delete button[i];
    }
}

/* メニュー入ったときの処理 */
int Page::entry(void){
    log_i("Page enter %s", this->title.c_str());

    disp_init = true;

    /* 画面を白く */
    canvas_main.createCanvas(LCD_W, LCD_H);
    canvas_main.fillCanvas(G0);
    canvas_main.pushCanvas(0, LCD_HEADER_H, UPDATE_MODE_GC16);
    last_update_time_main = millis();

    /* ヘッダを切り替え */
    canvas_header.createCanvas(LCD_HEADER_W, LCD_HEADER_H);
    canvas_header.fillCanvas(G0);
    dispTitleBar();
    canvas_header.pushCanvas(0, 0, UPDATE_MODE_A2);
    last_update_time_header = millis();


    /* フッタ切り替え */
    canvas_footer.createCanvas(LCD_FOOTER_W, LCD_FOOTER_H);
    canvas_footer.fillCanvas(G0);
    canvas_footer.pushCanvas(0, LCD_HEADER_H + LCD_H, UPDATE_MODE_A2);
    last_update_time_footer = millis();


    /* ボタン3つ */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        (*itr)->draw();
    }
    /* ボタン3つ */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        (*itr)->reset();
    }

    entryProc();

    return 0;
}

/* メニューを抜けるときの処理 */
int Page::exit(void){
    exitProc();

    canvas_main.deleteCanvas();
    canvas_header.deleteCanvas();
    canvas_footer.deleteCanvas();
    return 0;
}

/* ページ更新 */
Page::e_state Page::update(Page** next_page){
    /* メニュークリア */

    /* ボタン更新 */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        (*itr)->read();

        /* 押したら鳴らす */
        if((*itr)->wasPressed()){
            //buzz.noteSingle(BUZZ_NOTE::BUZZ_NOTE_C4);
            //M5.Speaker.tone(800, 100);
        }
    }
    /* 特殊ボタン */
    /* センター長押しでシャットダウン */
    if(M5.BtnP.pressedFor(4000) && (&page_power_off != this)){
        /* 電源OFF */
        *next_page = &page_power_off;
        return e_state::NEXT;
    }
    /* 電圧監視 */
    if(3400 >= M5.getBatteryVoltage()){
        /* 電源OFF */
        *next_page = &page_power_off;
        return e_state::NEXT;
    }

    
    ++guruguru_cnt;
    if(guruguru_cnt>3){
        guruguru_cnt = 0;
    }

    /* 点滅カウント */
    if(blink_time + 500 < millis()){
        blink = !blink;
        blink_time = millis();
    }
    /* ボタン3つ */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        if((*itr)->wasPressed()){
            blink = 0;
            blink_time = millis();
        }
    }

    
    /* タイトルバーの描画 */
    dispTitleBar();

    /* EPD書き出し */
    /* 表示更新時間がかかるので、何回かに一度 */
    uint32_t current_time = millis();
    if(last_update_time_header + 5000 < current_time || disp_init){
        canvas_header.pushCanvas(0, 0, UPDATE_MODE_A2);             /* ヘッダ部書き出し */
        last_update_time_header = current_time;
    }
    if(last_update_time_footer + 5000 < current_time || disp_init){
        canvas_footer.pushCanvas(0, LCD_HEADER_H + LCD_H, UPDATE_MODE_A2);             /* ヘッダ部書き出し */
        last_update_time_footer = current_time;
    }
    

    /* 各メニューの描画処理 */
    Page::e_state ret = updateProc(next_page);


    disp_init = false;
    return ret;
}

int Page::dispMenuTitle(uint16_t pos_x, uint16_t pos_y){
    /* メニュータイトル */
    canvas_header.drawFastHLine(0,LCD_HEADER_H-1, LCD_W, 1, G15);
    
    canvas_header.setTextFont(1);
    canvas_header.setTextSize(5);
    canvas_header.setTextColor(G15);
    canvas_header.drawString(title.c_str(), pos_x, pos_y);

    return 0;
}

int Page::dispTitleBar(void){

    /* タイトル */
    dispMenuTitle(20, 2);

    /* セクター */
    //if(1<lap_timer.getSectorNum()){
    //    dispSector(440, 10);
    //}

    /* 動作状態 */
    dispMesState(430, 10);

    /* GNSS状態 */
    dispGnssSta(490, 5);

    /* SDカード挿入状態 */
    dispSdState(530, 5);

    /* バッテリ状態 */
    float dmax = 4.35f - 3.2f;
	float delt = M5.getBatteryVoltage()/1000.0f - 3.2f;
	int8_t soc = (delt / dmax) * 100.0f;

    // 上帯：高さ28
    uint8_t batt_level = soc/20;
    dispBattState(580, 4, batt_level);

    /* ラップセンサ関係 */
    LapSensorMgr::TYPE sensor_type = LapSensorMgr::selectedType();
    char strtmp[10];
    if(LapSensorMgr::TYPE::BT == sensor_type){   /* Bluetoothセンサなら表示 */
        /* センサ接続状態 */
        dispAntenna(620, 5, bt_cmd.isConnected()?3:0);

        /* センサバッテリ状態 */
        soc = SensBatt.SocPct;
        static uint16_t cnt = 0;
        static bool con_last = false;
        bool con = bt_cmd.isConnected();
        
        if(con && !con_last){   /* 接続時にすぐ表示されるようにする */
            cnt = 65535;
        }
        con_last = con;

        /* 一定時間ごとに更新 */
        if(cnt>=200){
            SensBatt.update();
            cnt = 0;
        }else{
            ++cnt;
        }
        batt_level = soc/20;
        dispBattState(660, 4, batt_level);
    }else if(LapSensorMgr::TYPE::CAN == sensor_type){
        canvas_header.setTextFont(1);
        canvas_header.setTextSize(4);
        canvas_header.setTextColor(G15, G0);
        canvas_header.setTextDatum(0);
        canvas_header.drawString("CAN", 610, 10);
    }else if(LapSensorMgr::TYPE::GPIO == sensor_type){
        canvas_header.setTextFont(1);
        canvas_header.setTextSize(4);
        canvas_header.setTextColor(G15, G0);
        canvas_header.setTextDatum(0);
        canvas_header.drawString("IO", 620, 10);
    }
    
    dispTime(702, 10);

    return 0;
}



Page::DispButton::DispButton(const char* label, uint8_t pin, uint8_t invert, uint32_t dbTime):
        Button(pin, invert, dbTime),
        initialized_wasReleased(false),
        initialized_pressedFor(false){
    
    setLabel(label);
}

int Page::DispButton::setLabel(const char* label){
    this->label = label;   /* タイトルをセット */

    return 0;
}

int Page::DispButton::draw(void){

    /* ボタン枠 */
    canvas_footer.drawRect(_x, _y, _w, _h, G15);
    /* ボタン文字 */
    canvas_footer.setTextArea(_x + _w/2 - (label.length()*CHAR_W/2), _y + 1, LCD_W, LCD_H);
    canvas_footer.setTextFont(1);
    canvas_footer.setTextSize(2);
    canvas_footer.setTextColor(G15);
    canvas_footer.printf("%s", label.c_str());

    return 0;
}

int Page::DispButton::setPos(int x, int y, int w, int h){
    _x=x;
    _y=y;
    _w=w;
    _h=h;

    return 0;
}

void Page::DispButton::reset(void){
    initialized_wasReleased = false;
    initialized_pressedFor = false;
}

bool Page::DispButton::isLongPress(void){
    return pressedFor(1500);    /* 1500ms以上押しっぱなし */
}


uint8_t Page::DispButton::wasReleased(void){
    uint8_t btn = Button::wasReleased();
    if(initialized_wasReleased){
        return btn;
    }else{
        if(isReleased()){
            initialized_wasReleased = true;
        }
        return false;
    }
}
uint8_t Page::DispButton::pressedFor(uint32_t ms){
    if(initialized_pressedFor){
        return Button::pressedFor(ms);
    }else{
        if(isReleased()){
            initialized_pressedFor = true;
        }
        return false;
    }
}

/* GNSS状態 */
#include "img_gnss_catch.h"
#include "img_gnss_catch_off.h"
void Page::dispGnssSta(uint16_t pos_x, uint16_t pos_y){
    if(gnss.available()){
        canvas_header.drawJpg(img_gnss_catch, IMG_GNSS_CATCH_LEN, pos_x, pos_y);
    }else{
        canvas_header.drawJpg(img_gnss_catch_off, IMG_GNSS_CATCH_OFF_LEN, pos_x, pos_y);
    }
}

/* セクター表示 */
void Page::dispSector(uint16_t pos_x, uint16_t pos_y){
    uint8_t sec = lap_timer.getSector();
    uint8_t sector_num = lap_timer.getSectorNum();
    char str_tmp[64];

    if(sec && (LapTimer::TimState::COUNTING == lap_timer.getState())){
        sprintf(str_tmp, "%01d/%01d", sec, sector_num);
    }else{
        sprintf(str_tmp, "-/%01d", sector_num);
    }
    canvas_header.setTextFont(1);
    canvas_header.setTextSize(4);
    canvas_header.setTextColor(G15, G0);
    canvas_header.drawString(str_tmp, pos_x, pos_y);
}


/* 計測状態 */
void Page::dispMesState(uint16_t pos_x, uint16_t pos_y){
    LapTimer::TimState current_timer_state = lap_timer.getState();

    switch(current_timer_state){

    case LapTimer::TimState::STOP :
        canvas_header.fillRect(pos_x-30/2, pos_y, 30, 30, G0);
        canvas_header.fillRect(pos_x-30/2, pos_y, 30, 30, G15);   /* 停止 */
        break;
    case LapTimer::TimState::STANDBY :
        canvas_header.fillRect(pos_x-30/2, pos_y, 30, 30, G0);
        canvas_header.fillRect(pos_x-12, pos_y, 10, 30, G15);   /* 一時停止 */
        canvas_header.fillRect(pos_x+2, pos_y, 10, 30, G15);
        break;
    case LapTimer::TimState::COUNTING :
        canvas_header.fillRect(pos_x-30/2, pos_y, 30, 30, G0);
        canvas_header.fillTriangle(pos_x-30/2, pos_y, pos_x+30/2, pos_y+30/2, pos_x-30/2, pos_y+30, G15);
        break;
    }

    if(LapTimer::TimState::COUNTING != current_timer_state){
    }else{
    }
}

/* SDカード挿入状態 */
#include "img_sd.h"
#include "img_sd_off.h"
void Page::dispSdState(uint16_t pos_x, uint16_t pos_y){

    if(LapTimerLogMgr::isSdCardAvailable()){
        canvas_header.drawJpg(img_sd, IMG_SD_LEN, pos_x, pos_y);
    }else{
        canvas_header.drawJpg(img_sd_off, IMG_SD_OFF_LEN, pos_x, pos_y);
    }
}

/* バッテリ状態 */
void Page::dispBattState(uint16_t pos_x, uint16_t pos_y, uint8_t batt_level){
    const uint8_t cell_size_w = 23;
    const uint8_t cell_size_h = 7;
    bool charging = false;
    
    /* チャージ済みセル：緑 */
    /* チャージ中セル：緑点滅 */
    /* 未チャージセル：灰色 */
    for(int cell=0; 4>cell; ++cell){
        canvas_header.fillRect(pos_x-cell_size_w/2, pos_y + 6 + (3 - cell) * (cell_size_h+1), cell_size_w, cell_size_h, 
            (batt_level>cell)?G15:(charging&&(batt_level==cell&&(blink))?G15:G0)
        );
    }
    /* バッテリ枠：灰色 */
    canvas_header.drawRect(pos_x-cell_size_w/2 - 2, pos_y + 4, cell_size_w + 4, (cell_size_h+1)*4+3, G15);
    canvas_header.drawRect(pos_x-cell_size_w/4, pos_y, cell_size_w/2, 5, G15);

}


/* センサ接続状態 */
void Page::dispAntenna(uint16_t pos_x, uint16_t pos_y, uint8_t bari){
    const uint8_t bari_h = 10;
    static uint8_t bari_last_10 = 0;

    if(bari*10 > bari_last_10){
        bari_last_10 += 3;
    }else if(bari*10 < bari_last_10){
        bari_last_10 -= 3;
    }else{
        bari_last_10 = bari*10;
    }


    if((bari_last_10/10)>=3){
        canvas_header.fillRect(pos_x-36/2, pos_y, 36, bari_h, G15);
    }else{
        canvas_header.fillRect(pos_x-36/2, pos_y, 36, bari_h, G0);
        canvas_header.drawRect(pos_x-36/2, pos_y, 36, bari_h, G15);
    }
    if((bari_last_10/10)>=2){
        canvas_header.fillRect(pos_x-22/2, pos_y + (2 + bari_h), 22, bari_h, G15);
    }else{
        canvas_header.fillRect(pos_x-22/2, pos_y + (2 + bari_h), 22, bari_h, G0);
        canvas_header.drawRect(pos_x-22/2, pos_y + (2 + bari_h), 22, bari_h, G15);
    }
    if((bari_last_10/10)>=1){
        canvas_header.fillRect(pos_x-12/2, pos_y + (2 + bari_h)*2, 12, bari_h, G15);
    }else{
        canvas_header.fillRect(pos_x-12/2, pos_y + (2 + bari_h)*2, 12, bari_h, G0);
        canvas_header.drawRect(pos_x-12/2, pos_y + (2 + bari_h)*2, 12, bari_h, G15);
    }

}


/* 時刻 */
void Page::dispTime(uint16_t pos_x, uint16_t pos_y){
    rtc_time_t RTCtime;
    rtc_date_t RTCdate;
    
    M5.RTC.getDate(&RTCdate);
    M5.RTC.getTime(&RTCtime);

    canvas_header.setTextFont(1);
    canvas_header.setTextSize(4);
    canvas_header.setTextColor(G15, G0);
    canvas_header.setTextArea(pos_x, pos_y, 300, 40);
    canvas_header.printf("%02d/%02d", RTCdate.mon, RTCdate.day);
    canvas_header.setTextArea(pos_x+130, pos_y, 300, 40);
    canvas_header.printf("%02d:%02d", RTCtime.hour, RTCtime.min);

}

void CmdProcGetSensBatt::update(void){
    uint8_t send_data;

    send_data = 0x00;   /* SOC */
    if(bt_cmd.send(0x10, &send_data, 1, this)){
        SocPct = 0;
    }
    send_data = 0x01;   /* 電圧 */
    if(bt_cmd.send(0x10, &send_data, 1, this)){
        Voltage = 0;
    }
}

uint8_t CmdProcGetSensBatt::CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len){
    if(0x00 == rcv_msg[0]){
        SocPct = rcv_msg[1];
        log_i("sens SOC: %d%%", SocPct);
    }else if(0x01 == rcv_msg[0]){
        Voltage = (uint16_t)rcv_msg[1] | ((uint16_t)rcv_msg[2]<<8);
        log_i("sens Voltage: %.2fV", Voltage/1000.0f);
    }
    return 0;
}
