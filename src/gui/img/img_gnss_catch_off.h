#pragma once

#include <stdint.h>

#define IMG_GNSS_CATCH_OFF_LEN (18638)
extern const uint8_t img_gnss_catch_off[IMG_GNSS_CATCH_OFF_LEN];
