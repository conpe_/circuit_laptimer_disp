#pragma once

#include <stdint.h>

#define IMG_GNSS_CATCH_LEN (18648)
extern const uint8_t img_gnss_catch[IMG_GNSS_CATCH_LEN];
