/**************************************************
	DataMemList.h
	EEPROMメモリ管理
				2023/10/25 0:16:59
**************************************************/

#ifndef __DATAMEMLIST_H__
#define __DATAMEMLIST_H__

#include "stdint.h"
#include "DataMemType.h"

/* EEPROM容量 [byte] */
#define MEM_CAPACITY 1200	/* 容量(byte) */

/* EEPROM使用量 [byte] */
#define MEM_USED_BYTE 14	/* 使用量(byte) */

/* EEPROM使用ID数 */
#define MEM_ID_NUM 14	/* 使用ID数 */

/* グループ数 */
#define MEM_GROUP_NUM 3	/* グループ数 */



/* EEPROM ID */
#define	MEM_INITIALIZED	0x0000	/* EEPROM初期化済みフラグ */
#define	MEM_SEC	0x0001	/* セクター数 */
#define	DISP_SPD_METHOD	0x0002	/* 速度表示方法 */
#define	LAP_SENS_SEL	0x0100	/* ラップセンサ選択 */
#define	MEM_BT0	0x0101	/* BT接続先MACアドレス0 */
#define	MEM_BT1	0x0102	/* BT接続先MACアドレス1 */
#define	MEM_BT2	0x0103	/* BT接続先MACアドレス2 */
#define	MEM_BT3	0x0104	/* BT接続先MACアドレス3 */
#define	MEM_BT4	0x0105	/* BT接続先MACアドレス4 */
#define	MEM_BT5	0x0106	/* BT接続先MACアドレス5 */
#define	TPMS_EN	0x0107	/* TPMS有効 */
#define	GEAR_T_DRIVE	0x0200	/* スプロケドライブ歯数 */
#define	GEAR_T_DRIVEN	0x0201	/* スプロケドリブン歯数 */
#define	ENG_PULSE_DIV	0x0202	/* エンジンパルス分周 */


/* EEPROM変数 */
extern stMem<uint8_t> mem_initialized;		/* EEPROM初期化済みフラグ */
extern stMem<uint8_t> mem_sec;		/* セクター数 */
extern stMem<uint8_t> disp_spd_method;		/* 速度表示方法 */
extern stMem<uint8_t> lap_sens_sel;		/* ラップセンサ選択 */
extern stMem<uint8_t> mem_bt0;		/* BT接続先MACアドレス0 */
extern stMem<uint8_t> mem_bt1;		/* BT接続先MACアドレス1 */
extern stMem<uint8_t> mem_bt2;		/* BT接続先MACアドレス2 */
extern stMem<uint8_t> mem_bt3;		/* BT接続先MACアドレス3 */
extern stMem<uint8_t> mem_bt4;		/* BT接続先MACアドレス4 */
extern stMem<uint8_t> mem_bt5;		/* BT接続先MACアドレス5 */
extern stMem<uint8_t> tpms_en;		/* TPMS有効 */
extern stMem<uint8_t> gear_t_drive;		/* スプロケドライブ歯数 */
extern stMem<uint8_t> gear_t_driven;		/* スプロケドリブン歯数 */
extern stMem<uint8_t> eng_pulse_div;		/* エンジンパルス分周 */


/* EEPROM管理テーブル */
extern const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM];
/* グループ管理テーブル */
extern const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM];


#endif

