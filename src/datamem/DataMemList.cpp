/**************************************************
	DataMemList.cpp
	EEPROMメモリ管理
				2023/10/25 0:16:59
**************************************************/

#include "DataMemList.h"


/* EEPROM変数 */
stMem<uint8_t> mem_initialized = {1, 0, 1};		/* EEPROM初期化済みフラグ */
stMem<uint8_t> mem_sec = {1, 1, 16};		/* セクター数 */
stMem<uint8_t> disp_spd_method = {0, 0, 1};		/* 速度表示方法 */
stMem<uint8_t> lap_sens_sel = {0, 0, 2};		/* ラップセンサ選択 */
stMem<uint8_t> mem_bt0 = {0, 0, 255};		/* BT接続先MACアドレス0 */
stMem<uint8_t> mem_bt1 = {0, 0, 255};		/* BT接続先MACアドレス1 */
stMem<uint8_t> mem_bt2 = {0, 0, 255};		/* BT接続先MACアドレス2 */
stMem<uint8_t> mem_bt3 = {0, 0, 255};		/* BT接続先MACアドレス3 */
stMem<uint8_t> mem_bt4 = {0, 0, 255};		/* BT接続先MACアドレス4 */
stMem<uint8_t> mem_bt5 = {0, 0, 255};		/* BT接続先MACアドレス5 */
stMem<uint8_t> tpms_en = {1, 0, 1};		/* TPMS有効 */
stMem<uint8_t> gear_t_drive = {31, 9, 31};		/* スプロケドライブ歯数 */
stMem<uint8_t> gear_t_driven = {71, 65, 90};		/* スプロケドリブン歯数 */
stMem<uint8_t> eng_pulse_div = {1, 1, 2};		/* エンジンパルス分周 */


/* EEPROM管理テーブル */
const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN],		DataId,		Adrs,		DataType,		InitVal,		MinVal,		MaxVal,		ExternalAccessPermission,		*/
	{	"", MEM_INITIALIZED,		0x0000,		MEM_UINT8,		&mem_initialized.Init,		&mem_initialized.Min,		&mem_initialized.Max,		DATAMEM_PERMISSION_READ_ONLY,		},		/* EEPROM初期化済みフラグ (Resolution=1, Offset=0) */
	{	"sector_num", MEM_SEC,		0x0001,		MEM_UINT8,		&mem_sec.Init,		&mem_sec.Min,		&mem_sec.Max,		DATAMEM_PERMISSION_FULL,		},		/* セクター数 (Resolution=1, Offset=0) */
	{	"disp_spd_method", DISP_SPD_METHOD,		0x0002,		MEM_UINT8,		&disp_spd_method.Init,		&disp_spd_method.Min,		&disp_spd_method.Max,		DATAMEM_PERMISSION_FULL,		},		/* 速度表示方法 (Resolution=1, Offset=0) */
	{	"lap_sens_sel", LAP_SENS_SEL,		0x0003,		MEM_UINT8,		&lap_sens_sel.Init,		&lap_sens_sel.Min,		&lap_sens_sel.Max,		DATAMEM_PERMISSION_FULL,		},		/* ラップセンサ選択 (Resolution=1, Offset=0) */
	{	"bt_adrs_0", MEM_BT0,		0x0004,		MEM_UINT8,		&mem_bt0.Init,		&mem_bt0.Min,		&mem_bt0.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス0 (Resolution=1, Offset=0) */
	{	"bt_adrs_1", MEM_BT1,		0x0005,		MEM_UINT8,		&mem_bt1.Init,		&mem_bt1.Min,		&mem_bt1.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス1 (Resolution=1, Offset=0) */
	{	"bt_adrs_2", MEM_BT2,		0x0006,		MEM_UINT8,		&mem_bt2.Init,		&mem_bt2.Min,		&mem_bt2.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス2 (Resolution=1, Offset=0) */
	{	"bt_adrs_3", MEM_BT3,		0x0007,		MEM_UINT8,		&mem_bt3.Init,		&mem_bt3.Min,		&mem_bt3.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス3 (Resolution=1, Offset=0) */
	{	"bt_adrs_4", MEM_BT4,		0x0008,		MEM_UINT8,		&mem_bt4.Init,		&mem_bt4.Min,		&mem_bt4.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス4 (Resolution=1, Offset=0) */
	{	"bt_adrs_5", MEM_BT5,		0x0009,		MEM_UINT8,		&mem_bt5.Init,		&mem_bt5.Min,		&mem_bt5.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス5 (Resolution=1, Offset=0) */
	{	"tpms_enable", TPMS_EN,		0x000A,		MEM_UINT8,		&tpms_en.Init,		&tpms_en.Min,		&tpms_en.Max,		DATAMEM_PERMISSION_FULL,		},		/* TPMS有効 (Resolution=1, Offset=0) */
	{	"gear_t_drive", GEAR_T_DRIVE,		0x000B,		MEM_UINT8,		&gear_t_drive.Init,		&gear_t_drive.Min,		&gear_t_drive.Max,		DATAMEM_PERMISSION_FULL,		},		/* スプロケドライブ歯数 (Resolution=1, Offset=0) */
	{	"gear_t_driven", GEAR_T_DRIVEN,		0x000C,		MEM_UINT8,		&gear_t_driven.Init,		&gear_t_driven.Min,		&gear_t_driven.Max,		DATAMEM_PERMISSION_FULL,		},		/* スプロケドリブン歯数 (Resolution=1, Offset=0) */
	{	"eng_puluse_div", ENG_PULSE_DIV,		0x000D,		MEM_UINT8,		&eng_pulse_div.Init,		&eng_pulse_div.Min,		&eng_pulse_div.Max,		DATAMEM_PERMISSION_FULL,		},		/* エンジンパルス分周 (Resolution=1, Offset=0) */
};

/* ・Name無しは表示しない */
/* ・DataIdの上位1byteがグループID */

/* グループ管理テーブル */
const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN]	*/
	{	"Util"	},		/* Group0 Util */
	{	"Sens"	},		/* Group1 Sens */
	{	"Drive"	},		/* Group2 Drive */
};
