#include "mdlCanDataConfig_B0.h"
#include <vector>

StMdlCan_B0_TxVal stMdlCan_B0_TxVal;
StMdlCan_B0_RxVal stMdlCan_B0_RxVal;

/* 送信 */
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_System = {
    {false, 0, 4, 0, 32, (void*)&stMdlCan_B0_TxVal.SysTime_s},
    {false, 4, 1, 32, 8, (void*)&stMdlCan_B0_TxVal.SysStatus},
    {false, 5, 1, 40, 2, (void*)&stMdlCan_B0_TxVal.KeyStatus},
    {false, 5, 1, 42, 2, (void*)&stMdlCan_B0_TxVal.Pit}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_LapTime = {
    {false, 0, 2, 0, 16, (void*)&stMdlCan_B0_TxVal.Lap},
    {false, 2, 1, 16, 1, (void*)&stMdlCan_B0_TxVal.Tick},
    {false, 4, 4, 32, 32, (void*)&stMdlCan_B0_TxVal.LapTime_ms}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_TickTime = {
    {false, 0, 2, 0, 16, (void*)&stMdlCan_B0_TxVal.Lap},
    {false, 2, 1, 16, 1, (void*)&stMdlCan_B0_TxVal.Tick},
    {false, 4, 4, 32, 32, (void*)&stMdlCan_B0_TxVal.TickTime_ms}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_Env = {
    {false, 0, 1, 0, 8, (void*)&stMdlCan_B0_TxVal.Temp_deg},
    {false, 1, 1, 8, 16, (void*)&stMdlCan_B0_TxVal.Humi_pct}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_Engine = {
    {false, 0, 2, 0, 16, (void*)&stMdlCan_B0_TxVal.EngSpd_rpm},
    {false, 2, 1, 16, 8, (void*)&stMdlCan_B0_TxVal.EngPulseDiv}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_Gps0 = {
    {false, 0, 8, 0, 64, (void*)&stMdlCan_B0_TxVal.GpsLongitude}   /* 経度 */
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_Gps1 = {
    {false, 0, 8, 0, 64, (void*)&stMdlCan_B0_TxVal.GpsLatitude}    /* 緯度 */
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_Gps2 = {
    {false, 0, 2, 0, 16, (void*)&stMdlCan_B0_TxVal.GpsSpd_kmph},
    {false, 2, 2, 16, 16, (void*)&stMdlCan_B0_TxVal.GpsDir},
    {false, 4, 2, 32, 16, (void*)&stMdlCan_B0_TxVal.GpsAltitude},
    {false, 6, 1, 48, 8, (void*)&stMdlCan_B0_TxVal.GpsHdop}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_CommandPdlAcc = {
    {true, 0, 8, 0, 32, (void*)&stMdlCan_B0_TxVal.CommandPdlAcc}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_CommandPdlBrk = {
    {true, 0, 8, 0, 32, (void*)&stMdlCan_B0_TxVal.CommandPdlBrk}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Tx_CommandStr = {
    {true, 0, 8, 0, 32, (void*)&stMdlCan_B0_TxVal.CommandStr}
};

/* 受信 */
std::vector<mdlCanSignal> MdlCanSignal_B0_Rx_PdlAcc = {
    {false, 0, 1, 0, 8, (void*)&stMdlCan_B0_RxVal.PdlAcc},
    {false, 7, 1, 56, 8, (void*)&stMdlCan_B0_RxVal.PdlAccCalibSta}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Rx_PdlBrk = {
    {false, 0, 1, 0, 8, (void*)&stMdlCan_B0_RxVal.PdlBrk},
    {false, 7, 1, 56, 8, (void*)&stMdlCan_B0_RxVal.PdlBrkCalibSta}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Rx_Str = {
    {false, 0, 1, 0, 8, (void*)&stMdlCan_B0_RxVal.Str},
    {false, 7, 1, 56, 8, (void*)&stMdlCan_B0_RxVal.StrCalibSta}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Rx_TickTime = {
    {false, 0, 2, 0, 16, (void*)&stMdlCan_B0_RxVal.TickNum},
    {false, 4, 4, 32, 32, (void*)&stMdlCan_B0_RxVal.TickTime_ms}
};
std::vector<mdlCanSignal> MdlCanSignal_B0_Rx_Engine = {
    {false, 0, 2, 0, 16, (void*)&stMdlCan_B0_RxVal.EngSpd_rpm}
};


std::vector<mdlCanMessage> CanMsg_B0_Tx = {
    {0x18FF0020, 100, 0, 8, &MdlCanSignal_B0_Tx_System, nullptr},             /* ステータス */
    {0x18FF0120, 0xFFFFFFFF, 0, 8, &MdlCanSignal_B0_Tx_LapTime, nullptr},     /* laptime */
    {0x18FF0220, 0xFFFFFFFF, 0, 8, &MdlCanSignal_B0_Tx_TickTime, nullptr},     /* ticktime */
    {0x18FF0320, 30000, 0, 2, &MdlCanSignal_B0_Tx_Env, nullptr},          /* 気象 */
    {0x18FF1020, 100, 0, 3, &MdlCanSignal_B0_Tx_Engine, nullptr},            /* エンジン回転数 */
    {0x18FF1120, 0xFFFFFFFF, 0, 8, &MdlCanSignal_B0_Tx_Gps0, nullptr},     /* GPS */
    {0x18FF1220, 0xFFFFFFFF, 0, 8, &MdlCanSignal_B0_Tx_Gps1, nullptr},     /* GPS */
    {0x18FF1320, 0xFFFFFFFF, 0, 7, &MdlCanSignal_B0_Tx_Gps2, nullptr},      /* GPS */
    {0x0CEF1020, 0xFFFFFFFF, 0, 8, &MdlCanSignal_B0_Tx_CommandPdlAcc, nullptr}, /* シリアルコマンド アクセル */
    {0x0CEF1120, 0xFFFFFFFF, 0, 8, &MdlCanSignal_B0_Tx_CommandPdlBrk, nullptr},  /* シリアルコマンド ブレーキ */
    {0x0CEF1220, 0xFFFFFFFF, 0, 8, &MdlCanSignal_B0_Tx_CommandStr, nullptr}  /* シリアルコマンド ステアリング */
};

std::vector<mdlCanMessage> CanMsg_B0_Rx = {
    {0x18FF2010, 50, 0, 8, &MdlCanSignal_B0_Rx_PdlAcc, nullptr},
    {0x18FF2011, 50, 0, 8, &MdlCanSignal_B0_Rx_PdlBrk, nullptr},
    {0x18FF2012, 50, 0, 8, &MdlCanSignal_B0_Rx_Str, nullptr},
    {0x18FF0200, 50, 0, 8, &MdlCanSignal_B0_Rx_TickTime, nullptr},
    {0x18FF1000, 50, 0, 3, &MdlCanSignal_B0_Rx_Engine, nullptr}
};
