#pragma once

#include "mdlCanData.h"

struct StMdlCan_B0_TxVal{
    int32_t SysTime_s;
    uint8_t SysStatus;
    uint8_t KeyStatus;
    uint8_t Pit;
    uint16_t Lap;
    bool Tick;
    uint32_t LapTime_ms; 
    uint32_t TickTime_ms; 
    uint8_t Temp_deg;
    uint8_t Humi_pct;
    uint16_t EngSpd_rpm;
    uint8_t EngPulseDiv;
    double GpsLongitude;
    double GpsLatitude;
    uint16_t GpsSpd_kmph;
    uint16_t GpsDir;
    uint8_t GpsHdop;
    int16_t GpsAltitude;
    uint8_t CommandPdlAcc[8];
    uint8_t CommandPdlBrk[8];
    uint8_t CommandStr[8];
};
struct StMdlCan_B0_RxVal{
    uint8_t PdlAcc;
    uint8_t PdlAccCalibSta;
    uint8_t PdlBrk;
    uint8_t PdlBrkCalibSta;
    uint8_t Str;
    uint8_t StrCalibSta;
    uint16_t TickNum;
    uint32_t TickTime_ms;
    uint16_t EngSpd_rpm;
};


extern StMdlCan_B0_TxVal stMdlCan_B0_TxVal;
extern StMdlCan_B0_RxVal stMdlCan_B0_RxVal;

extern std::vector<mdlCanMessage> CanMsg_B0_Tx;
extern std::vector<mdlCanMessage> CanMsg_B0_Rx;
