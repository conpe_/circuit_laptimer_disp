/*******************************************************************************
 * コマンドサーバ データリンク
*******************************************************************************/

#include "CmdSrvDl.h"

#include "LapSensorBt.h"
#include "DataMem.h"


/* コマンド関数 */
const std::vector<CmdProc::ST_CMD_PROC> CmdProc::stCmdProcTbl = {
    {0x00, &cmdproc_datamem_clearall},  /* データメモリ初期化 */
    {0x01, &cmdproc_datamem_write},     /* データメモリ書き込み */
    {0x02, &cmdproc_datamem_read},      /* データメモリ読み出し */
    {0x10, &lapsensor_bt}                /* ラップタイム通知 */
};
