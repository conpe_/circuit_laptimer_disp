function [] = file2array(filename)

[d, n, e] = fileparts(filename);

def_len = strcat(upper(n), '_LEN');

fs = fopen(filename, 'r');
A = fread(fs);
fclose(fs);


[~,outfile] = fileparts(filename);

fs = fopen(strcat(outfile, '.c'), 'w');
fprintf(fs, '#include "%s"\n\n', strcat(outfile, '.h'));
fprintf(fs, 'const uint8_t %s[%s] = { \n\t', outfile, def_len);

for i=1:length(A)
    fprintf(fs, '0x%02x', A(i));
    if i~=length(A)
        fprintf(fs, ', ');
    end
    if mod(i, 100)==0 && i~=length(A)
        fprintf(fs, '\n\t');
    end
end

fprintf(fs, '\n};\n');
fclose(fs);

fs = fopen(strcat(outfile, '.h'), 'w');
fprintf(fs, '#pragma once\n\n');
fprintf(fs, '#include <stdint.h>\n\n');
fprintf(fs, '#define %s (%d)\n', def_len, length(A));
fprintf(fs, 'extern const uint8_t %s[%s];\n', outfile, def_len);
fclose(fs);