
#include <M5EPD.h>
#include "LittleFS.h"
#include "SD.h"

#include "buzz.h"
#include "LapTimer.h"
#include "GuiProc.h"
#include "PageObj.h"
#include "CmdSrv.h"
#include "DataMem.h"
#include "drvMemEsp32Flash.h"
#include "BtSerialCmdMaster.h"
#include "SerialCmd.h"
#include "LapTimerLogMgr.h"
#include "LapSensorMgr.h"
#include "Tire.h"
#include "EngineCAN.h"
#include "Gnss.h"
#include "TimeAdjust.h"

#include "drvCanEsp32.h"
#include "mdlCan.h"
#include "mdlCanData.h"
#include "mdlCanDataConfig_B0.h"

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#include <esp_now.h>
#include <WiFi.h>

/* ラップセンサ(PORT.A) */
#define PORT_WIRED_LAP_SENSOR (25)  /* 有線磁気センサのポート */
//#define PORT_ENGINE_PULSE (32)  /* エンジンパルスのポート */

/* CAN (PORT.B) */
#define PIN_CANTX GPIO_NUM_26
#define PIN_CANRX GPIO_NUM_33
/* GPS (PORT.C) */
#define RX_PIN (19)
#define TX_PIN (18)

SemaphoreHandle_t   semaphore_spi;
SemaphoreHandle_t   semaphore_session;

static uint32_t wdt = 20*1000;

drvCan* can;
mdlCan* mdl_can;

uint32_t BatteryVoltage;  /* バッテリ電圧 */
float Humi; /* 湿度 */
float Temp; /* 温度 */

void GuiTask(void *pvParameters);
void SerialTask(void *pvParameters);
void BtSerialTask(void *pvParameters);
void SdTask(void *pvParameters);
void BleScan(void *pvParameters);
void CanTask(void *pvParameters);
void LogTask(void *pvParameters);
void MainTask1000(void *pvParameters);

void printLogIdx(void);
void listDir(fs::FS &fs, const char * dirname, uint8_t levels);

uint8_t slaveAddress[] = { 0x24, 0x0A, 0xC4, 0x5B, 0xC9, 0xE8 }; // 受信機のMACアドレス
void espnow_setup(void);
void espnow_off(void);
void espnow_onSend(const uint8_t* mac_addr, esp_now_send_status_t status);
void espnow_sendPitTime(void);

TaskHandle_t taskHandle[10];

void savePanicPrintBacktrace(void);

DrvMemEsp32Flash drv_mem_flash(MEM_FILENAME_DEFAULT, MEM_USED_BYTE);

// the setup routine runs once when M5Stack starts up
void setup() {
  /* セマフォ */
  semaphore_spi = xSemaphoreCreateMutex();
  semaphore_session = xSemaphoreCreateMutex();

  // initialize the M5Stack object
  M5.begin(true, true, true, true, true);
  M5.RTC.begin();

  /* 現在時刻設定 */
  rtc_time_t RTCtime;
  rtc_date_t RTCdate;
  M5.RTC.getDate(&RTCdate);
  M5.RTC.getTime(&RTCtime);
  log_i("RTC %04d/%02d/%02d %02d:%02d:%02d", RTCdate.year, RTCdate.mon, RTCdate.day, RTCtime.hour, RTCtime.min, RTCtime.sec);
  /* システム時刻設定 */
  struct tm rtc_time;
  rtc_time.tm_sec = RTCtime.sec;
  rtc_time.tm_min = RTCtime.min; 
  rtc_time.tm_hour = RTCtime.hour; 
  rtc_time.tm_mday = RTCdate.day; 
  rtc_time.tm_wday = RTCdate.week;
  rtc_time.tm_mon = RTCdate.mon - 1;      // tm構造体は0-11の範囲なので１引く
  rtc_time.tm_year = RTCdate.year - 1900; // tm構造体は1900年起点なので1900を引く
  rtc_time.tm_yday = 0;
  rtc_time.tm_isdst = 0;
  
  time_t timertc = mktime(&rtc_time);
  struct timeval tv = {  .tv_sec = timertc };
  settimeofday(&tv, NULL);

  /* 例外による起動か判定してログ書き出し */
  savePanicPrintBacktrace();

  M5.EPD.SetRotation(180);
  M5.EPD.Clear(true);

  buzz.init();

  DataMem::init(&drv_mem_flash);
  /* 初期化チェック */
  uint8_t inited = false;
  DataMem::read(MEM_INITIALIZED, &inited);
  if(!inited){
    DataMem::clearAll();
  }
  
  /* BTコマンド初期化 */
  uint8_t sens_address[6] = {0x24, 0x0A, 0xC4, 0x04, 0x6B, 0xB6};
  DataMem::read(MEM_BT0, &sens_address[0]);
  DataMem::read(MEM_BT1, &sens_address[1]);
  DataMem::read(MEM_BT2, &sens_address[2]);
  DataMem::read(MEM_BT3, &sens_address[3]);
  DataMem::read(MEM_BT4, &sens_address[4]);
  DataMem::read(MEM_BT5, &sens_address[5]);
  bt_cmd.init(sens_address);
  
  /* シリアルコマンド初期化 */
  serial_cmd.init();

  /* エンジン初期化 */
  EngineCAN::init();

  /* GNSS初期化 */
  gnss.init(RX_PIN, TX_PIN);

  /* CAN通信 */
  can = new drvCanEsp32(PIN_CANTX, PIN_CANRX);
  mdl_can = new mdlCan(can, &CanMsg_B0_Tx, &CanMsg_B0_Rx);
  uint32_t filter = 0;
  uint32_t mask = 0;
  mdl_can->getRxMask(&filter, &mask);
  //filter = 0xffffff23;  /* フィルタとマスクがうまく動かないので */
  //mask = 0x00000000;    /* マスクは0とする。 */
  if(0 != can->init(CanBaud500, filter, mask)){
    log_i("twai install failed.");
  }

  /* 初期ページ設定 */
  Gui.setPage(&page_laptime_cnt);

  /* ログ機能初期化 */
  LapTimerLogMgr::init();


  /* SDカードのファイル一覧を表示 */
  #if ARDUHAL_LOG_LEVEL >= ARDUHAL_LOG_LEVEL_INFO
  printLogIdx();
  #endif

  uint8_t tpmsen = 1;
  DataMem::read(TPMS_EN, &tpmsen);
  if(tpmsen){
    Tire::createTireObj();
  }

  /* タスク */
  /* 優先度：数値が大きいほうが優先度高い */
  xTaskCreatePinnedToCore(GuiTask, "guitask", 4096, NULL, 2, &taskHandle[0], 0);        /* core0だと、stop()時にDevice response timeoutしてウォッチドッグで止まることがある */
  xTaskCreatePinnedToCore(BtSerialTask, "bttask", 4096, NULL, 6, &taskHandle[1], 0);    /* 2048だと足りない */
  xTaskCreatePinnedToCore(SerialTask, "serialtask", 2048, NULL, 8, &taskHandle[2], 1);
  if(Tire::isEnableTireData()){ /* タイヤセンサ有効なら */
    xTaskCreatePinnedToCore(BleScan, "bletask", 2048, NULL, 3, &taskHandle[3], 1);
  }
  xTaskCreatePinnedToCore(CanTask, "cantask", 2500, NULL, 3, &taskHandle[4], 0);      /* 1024だと足りない */
  xTaskCreatePinnedToCore(LogTask, "logtask", 8192, NULL, 6, &taskHandle[5], 1);
  xTaskCreatePinnedToCore(MainTask1000, "maintask1000", 4048, NULL, 5, &taskHandle[6], 0);  /* 保存とかゆっくりめで良いもの */

}

// the loop routine runs over and over again forever
void loop(){
  static uint32_t tim_50ms = 0;
  static uint32_t tim_1ms = 0;
  static uint32_t tim_200ms = 0;

  static LapSensorMgr::TYPE sens_sel_last = LapSensorMgr::TYPE::NONE;
  static LapSensor *lap_sensor;
  static bool Connected = false;

  static uint32_t batt_sum = 0;
  static uint8_t batt_sum_cnt = 0;

  uint32_t current_tim = millis();

  /* 1msごと処理 */
  if(tim_1ms <= current_tim){
    buzz.task();

    tim_1ms = current_tim + 1;
  }
  
  /* 50msごと処理 */
  if(tim_50ms <= current_tim){

    /* センサ */
    LapSensorMgr::TYPE sens_sel = LapSensorMgr::selectedType();
    lap_sensor = LapSensorMgr::sensObj(sens_sel);

    lap_sensor->task();

    /* センサ選択変化 */
    if((sens_sel_last != sens_sel) && (LapSensorMgr::TYPE::NONE != sens_sel)){
      /* 有線センサのときは初期化処理 */
      if(LapSensorMgr::TYPE::GPIO == sens_sel){
        LapSensorWired::init(PORT_WIRED_LAP_SENSOR);
      }else{
        LapSensorWired::deinit();
      }

      lap_timer.stop();
      
      sens_sel_last = sens_sel;
    }
    /* センサ途切れた */
    if(Connected && !lap_sensor->isConnect()){
      lap_timer.stop();
    }
    Connected = lap_sensor->isConnect();


    TimeAdjust::task();
    lap_timer.task();
    
    tim_50ms = current_tim + 50;
  }

  /* 200msごと処理 */
  if(tim_200ms <= current_tim){

    batt_sum += M5.getBatteryVoltage();
    ++batt_sum_cnt;

    if(5 <= batt_sum_cnt){
      BatteryVoltage = batt_sum/batt_sum_cnt;
      batt_sum = 0;
      batt_sum_cnt = 0;
    }

    tim_200ms = current_tim + 200;
  }

}

/* 1000msごとの実行 */
/* 優先度低め */
void MainTask1000(void *pvParameters){
  static uint32_t t = 0;

  while(1){
    uint32_t currenttime = millis();

    espnow_sendPitTime();
    
    DataMem::save();  /* たまに保存 */
    
    if(30000 + t < currenttime){
        LapTimerLogMgr::checkSdCard();
        t = currenttime;
    }

    /* センサログデータ保存 */
    LapTimerLogMgr::task();
    
    /* 気温、湿度更新 */
    Humi = M5.SHT30.GetRelHumidity();
    Temp = M5.SHT30.GetTemperature();
    
    wdt = millis();

    vTaskDelay(1000);
  }
}

void GuiTask(void *pvParameters){
  while(1){
    Gui.update();
    M5.update();
    
    vTaskDelay(49); /* 表示の1/1000秒台を変化させるため少しずらした周期とする */
  }
}

void SerialTask(void *pvParameters){
  while(1){
    serial_cmd.task();
    
    vTaskDelay(20);
  }
}

void BtSerialTask(void *pvParameters){
  while(1){
    bt_cmd.task();
    
    vTaskDelay(20);
  }
}


class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    
    BLEAddress address = advertisedDevice.getAddress();

    for(auto tire = Tires.begin(); tire!=Tires.end(); ++tire){
      if(address == tire->address){                   /* 設定されているタイヤのアドレスと一致 */
        if(advertisedDevice.haveManufacturerData()){  /* データ部有り */
          std::string name = advertisedDevice.getName();
          char *pHex = BLEUtils::buildHexData(nullptr, (uint8_t*)advertisedDevice.getManufacturerData().data(), advertisedDevice.getManufacturerData().length());
          std::string mdata(pHex);
          /* 文字列をバイト列に */
          std::vector<uint8_t> rcvdata;
          for(int i=0; i<mdata.size(); i+=2){
            rcvdata.push_back(std::stoi(mdata.substr(i,2), nullptr, 16));
          }

          tire->tpms_callback(&rcvdata);
        }
      }
    }
  }
  //log_i("Advertised Device: %s", advertisedDevice.toString().c_str());
};

void BleScan(void *pvParameters){
  
  int scanTime = 3; //In seconds
  BLEScan* pBLEScan;

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(false); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value


  while(1){
    BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
    
    log_i("Devices found: %d", foundDevices.getCount());

    pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
    vTaskDelay(1000);
  }
}

void printLogIdx(void){
    char filename[30];

    log_i("[print SD]");
    listDir(SD, "/", 5);

    if(!LittleFS.begin(1)){
        log_i("failed to begin LittleFS");
        return;
    }
    log_i("[print LittleFS]");
    listDir(LittleFS, "/", 5);

}

//
// levels: 表示する階層
void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    char path[32];

    File root = fs.open(dirname);
    if(!root){
        return;
    }
    /* ルートがディレクトリでないなら処理終了 */
    if(!root.isDirectory()){
        return;
    }

    /* ディレクトリ内のファイルまたはディレクトリを取得 */
    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            // ディレクトリ名を出力
            sprintf(path, "%s/%s", dirname, file.name());
            Serial.print("  DIR : ");
            Serial.println(path);
            if(levels){
                // サブディレクトリを出力
                listDir(fs, path, levels-1);
            }
        } else {
            // ファイル名とファイルサイズを出力
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("\tSIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}


void CanTask(void *pvParameters){
  /* CAN送受信処理 */
  while(1){
    can->task();

    /* 送信値セット */
    /* 時刻 */
    time_t timertc = time(NULL);
    stMdlCan_B0_TxVal.SysTime_s = timertc;
    /* 計測状態 */
    stMdlCan_B0_TxVal.SysStatus = (uint8_t)lap_timer.getState();
    /* キー状態 */
    stMdlCan_B0_TxVal.KeyStatus = BatteryVoltage>4150;  /* 0:OFF,1:ON */
    /* PIT */
    stMdlCan_B0_TxVal.Pit = (0!=lap_timer.pitTime());  /* 0:outpit, 1:inpit */
    
    uint8_t lap;
    uint32_t time;
    stMdlCan_B0_TxVal.Tick = 0;
    if(0 == lap_timer.getLap(-1, &lap, &time)){
      stMdlCan_B0_TxVal.Lap = lap;
      stMdlCan_B0_TxVal.LapTime_ms = time;
    }else{
      stMdlCan_B0_TxVal.Lap = 0xFFFF;
      stMdlCan_B0_TxVal.LapTime_ms = 0xFFFFFFFF;
    }

    stMdlCan_B0_TxVal.Temp_deg = (int8_t)Temp;
    stMdlCan_B0_TxVal.Humi_pct = (uint8_t)Humi;

    /* エンジン */
    stMdlCan_B0_TxVal.EngSpd_rpm = eng->spd_rpm;      /* エンジン回転数 */
    stMdlCan_B0_TxVal.EngPulseDiv = eng->pulse_div;   /* 分周比 */

    mdl_can->task();
    vTaskDelay(1);
  }
}

void LogTask(void *pvParameters){

  while(1){

    /* WDT */
    if(10*1000+wdt < millis()){
      log_i("WDT %d[ms]", millis()-wdt);
      ESP.restart();
    }
    
    /* セッションに依存した時間 */
    xSemaphoreTake(semaphore_session, portMAX_DELAY);
    Session *s;
    s = lap_timer.getSession();
    uint32_t t;
    if(s){
      t = s->passedTime();
    }else{
      t = 0;
    }
    xSemaphoreGive(semaphore_session);

    gnss.task(t);
    /* エンジン */
    if(eng){
      eng->update(t);
    }
    

    vTaskDelay(50);
  }
}

void espnow_setup(void){

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  if (esp_now_init() == ESP_OK) {
      log_i("ESPNow Init Success");
  }

  static esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, slaveAddress, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
      log_i("Failed to add peer");
      return;
  }

  esp_now_register_send_cb(espnow_onSend);
}

void espnow_off(void){
  esp_now_deinit();
  WiFi.mode(WIFI_OFF);

  log_i("espnow close");
}

void espnow_onSend(const uint8_t* mac_addr, esp_now_send_status_t status) {
  char macStr[18];
  char result[10];
  snprintf(macStr, sizeof(macStr), "%02X:%02X:%02X:%02X:%02X:%02X",
      mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);

  if(ESP_NOW_SEND_SUCCESS == status){
    sprintf(result, "success");
  }else{
    sprintf(result, "fail");
  }

  log_i("EspSend %s, %s", macStr, result);

}

void espnow_sendPitTime(void){
  static bool espnow_sta = false;
  /* ピット時間取得 */
  uint32_t pittime = lap_timer.pitTime();
  /* ピット入ってたらESPnow起動 */
  if(espnow_sta && (0==pittime)){
    espnow_off();
    espnow_sta = false;
  }else if(!espnow_sta && (0!=pittime)){
    espnow_setup();
    espnow_sta = true;
  }
  /* ピット時間送信 */
  if(espnow_sta){
    static uint8_t esp_send[4];
    esp_send[0] = pittime;
    esp_send[1] = pittime>>8;
    esp_send[2] = pittime>>16;
    esp_send[3] = pittime>>24;
    esp_err_t result = esp_now_send(slaveAddress, esp_send, sizeof(esp_send));
  }
}


/* 異常終了時にログを残す */

/* blild_flags=に追加する */
/* 「	-Wl,--wrap=panic_print_backtrace」 */
#include "esp_debug_helpers.h"

#define STACK_DEPTH 32  /* 覚える最大の深さ */
typedef struct {
    uint32_t pc[STACK_DEPTH];
    uint32_t sp[STACK_DEPTH];
} ExceptionInfo_t;

extern "C" void __real_panic_print_backtrace(const void* f, int core);

/* RTC SLOW MEMORYに配置される(リセットしても残る) */
RTC_NOINIT_ATTR static ExceptionInfo_t s_exception_info;

extern "C" void __wrap_panic_print_backtrace(const void* f, int core) {
    XtExcFrame* xt_frame = (XtExcFrame*) f;
    esp_backtrace_frame_t stk_frame = { .pc = (uint32_t)xt_frame->pc, .sp = (uint32_t)xt_frame->a1, .next_pc = (uint32_t)xt_frame->a0, .exc_frame = xt_frame };

    // 最初のスタックフレームが有効か
    bool corrupted = !(esp_stack_ptr_is_sane(stk_frame.sp) &&
                       (esp_ptr_executable((void*) esp_cpu_process_stack_pc(stk_frame.pc)) ||
                        /* Ignore the first corrupted PC in case of InstrFetchProhibited */
                        (stk_frame.exc_frame && ((XtExcFrame*) stk_frame.exc_frame)->exccause == EXCCAUSE_INSTR_PROHIBITED)));

    // 最後のコールスタックから、順番に取り出していく
    uint32_t i = STACK_DEPTH;
    for(i = 0; STACK_DEPTH>i; ++i){
        s_exception_info.pc[i] = 0x00000000;
        s_exception_info.sp[i] = 0x00000000;
    }
    
    i = STACK_DEPTH;

    while (i > 0 && !corrupted) {
        s_exception_info.pc[STACK_DEPTH - i] = esp_cpu_process_stack_pc(stk_frame.pc);
        s_exception_info.sp[STACK_DEPTH - i] = stk_frame.sp;
        
        log_w("0x%08x:0x%08x (next 0x%08x)",esp_cpu_process_stack_pc(stk_frame.pc), stk_frame.sp, stk_frame.next_pc);

        if (!esp_backtrace_get_next_frame(&stk_frame)) { // 前のスタックフレームを取得
            corrupted = true;
        }
        --i;
    }

    // 元のpanic_handler関数を実行
    __real_panic_print_backtrace(f, core);
}

/* パニックからの再起動時にSDカードにトレース情報を記録する */
void savePanicPrintBacktrace(void){
  /* 例外発生による再起動かを判定 */
  esp_reset_reason_t reason = esp_reset_reason();
  log_w("reset reason = %02X", reason);

  if(reason == ESP_RST_PANIC) {
      char backtrace_str[1024];
      uint16_t offset = 0;
      offset += sprintf(backtrace_str + offset, "Backtrace: ");
      for (uint8_t i = 0; i < STACK_DEPTH; i++) {
          if (s_exception_info.pc[i] != 0 && s_exception_info.sp[i] != 0) {
              offset += sprintf(backtrace_str + offset, "0x%08x:0x%08x ", (unsigned int) s_exception_info.pc[i], (unsigned int) s_exception_info.sp[i]);
          }
      }
      /* ダンプ */
      log_w("%.*s", strlen(backtrace_str), backtrace_str);

      /* SDカード書き出し */
      char filepath[64];
      time_t current = time(NULL);
      struct tm* stime = localtime(&current);
      //SD.mkdir("errlog"); /* うごかず */ 
      //sprintf(filepath, "/errlog/%04d%02d%02d_%02d%02d%02d.txt", stime->tm_year+1900, stime->tm_mon+1, stime->tm_mday, stime->tm_hour, stime->tm_min, stime->tm_sec);
      sprintf(filepath, "/err_%04d%02d%02d_%02d%02d%02d.txt", stime->tm_year+1900, stime->tm_mon+1, stime->tm_mday, stime->tm_hour, stime->tm_min, stime->tm_sec);
      // SD.beginはM5.begin内で処理されているので不要
      File fp = SD.open(filepath, FILE_WRITE);
      if(fp){
        fp.printf("%.*s\r\n", strlen(backtrace_str), backtrace_str);
        fp.close();
      }else{
        log_w("failed to open errlog file. %s", filepath);
      }
  }
}
