#pragma once

#include <vector>
#include <string>

#include "Engine.h"

class EngineCAN : public Engine{
public:
    EngineCAN(std::string nm, uint8_t pulse_div);

    static void init(void);
    
    void update(uint32_t t);
    
private:

};

