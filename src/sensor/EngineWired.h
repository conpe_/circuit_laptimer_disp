#pragma once

#include <vector>
#include <string>
#include "driver/mcpwm.h"

#include "Engine.h"


class EngineWired : public Engine{
public:
    EngineWired(std::string nm, uint8_t pulse_div = 1);
    static void init(int gpio);

    static bool pulse(mcpwm_unit_t mcpwm, mcpwm_capture_channel_id_t cap_channel, const cap_event_data_t *edata, void *user_data);    /* パルス入ったときに呼ぶ */
    
    void update(uint32_t t);

protected:

};

