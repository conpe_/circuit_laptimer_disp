
#include "Engine.h"
#include <SD.h>
#include "DataMem.h"

#define PEAK_HOLD_TIME  (2000)  /* ピークホールド時間[ms] */
#define NO_PULSE_TIME   (200)   /* パルスなし時間[ms] */
#define MAX_RPM         (15000) /* 最大回転数 */


Engine* eng;


Engine::Engine(std::string nm, uint8_t pulse_div):
                Sensor(nm),
                pulse_div(pulse_div),
                spd_rpm(0),
                spd_rpm_peak(0),
                peak_update_time(0)
{
    semaphore_logs = xSemaphoreCreateMutex();
}


void Engine::calc_peak_spd(void){
    uint32_t current_time = millis();

    if(spd_rpm >= spd_rpm_peak){
        /* 保持している回転数より高いならそれに更新 */
        spd_rpm_peak = spd_rpm;
        peak_update_time = current_time;
    }else{
        /* 保持している回転数より低いので指定時間経ってから落とす */
        if(peak_update_time + PEAK_HOLD_TIME < current_time ){
            spd_rpm_peak = spd_rpm;
        }
    }
}

void Engine::calc_spd_kmph(void){
    uint8_t drivegear;
    uint8_t drivengear;
    uint16_t tire_dia = 273;
    DataMem::read(GEAR_T_DRIVE, &drivegear);
    DataMem::read(GEAR_T_DRIVEN, &drivengear);

    spd_kmph = (float)spd_rpm/60.0f / ((float)drivengear/(float)drivegear) * (float)tire_dia * 3.1415f /1000.0f/1000.0f*3600.0f;

}

uint16_t Engine::pulsediff2rpm(uint32_t pulsediff_us){
    return 1000*1000*60/pulsediff_us;
}

uint16_t Engine::getMaxSpd(void){
    return MAX_RPM;
}

uint8_t Engine::save(Session *s){
    bool saved_ = saved;

    /* 保存フラグクリア */
    saved = true;
    /* データ保存用にコピー、バッファクリア */
    xSemaphoreTake(semaphore_logs, portMAX_DELAY);
    std::vector<EngLog> logs_tmp = logs;
    logs.clear();
    xSemaphoreGive(semaphore_logs);


    if(!s){
        return 2;
    }
    if(!available()){
        return 3;
    }

    /* 初回 */
    if(!fp_sens){
        FS &fs = SD;
        /* ファイル名 */
        char dirname[20];
        char filename[20];
        char filepath[40];
        tm stime;
        s->sessionInfo(&stime);

        sprintf(dirname, "/laps/%04d%02d%02d", stime.tm_year+1900, stime.tm_mon+1, stime.tm_mday);
        sprintf(filename, "%02d%02d%02d_%s.txt", stime.tm_hour, stime.tm_min, stime.tm_sec, name.c_str());
        sprintf(filepath, "%s/%s", dirname, filename);
        
        fp_sens = fs.open(filepath, FILE_APPEND);

        if(fp_sens.position()){    /* すでにあるなら追記 */
            if(!fp_sens){
                log_i("Failed to open %s.", name.c_str());
                return 1;
            }
        }else{
            if(fp_sens){
                /* ヘッダ書き込み */
                fp_sens.printf("Sensor Log\r\n");
                fp_sens.printf("Name : %s\r\n", name.c_str());
                fp_sens.printf("PulseDiv : %d\r\n", pulse_div);
                fp_sens.printf("----------\r\n");
                fp_sens.printf("time,\tspd_rpm\r\n");

                fp_sens.close();
                log_i("Create %s.", name.c_str());
            }else{
                log_i("Failed to create %s.", name.c_str());
                return 1;
            }
        }
    }
    
    if(!saved_ && fp_sens){
        /* データ書き込み */
        for(auto itr = logs_tmp.begin(); itr!=logs_tmp.end(); itr++){
            fp_sens.printf("%5d.%03d,\t%d\r\n", 
                    itr->tim/1000,
                    itr->tim%1000,
                    itr->spd_rpm
                );
        }
    }

    return 0;
}
