#pragma once

#include <Arduino.h>
#include <vector>
#include <string>

#include <BLEDevice.h>

#include "Sensor.h"

class Tire : public Sensor{
public:
    Tire(std::string nm, std::string adrs):
                Sensor(nm),
                address(adrs),
                Pressure_kpa(0),
                Temp_deg(0),
                data_rcvd(false){};

    uint16_t Pressure_kpa;  /* 空気圧 0.1kPa */
    int8_t Temp_deg;        /* 温度 */
    BLEAddress address;     /* macアドレス */

    void init(void);
    bool available(void);
    uint8_t save(Session *s);

    static std::vector<Tire>* createTireObj(void);
    static void deleteTireObj(void);
    static bool isEnableTireData(void);
    static Tire* find(std::string nm);
    void tpms_callback(std::vector<uint8_t>* data);  /* TPMSからのデータ受け取り */

private:
    bool data_rcvd;
};


extern std::vector<Tire> Tires;

