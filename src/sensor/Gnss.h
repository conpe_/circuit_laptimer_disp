#pragma once

#include <Arduino.h>
#include <vector>
#include <string>

#include <TinyGPSPlus.h>

#include "Sensor.h"

class Gnss : public Sensor{
public:
    Gnss(std::string nm);
    TinyGPSPlus gps_module;

    void init(int8_t rx_pin, int8_t tx_pin);
    void task(uint32_t t);
    bool available(void);

    uint8_t save(Session *s);

    struct GnssLog{
        uint32_t tim;       /* 時刻 */
        double latitude;    /* 緯度 */
        double longitude;   /* 経度 */
        double altitude;    /* 高度 */
        double speed_kmph;
        double course_deg;
        double hdop;
    };

    GnssLog currentval;

private:
    SemaphoreHandle_t   semaphore_logs;
    uint32_t last_update_time;
    uint32_t last_session_time;
    
    uint8_t rcvedflg;       /* 一度は受信したフラグ。bit0から順にGnssLog構造体の順番(時刻除く) */

    std::vector<GnssLog> logs;

    void sendCan(void);             /* CAN送信 */
    void test_pit_in_motegi(void);  /* もてぎのピットイン、アウト座標のテスト */

    void sendCmd(uint16_t PktType, uint32_t DataField=0xFFFFFFFF);
    void sendCmd(uint16_t PktType, std::vector<uint32_t> DataField);
    void sendCmd(const char *PktTypeDataField);
    uint8_t checksum(char *str);
};

extern Gnss gnss;
