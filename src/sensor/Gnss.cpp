
#include "Gnss.h"
#include <SD.h>
#include <HardwareSerial.h>

HardwareSerial ss(1);


//#define TEST_GNSS_UPDATE
//#define TEST_MOTEGI_PIT

Gnss gnss(std::string("GNSS"));

Gnss::Gnss(std::string nm):
                Sensor(nm),
                last_update_time(0)
{
    semaphore_logs = xSemaphoreCreateMutex();
}

void Gnss::init(int8_t rx_pin, int8_t tx_pin){
    /* GPSモジュール初期設定 */
    /* 計測周期： 10Hz */ 
    /* ボーレート: 115200bps */

    ss.begin(9800, SERIAL_8N1, rx_pin, tx_pin);

    sendCmd(220, 100);      /* 10Hz */
    sendCmd(300, std::vector<uint32_t>{100,0,0,0,0});      /* 10Hz */
    sendCmd(251, 115200);   /* ボーレートを115200に変更 */

    delay(100);     /* 設定送信完了待ち */

    ss.updateBaudRate(115200);

    rcvedflg = 0x00;
    currentval = {0, 99999.999, 99999.999, 99999.999, 99999.999, 99999.999, 99999.999};

    xSemaphoreTake(semaphore_logs, portMAX_DELAY);
    logs.clear();
    xSemaphoreGive(semaphore_logs);

}

void Gnss::task(uint32_t t){
    char c = '\0';

    /* 新規セッション開始時にクリア */
    if((0 != t)&&(0==last_session_time)){
        xSemaphoreTake(semaphore_logs, portMAX_DELAY);
        logs.clear();
        xSemaphoreGive(semaphore_logs);
    }
    last_session_time = t;

    bool f_updated = false;
    
    while (ss.available() > 0){     /* シリアルデータあり */
        c = ss.read();         /* シリアル読み込み */

        #if ARDUHAL_LOG_LEVEL >= ARDUHAL_LOG_LEVEL_DEBUG
        Serial.printf("%c", c);
        #endif

        gps_module.encode(c);
        
        if (gps_module.location.isUpdated()){
            last_update_time = millis();
            if(currentval.latitude != gps_module.location.lat()){
                currentval.latitude = gps_module.location.lat();    /* 緯度 */
                f_updated = true;
                rcvedflg |= 0x01<<0;
            }
            if(currentval.longitude != gps_module.location.lng()){
                currentval.longitude = gps_module.location.lng();   /* 経度 */
                f_updated = true;
                rcvedflg |= 0x01<<1;
            }
            if(currentval.altitude != gps_module.altitude.meters()){
                currentval.altitude = gps_module.altitude.meters();    /* 高度 */
                f_updated = true;
                rcvedflg |= 0x01<<2;
            }

        }
        if(gps_module.speed.isUpdated()){
            last_update_time = millis();
            if(currentval.speed_kmph != gps_module.speed.kmph()){
                currentval.speed_kmph = gps_module.speed.kmph();
                f_updated = true;
                rcvedflg |= 0x01<<3;
            }
        }
        if(gps_module.course.isUpdated()){
            last_update_time = millis();
            if(currentval.course_deg != gps_module.course.deg()){
                currentval.course_deg = gps_module.course.deg();
                f_updated = true;
                rcvedflg |= 0x01<<4;
            }
        }
        if(gps_module.hdop.isUpdated()){
            last_update_time = millis();
            if(currentval.hdop != gps_module.hdop.value()){
                currentval.hdop = gps_module.hdop.value();
                f_updated = true;
                rcvedflg |= 0x01<<5;
            }
        }

    }

    if(f_updated && (rcvedflg==0x3F)){
        if(0 != t){
            saved = false;
            currentval.tim = t;

            xSemaphoreTake(semaphore_logs, portMAX_DELAY);
            logs.push_back(currentval);
            xSemaphoreGive(semaphore_logs);
        }
    }
    
    #if defined(TEST_GNSS_UPDATE)
    /* test */
    static double smpl_latitude = 0.0;
    static double smpl_longitude = 0.0;
    static double smpl_speed_kmph = 0.0;

    smpl_latitude += 0.001;
    smpl_longitude += 0.001;
    smpl_speed_kmph += 0.5;
    if(smpl_speed_kmph>199.0){
        smpl_speed_kmph = 0.0;
    }
    
    last_update_time = millis();
    saved = false;
    
    currentval = {t, smpl_latitude, smpl_longitude, 0.0, smpl_speed_kmph, 0.0, 0.0};
    if(0 != t){
        xSemaphoreTake(semaphore_logs, portMAX_DELAY);
        logs.push_back(currentval);
        xSemaphoreGive(semaphore_logs);
    }
    #endif

    sendCan();

    #if defined(TEST_MOTEGI_PIT)
    test_pit_in_motegi();
    #endif
}

bool Gnss::available(void){
    #if defined(TEST_GNSS_UPDATE)
    return 1;
    #endif
    /* モジュールがOKかつ、3秒以内に更新があった */
    return gps_module.location.isValid() && ((millis()-last_update_time)<3000);
}

uint8_t Gnss::save(Session *s){
    bool saved_ = saved;

    /* 保存フラグクリア */
    saved = true;
    /* データ保存用にコピー、バッファクリア */
    xSemaphoreTake(semaphore_logs, portMAX_DELAY);
    std::vector<GnssLog> logs_tmp = logs;
    logs.clear();
    xSemaphoreGive(semaphore_logs);

    /* セッション始まってない */
    if(!s){
        return 2;
    }
    /* GNSS使えない */
    if(!available()){
        return 3;
    }

    /* 初回 */
    if(!fp_sens){
        FS &fs = SD;
        /* ファイル名 */
        char dirname[20];
        char filename[20];
        char filepath[40];
        tm stime;
        s->sessionInfo(&stime);

        sprintf(dirname, "/laps/%04d%02d%02d", stime.tm_year+1900, stime.tm_mon+1, stime.tm_mday);
        sprintf(filename, "%02d%02d%02d_%s.txt", stime.tm_hour, stime.tm_min, stime.tm_sec, name.c_str());
        sprintf(filepath, "%s/%s", dirname, filename);

        fp_sens = fs.open(filepath, FILE_APPEND);

        if(fp_sens){
            if(0 == fp_sens.position()){    /* 新規ファイルなのでヘッダを書く */
                /* ヘッダ書き込み */
                fp_sens.printf("Sensor Log\r\n");
                fp_sens.printf("Name : %s\r\n", name.c_str());
                fp_sens.printf("----------\r\n");
                fp_sens.printf("time,\tlatitude,\tlongitude,\taltitude,\thdop,\tspd_kmph,\tdir\r\n");

                fp_sens.close();
                log_i("Create %s.", name.c_str());
            }
        }else{
            log_i("Failed to create %s.", name.c_str());
            return 1;
        }
    }
    
    /* 未保存なら保存する */
    if(!saved_){

        /* データ書き込み */
        log_i("logs=%d", logs_tmp.size());
        if(fp_sens){
            for(auto itr = logs_tmp.begin(); itr!=logs_tmp.end(); itr++){
                fp_sens.printf("%5d.%03d,\t%.9lf,\t%.9lf,\t%lf,\t%.2lf,\t%lf,\t%3.6lf\r\n", 
                        itr->tim /1000,
                        itr->tim %1000,
                        itr->latitude, 
                        itr->longitude, 
                        itr->altitude, 
                        itr->hdop,
                        itr->speed_kmph,
                        itr->course_deg
                    );
            }
        }
    }

    return 0;
}


#include "mdlCan.h"
#include "mdlCanDataConfig_B0.h"
extern mdlCan* mdl_can;
void Gnss::sendCan(void){
    bool update0 = false;
    bool update1 = false;

    struct StDif{
        double *current_val;
        double last_val;
    };
    static StDif dif_tbl0[] = {
        {&currentval.longitude, 0.0},
        {&currentval.latitude, 0.0}
    };
    static StDif dif_tbl1[] = {
        {&currentval.speed_kmph, 0.0},
        {&currentval.course_deg, 0.0},
        {&currentval.altitude, 0.0},
        {&currentval.hdop, 0.0}
    };

    /* 更新チェック */
    /* 座標 */
    for(int i=0; i<sizeof(dif_tbl0)/sizeof(dif_tbl0[0]); ++i){
        if(dif_tbl0[i].last_val != *(dif_tbl0[i].current_val)){   /* 前回値と異なる */
            if(0.0 != *(dif_tbl1[i].current_val)){  /* 無効値 */
                update0 = true;
            }
            dif_tbl0[i].last_val = *(dif_tbl0[i].current_val);    /* 前回値更新 */
        }
    }
    /* 速度他 */
    for(int i=0; i<sizeof(dif_tbl1)/sizeof(dif_tbl1[0]); ++i){
        if(dif_tbl1[i].last_val != *(dif_tbl1[i].current_val)){   /* 前回値と異なる */
            update1 = true;
            dif_tbl1[i].last_val = *(dif_tbl1[i].current_val);    /* 前回値更新 */
        }
    }

    /* CAN送信 */
    if(update0){
        stMdlCan_B0_TxVal.GpsLongitude = currentval.longitude;
        stMdlCan_B0_TxVal.GpsLatitude = currentval.latitude;

        mdl_can->send(&CanMsg_B0_Tx[5]);    /* 経度 *//* インデックス指定なのが不安 */
        mdl_can->send(&CanMsg_B0_Tx[6]);    /* 緯度 *//* インデックス指定なのが不安 */
    }
    if(update1){
        stMdlCan_B0_TxVal.GpsSpd_kmph = (uint16_t)(currentval.speed_kmph*100.0f);
        stMdlCan_B0_TxVal.GpsDir = (uint16_t)(currentval.course_deg*100.0f);
        stMdlCan_B0_TxVal.GpsAltitude = (int16_t)(currentval.altitude*100.0f);
        stMdlCan_B0_TxVal.GpsHdop = (uint8_t)currentval.hdop;

        mdl_can->send(&CanMsg_B0_Tx[7]);    /* 速度向きhdop *//* インデックス指定なのが不安 */
    }

}

void Gnss::sendCmd(uint16_t PktType, uint32_t DataField){
    char strtmp[32];
    if(0xFFFFFFFF != DataField){
        sprintf(strtmp, "%d,%d", PktType, DataField);
    }else{
        /* データフィールドなし */
        sprintf(strtmp, "%d", PktType);
    }
    
    sendCmd(strtmp);
}

void Gnss::sendCmd(uint16_t PktType, std::vector<uint32_t> DataField){
    char str[32];
    std::string strcmd;

    sprintf(str, "%d", PktType);
    strcmd += std::string(str);

    for(auto itr=DataField.begin(); itr!=DataField.end(); ++itr){
        sprintf(str, "%d", *itr);
        strcmd += ",";
        strcmd += std::string(str);
    }
    
    sendCmd(strcmd.c_str());
}

void Gnss::sendCmd(const char *PktTypeDataField){
    char strtmp[32];
    sprintf(strtmp, "PMTK%s", PktTypeDataField);

    uint8_t cs = checksum(strtmp);

    ss.printf("$%s*%02X\r\n", strtmp, cs);

    log_i("$%s*%02X\r\n", strtmp, cs);
}

uint8_t Gnss::checksum(char *str){
    uint8_t s = 0;

    for(int i=0; *str!='\0'; i){
        s ^= *str;
        ++str;
    }

    return s;
}


void Gnss::test_pit_in_motegi(void){
    static uint8_t state = 0;
    static uint32_t time = 0;

    typedef struct{
        double latitude;
        double longitude;
        uint32_t timing;
    } StGnssStream;

    StGnssStream stGnss[4] = {
        {36.53091, 140.2284, 20*1000},
        {36.53120, 140.2284, 30*1000},
        {36.53310, 140.226741, 3*1000},
        {36.53336, 140.226709, 10*1000},
    };

    switch(state){
    case 0:
        log_i("test_pit_in_motegi : befor pit entry");
        currentval.latitude = stGnss[state].latitude;
        currentval.longitude = stGnss[state].longitude;
        state = 1;
        time = millis();
        break;
    case 1:
        if(time+stGnss[state-1].timing < millis()){
            log_i("test_pit_in_motegi : enter pit");
            currentval.latitude = stGnss[state].latitude;
            currentval.longitude = stGnss[state].longitude;
            state = 2;
            time = millis();
        }
        break;
    case 2:
        if(time+stGnss[state-1].timing < millis()){ /* ピットアウト直前 */
            log_i("test_pit_in_motegi : befor pit out");
            currentval.latitude = stGnss[state].latitude;
            currentval.longitude = stGnss[state].longitude;
            state = 3;
            time = millis();
        }
        break;
    case 3:
        if(time+stGnss[state-1].timing < millis()){ /* ピットアウト */
            log_i("test_pit_in_motegi : pit out");
            currentval.latitude = stGnss[state].latitude;
            currentval.longitude = stGnss[state].longitude;
            state = 4;
            time = millis();
        }
        break;
    case 4:
        if(time+stGnss[state-1].timing < millis()){ /* 待ち */
            state = 0;
            time = millis();
        }
        break;
    default:
        state = 0;
        break;
    }
}
