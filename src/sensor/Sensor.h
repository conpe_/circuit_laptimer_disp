#pragma once

#include <Arduino.h>
#include <vector>
#include <string>

#include "Session.h"
#include "FS.h"

extern SemaphoreHandle_t   semaphore_spi;

class Sensor{
public:
    Sensor(std::string nm):
                name(nm),
                saved(true){};
                
    virtual bool available(void) = 0;
    virtual uint8_t save(Session *s) = 0;
    void stop(void){
        if(fp_sens){
            xSemaphoreTake(semaphore_spi, portMAX_DELAY);
            fp_sens.close();
            xSemaphoreGive(semaphore_spi);
        }
    };
    void write(void){
        if(fp_sens){
            xSemaphoreTake(semaphore_spi, portMAX_DELAY);
            fp_sens.flush();
            xSemaphoreGive(semaphore_spi);
        }
    };
    std::string name;
protected:
    File fp_sens;
    bool saved;     /* 保存済み */
};


