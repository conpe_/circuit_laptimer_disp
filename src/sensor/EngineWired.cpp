
#include "EngineWired.h"
#include "DataMem.h"

#define NO_PULSE_TIME   (200)   /* パルスなし時間[ms] */

EngineWired::EngineWired(std::string nm, uint8_t pulse_div):
                Engine(nm, pulse_div)
{

}


/* エンジンオブジェクト生成 */
void EngineWired::init(int gpio){
    uint8_t div = 1;
    
    DataMem::read(ENG_PULSE_DIV, &div);

    /* エンジンオブジェクト生成 */
    if(eng){
        delete eng;
    }
    eng = new EngineWired(std::string("Engine"), div);

    //xSemaphoreTake(eng->semaphore_logs, portMAX_DELAY);
    //eng->logs.clear();
    //xSemaphoreGive(eng->semaphore_logs);

    /* パルス割り込み設定 */
    pinMode(gpio, INPUT_PULLUP);
    /* インプットキャプチャ */
    mcpwm_capture_config_t input_capture_setup;
    input_capture_setup.cap_edge = MCPWM_NEG_EDGE;
    input_capture_setup.cap_prescale = 1;
    input_capture_setup.capture_cb = EngineWired::pulse;
    input_capture_setup.user_data = NULL;
    mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM_CAP_0, gpio);
    mcpwm_capture_enable_channel(MCPWM_UNIT_0, MCPWM_SELECT_CAP0, &input_capture_setup );
    
    log_i("Engine object created. port=%d, pulse div = %d", gpio, div);
}


void EngineWired::update(uint32_t t){
    static uint32_t last_cap;
    uint32_t current_time = millis();
    
    /* 新規セッション開始時にクリア */
    if((0 != t)&&(0==last_session_time)){
        xSemaphoreTake(semaphore_logs, portMAX_DELAY);
        logs.clear();
        xSemaphoreGive(semaphore_logs);
    }
    last_session_time = t;

    if(last_cap_time != last_cap){
        uint32_t rpm = pulse_freq*60;

        /* ストローク違い */
        rpm = rpm * pulse_div;

        /* それなりの値なら更新 */
        if(rpm < eng->getMaxSpd()){
            spd_rpm = rpm;
            saved = false;
            
            last_pulse_updatetime = current_time;
        }
    }
    last_cap = last_cap_time;

    /* パルスが一定時間来なかったらリセット */
    if(last_pulse_updatetime + NO_PULSE_TIME < current_time ){
        if(spd_rpm > 0){
            saved = false;
            log_i("Engine:: stopped.");
            spd_rpm = 0;
        }
    }

    /* セッション中ならログ用に記録 */
    if(0 != t){
        if(!saved){
            EngLog currentval;
            currentval.tim = t;
            currentval.spd_rpm = spd_rpm;

            xSemaphoreTake(semaphore_logs, portMAX_DELAY);
            logs.push_back(currentval);
            xSemaphoreGive(semaphore_logs);
        }
    }

    calc_peak_spd();

    calc_spd_kmph();
}

/* パルスを検出するたびに割り込み */
bool EngineWired::pulse(mcpwm_unit_t mcpwm, mcpwm_capture_channel_id_t cap_channel, const cap_event_data_t *edata, void *user_data){
    if(!eng){
        return false;
    }

    unsigned int cnt =  edata->cap_value;
    uint32_t input_capture_cnt = cnt - eng->last_cap_time;
    eng->last_cap_time = cnt;

    if(input_capture_cnt){
        eng->pulse_freq = (80*1000*1000)/input_capture_cnt;  /* Hz */
    }

    return false;
}
