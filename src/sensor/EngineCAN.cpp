
#include "EngineCAN.h"
#include "DataMem.h"

#include "mdlCanDataConfig_B0.h"

#define NO_PULSE_TIME   (2000)   /* パルスなし時間[ms] */


EngineCAN::EngineCAN(std::string nm, uint8_t pulse_div):
                Engine(nm, pulse_div)
{

}

/* エンジンオブジェクト生成 */
void EngineCAN::init(void){
    uint8_t div = 1;
    
    DataMem::read(ENG_PULSE_DIV, &div);

    /* エンジンオブジェクト生成 */
    if(eng){
        delete eng;
    }
    eng = new EngineCAN(std::string("Engine"), div);

    if(eng){
        log_i("Engine object created. port=CAN, pulse div = %d", div);
    }else{
        log_e("Failed to create engine object. port=CAN, pulse div = %d", div);
    }
}


void EngineCAN::update(uint32_t t){
    static uint32_t last_rcv = 0;
    uint32_t current_time = millis();
    
    /* 新規セッション開始時にクリア */
    if((0 != t)&&(0==last_session_time)){
        xSemaphoreTake(semaphore_logs, portMAX_DELAY);
        logs.clear();
        xSemaphoreGive(semaphore_logs);
    }
    last_session_time = t;

    /* CAN受信値 */
    uint32_t rpm = stMdlCan_B0_RxVal.EngSpd_rpm;

    /* 受信したら次を更新 */
    /* それなりの値なら更新 */
    if((rpm < eng->getMaxSpd()) && (last_rcv != CanMsg_B0_Rx[3].last_update)){
        spd_rpm = rpm;
        saved = false;
        
        last_pulse_updatetime = current_time;
    }

    /* パルスが一定時間来なかったらリセット */
    if(last_pulse_updatetime + NO_PULSE_TIME < current_time ){
        if(spd_rpm > 0){
            saved = false;
            log_i("Engine:: stopped.");
            spd_rpm = 0;
        }
    }

    /* セッション中ならログ用に記録 */
    if(0 != t){
        if(!saved){
            EngLog currentval;
            currentval.tim = t;
            currentval.spd_rpm = spd_rpm;

            xSemaphoreTake(semaphore_logs, portMAX_DELAY);
            logs.push_back(currentval);
            xSemaphoreGive(semaphore_logs);
        }
    }


    calc_peak_spd();

    calc_spd_kmph();


    last_rcv = CanMsg_B0_Rx[3].last_update;
}
