#pragma once

#include <Arduino.h>
#include <vector>
#include <string>

#include "Sensor.h"

class Engine;

class Engine : public Sensor{
public:
    Engine(std::string nm, uint8_t pulse_div = 1);
    
    virtual void update(uint32_t t) = 0;
    
    uint16_t getMaxSpd(void);
    bool available(void){return 0 < spd_rpm_peak;}; /* エンジン動いている */
    uint8_t save(Session *s);

    uint8_t pulse_div;      /* パルス分割(エンジン回転何回に一度パルスが出るか) */
    uint16_t spd_rpm;       /* エンジン回転数 */
    uint16_t spd_rpm_peak;  /* エンジン回転数(ピークホールド) */
    float spd_kmph;         /* 換算速度 */
    
    uint32_t last_cap_time;    /* 最後にパルスを受けた時間(カウント値) */
    uint32_t pulse_freq;       /* パルス周波数[Hz] */

protected:
    struct EngLog{
        uint32_t tim;       /* 時刻 */
        uint16_t spd_rpm;    /* 回転数 */
    };
    SemaphoreHandle_t semaphore_logs;
    uint32_t last_session_time;
    std::vector<EngLog> logs;

    uint32_t last_pulse_updatetime;
    void calc_peak_spd(void);
    void calc_spd_kmph(void);
    static uint16_t pulsediff2rpm(uint32_t pulsediff_us);

private:
    uint32_t peak_update_time;

};

extern Engine* eng;
