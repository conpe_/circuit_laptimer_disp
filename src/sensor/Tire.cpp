
#include "Tire.h"
#include <SD.h>

std::vector<Tire> Tires;


void Tire::init(void){
    data_rcvd = false;
    saved = true;
}

bool Tire::available(void){
    return data_rcvd;
}


/* SDカードの情報をもとにタイヤオブジェクト生成 */
/* 失敗したらNULL返す */
/* LapTimer.iniに次の記述が必要 */
/* TIRE_FL_MAC = ac:15:85:01:0e:12 */
/* TIRE_FR_MAC = ac:15:85:01:7c:6f */
/* TIRE_RL_MAC = ac:15:85:01:0f:5f */
/* TIRE_RR_MAC = ac:15:85:01:74:14 */
std::vector<Tire>* Tire::createTireObj(void){
    File fp;
    String config_ini;
    String mac;
    int pos;

    Tires.clear();

    /* SDカードのiniファイルからTPMSのMACアドレス情報を読む */
    fp = SD.open("/LapTimer.ini", FILE_READ);
    if(!fp){
        log_i("failed to load LapTimer.ini");
        return nullptr;
    }
    
    /* サイズ分ループ */
    while(fp.available()){
        config_ini = config_ini + fp.readString();
    }
    /* ファイルクローズ */   
    fp.close();

    /* 左前 */
    pos = config_ini.indexOf("TIRE_FL_MAC = ") + 14;
    if(0 <= pos){
        mac = config_ini.substring(pos, config_ini.indexOf("\r\n", pos));
        if(mac.length() >= 17){   /* macアドレス分の文字列有り */
            Tires.push_back(Tire(std::string("TireFL"), std::string(mac.c_str())));
            log_i("create tire FL : %s", mac.c_str());
        }
    }
    
    /* 右前 */
    pos = config_ini.indexOf("TIRE_FR_MAC = ") + 14;
    if(0 <= pos){
        mac = config_ini.substring(pos, config_ini.indexOf("\r\n", pos));
        if(mac.length() >= 17){   /* macアドレス分の文字列有り */
            Tires.push_back(Tire(std::string("TireFR"), std::string(mac.c_str())));
            log_i("create tire FR : %s", mac.c_str());
        }
    }
    /* 左後 */
    pos = config_ini.indexOf("TIRE_RL_MAC = ") + 14;
    if(0 <= pos){
        mac = config_ini.substring(pos, config_ini.indexOf("\r\n", pos));
        if(mac.length() >= 17){   /* macアドレス分の文字列有り */
            Tires.push_back(Tire(std::string("TireRL"), std::string(mac.c_str())));
            log_i("create tire RL : %s", mac.c_str());
        }
    }
    /* 右後 */
    pos = config_ini.indexOf("TIRE_RR_MAC = ") + 14;
    if(0 <= pos){
        mac = config_ini.substring(pos, config_ini.indexOf("\r\n", pos));
        if(mac.length() >= 17){   /* macアドレス分の文字列有り */
            Tires.push_back(Tire(std::string("TireRR"), std::string(mac.c_str())));
            log_i("create tire RR : %s", mac.c_str());
        }
    }

    if(Tires.size()==4){
        return &Tires;
    }else{
        log_i("Tire config failed. \r\nCheck the LapTimer.ini at SD card.");
        return nullptr;
    }

}

void Tire::deleteTireObj(void){
    Tires.clear();
}


bool Tire::isEnableTireData(void){
    return 0<Tires.size();
};


Tire* Tire::find(std::string nm){
    
    for(auto tire = Tires.begin(); tire!=Tires.end(); ++tire){
        if(tire->name == nm){
            return &(*tire);
        }
    }

    return nullptr;
}

void Tire::tpms_callback(std::vector<uint8_t>* data){
    if(5>data->size()){
        return;
    }

    int temp = data->at(2);
    float press = ((float)((uint16_t)data->at(3)<<8 | (uint16_t)data->at(4)) - 145U)*6.8947f/10.0f;
    press = press<0.0f?0.0f:press;
    
    #if ARDUHAL_LOG_LEVEL >= ARDUHAL_LOG_LEVEL_INFO
    Serial.printf("%s: ", this->name.c_str());
    for(auto it=data->begin(); it!=data->end(); ++it){
        Serial.printf("%02x ", *it);
    }
    Serial.printf(" : temp=%d[deg] press=%3.1f[kPa] %d\r\n", temp, press, (uint16_t)press);  
    #endif
    
    this->Pressure_kpa = (uint16_t)(press*10.0f);
    this->Temp_deg = (int8_t)temp;

    this->data_rcvd = true;
    this->saved = false;

}

uint8_t Tire::save(Session *s){
    if(!s){
        return 2;
    }
    if(!available()){
        return 3;
    }

    /* 初回 */
    if(!fp_sens){
        FS &fs = SD;
        /* ファイル名 */
        char dirname[20];
        char filename[20];
        char filepath[40];
        tm stime;
        s->sessionInfo(&stime);

        sprintf(dirname, "/laps/%04d%02d%02d", stime.tm_year+1900, stime.tm_mon+1, stime.tm_mday);
        sprintf(filename, "%02d%02d%02d_%s.txt", stime.tm_hour, stime.tm_min, stime.tm_sec, name.c_str());
        sprintf(filepath, "%s/%s", dirname, filename);

        fp_sens = fs.open(filepath, FILE_APPEND);

        if(fp_sens.position()){    /* すでにあるなら追記 */
            if(!fp_sens){
                log_i("Failed to open %s.", name.c_str());
                return 1;
            }
        }else{
            if(fp_sens){
                /* ヘッダ書き込み */
                fp_sens.printf("Sensor Log\r\n");
                fp_sens.printf("Name : %s\r\n", name.c_str());
                fp_sens.printf("MAC : %s\r\n", address.toString().c_str());
                fp_sens.printf("----------\r\n");
                fp_sens.printf("time,\tpressure_kPa,\ttemperature_deg\r\n");
                
                fp_sens.close();
                log_i("Create %s.", name.c_str());
            }else{
                log_i("Failed to create %s.", name.c_str());
                return 1;
            }
        }
    }

    if(!saved && fp_sens){    /* データ更新時 */
        saved = true;

        /* データ書き込み */
        fp_sens.printf("%5d.%03d,\t%d.%01d,\t%d\r\n", 
                s->passedTime()/1000,
                s->passedTime()%1000,
                Pressure_kpa/10,
                Pressure_kpa%10,
                Temp_deg
            );
    }
    
    return 0;
}
