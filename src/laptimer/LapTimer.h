
//#pragma once
#ifndef LAPTIMER_H
#define LAPTIMER_H

#include <Arduino.h>
#include <vector>

#include "Session.h"

/* ラップタイム計測処理 */
class LapTimer{
public:
    enum TimState{
        STOP,
        STANDBY,
        COUNTING,
        NONE
    };

    LapTimer(void);
    ~LapTimer(void);

    int init(void);     /* 初期化 */
    void task(void);

    int start(void);    /* 計測開始 */
    int stop(void);     /* 計測停止 */
    int lap(uint8_t mode=0);      /* ラップ頭出し(0:次の検知から第1セクター, 1:計測中セクターが第1セクター) */
    int tick(uint32_t detect_time_ms);     /* ラップ検知 */

    TimState getState(void){return state;};
    uint8_t getLap(int16_t getlap, uint8_t* lap, uint32_t* time);   /* 0:今のラップ，1:1周目のラップ，1:2周目のラップ…，-1:前のラップ，-2:2周前のラップ… */
    uint8_t getBestLap(uint8_t* best_lap, uint32_t* best_time);
    uint8_t getSector(void);            /* 計測中のセクター 1インデックス */
    uint8_t getSectorNum(void){return sector_num;};

    Session* getSession(void){return session;};          /* セッションを渡す */

    uint32_t pitTime(void){
        if(PitEntryTime>0){
            return millis()-PitEntryTime;
        }else{
            return 0;
        }
    };      /* ピット入ってからの時間(0のときピットではない) */

private:
    TimState state;
    uint8_t sector_num;
    uint32_t LapStartTime;      /* 周の開始時間(ローカル) */
    Session* session;           /* 計測中セッション */

    uint32_t StartDetectTime;   /* セッション開始時の時間(センサ時間) */
    uint32_t LastDetectTime;    /* 最後に検知した時間(センサ時間) */

    uint32_t PitEntryTime;      /* ピットに入った時刻 */
    struct StPos{
        double x;
        double y;
    };
    StPos PitEntryLine[2];
    StPos PitOutLine[2];
    StPos LastPos;
    void updatePit(void);       /* ピット入っているか判定 */

    uint8_t startNewSession(void);
    void closeCurrentSession(void);
    void saveCurrentSession(void);
    bool mkdir(FS &fs, char * dirname);     /* サブディレクトリ対応 */

};

extern LapTimer lap_timer;


#endif
