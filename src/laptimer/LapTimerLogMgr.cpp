
#include <M5EPD.h>
#include <WiFi.h>
#include <string>
#include "esp_sntp.h"
#include "time.h"

#include "LapTimerLogMgr.h"
#include "DataMem.h"
#include "LapTimer.h"

#include "Gnss.h"
#include "Engine.h"
#include "Tire.h"


//#define FILE_DIR "/LapTimer/"
#define FILE_DIR "/"
#define FILE_BASE_NAME "LapLog"
#define FILE_EXT ".csv"

#define SD_LOG_DIR "/laps"

extern SemaphoreHandle_t   semaphore_spi;
extern SemaphoreHandle_t   semaphore_session;

bool LapTimerLogMgr::SdOpened = false;

int LapTimerLogMgr::init(void){
    // SD.beginはM5.begin内で処理されているので不要
    checkSdCard();

    return 0;
}

void LapTimerLogMgr::task(void){

    /* センサ値の記録 */
    task_saveSensors();

}

bool LapTimerLogMgr::checkSdCard(void){
    /* ファイルを開く(作る)ことで確認 */
    xSemaphoreTake(semaphore_spi, portMAX_DELAY);
    File fp = SD.open("/sdcheck.ini", FILE_APPEND);

    if(fp){
        SdOpened = true;
        fp.close();
    }else{
        SdOpened = false;
    }
    xSemaphoreGive(semaphore_spi);

    return SdOpened;
}

std::string LapTimerLogMgr::makeWriteLine(uint8_t lap, LapTimeLog* laptimelog){
    std::string write_line_str;
    char tmp_str[128];

    /* 一周のタイム */
    uint32_t laptime;
    laptime = laptimelog->getLapTime();

    /* ラップ番号，ラップタイム, セクタ数 書き出し */
    if(0<laptime){
        sprintf(tmp_str, "%d,%.3f,%d", lap, (float)laptime/1000.0f,laptimelog->getSectorNum());
    }else{
        sprintf(tmp_str, "%d,,%d", lap,laptimelog->getSectorNum());
    }
    write_line_str += std::string(tmp_str);

    /* セクタータイム書き出し */
    //if(1 < laptimelog->getSectorNum()){
        for(int i=0; laptimelog->getSectorNum() > i; ++i ){
            uint32_t sec_time = laptimelog->getSectorTime(i);
            if(0!=sec_time){
                sprintf(tmp_str, ",%.3f", (float)sec_time/1000.0f);
            }else{
                sprintf(tmp_str, ",");
            }
            write_line_str += std::string(tmp_str);
        }
    //}

    write_line_str += "\n";

    return write_line_str;
}



/* ラップタイム型 */
LapTimeLog::LapTimeLog(uint8_t a_sector_num):sector_num(a_sector_num){
    sector_time.assign(sector_num, 0);
};
LapTimeLog::LapTimeLog(SectorTimeLog& sectortime):sector_num(sectortime.sector_num){
    sector_time.assign(sector_num, 0);
    setSectorTime(sectortime);
}

uint8_t LapTimeLog::setSectorTime(uint8_t sector, uint32_t time){
    if(checkSector(sector)){
        return 1;
    }
    sector_time.at(sector) = time;
    return 0;
}

uint8_t LapTimeLog::setSectorTime(SectorTimeLog& sectortime){
    if(!sectortime.isAvailable()){  /* 無効なタイム */
        return 1;
    }
    if(sectortime.sector_num != sector_num){   /* セクタ数違う */
        return 1;
    }

    return setSectorTime(sectortime.sector, sectortime.time_ms);
}

uint32_t LapTimeLog::getLapTime(void){
    uint32_t laptime = 0;
    for(auto itr=sector_time.begin(); itr!=sector_time.end(); itr++){
        if(*itr != 0){
            laptime += *itr;
        }else{
            return 0;   /* 0secのセクターがあったらラップタイムは0(無効値)とする */
        }
    }

    return laptime;
};

uint32_t LapTimeLog::getSectorTime(uint8_t sector){
    if(checkSector(sector)){
        return 0;
    }
    return sector_time.at(sector);
};

bool LapTimeLog::checkSector(uint8_t sector){
    if(sector_time.size() <= sector){    /* セットされてるセクター数より大きい */
        return 1;
    }
    return 0;
}

bool LapTimeLog::isAvailable(void){
    for(auto itr=sector_time.begin(); itr!=sector_time.end(); ++itr){
        if(*itr == 0){
            return false;   /* タイム0はNG */
        }
    }

    return true;
}


/* ログされている日付のリスト */
/* YYYYMMDDのリストを返す */
std::vector<std::string> LapTimerLogMgr::listSessionLogDate(void){
    fs::FS &fs = SD;
    const char dirname[] = SD_LOG_DIR;
    std::vector<std::string> log_date_list;
    
    xSemaphoreTake(semaphore_spi, portMAX_DELAY);
    File root = fs.open(dirname);
    if(!root){
        SdOpened = false;
        xSemaphoreGive(semaphore_spi);
        return log_date_list;
    }
    SdOpened = true;

    /* ルートがディレクトリでないなら処理終了 */
    if(!root.isDirectory()){
        xSemaphoreGive(semaphore_spi);
        return log_date_list;
    }
    File file = root.openNextFile();
    while(file){
        std::string filename(file.name());
        if(file.isDirectory() && (8==filename.size())){
            log_date_list.push_back(filename);
        }
        
        file = root.openNextFile();
    }
    xSemaphoreGive(semaphore_spi);

    /* 日付昇順 */
    sort(log_date_list.begin(), log_date_list.end());

    return log_date_list;
}

/* ログされているセッションのリスト */
/* date : YYYY/MM/DD もしくはYYYYMMDD */
/* hhmmssのリストを返す */
std::vector<std::string> LapTimerLogMgr::listSession(std::string date){
    fs::FS &fs = SD;
    std::vector<std::string> sessions_str;

    String dir_str(SD_LOG_DIR);
    String dir_date_str(date.c_str());

    dir_date_str.replace("/", "");   /* "/"を取り除く */

    dir_str = dir_str + "/" + dir_date_str;

    log_i("list session");
    
    xSemaphoreTake(semaphore_spi, portMAX_DELAY);
    File root = fs.open(dir_str.c_str());
    if(!root){
        xSemaphoreGive(semaphore_spi);
        return sessions_str;
    }
    /* ルートがディレクトリでないなら処理終了 */
    if(!root.isDirectory()){
        return sessions_str;
    }
    File file = root.openNextFile();
    while(file){
        if(!file.isDirectory()){
            String filename = file.name();

            log_i("%s", filename.c_str());
            
            if(6 == filename.indexOf(".txt")){   /* ファイル名チェック "hhmmss.txt" */
                sessions_str.push_back(std::string(filename.substring(0,6).c_str()));
            }
        }
        
        file = root.openNextFile();
    }
    xSemaphoreGive(semaphore_spi);

    /* 日付昇順 */
    sort(sessions_str.begin(), sessions_str.end());

    return sessions_str;
}

/* セッション */
/* date : YYYY/MM/DD もしくはYYYYMMDD */
/* session : hh:mm:ss もしくはhhmmss */
uint8_t LapTimerLogMgr::loadSession(std::string date, std::string session, Session *session_ret){
    fs::FS &fs = SD;
    std::vector<std::string> sessions_str;
    uint8_t ret = 0;

    log_i("load session");

    String dir_str(SD_LOG_DIR);
    String dir_date_str(date.c_str());
    String file_session_str(session.c_str());

    dir_date_str.replace("/", "");          /* "/"を取り除く */
    file_session_str.replace(":", "");      /* "/"を取り除く */

    file_session_str = dir_str + "/" + dir_date_str + "/" + file_session_str + ".txt";

    log_i("%s", file_session_str.c_str());

    xSemaphoreTake(semaphore_spi, portMAX_DELAY);
    File fp = SD.open(file_session_str, FILE_READ);
    if(fp){
        ret = session_ret->loadFile(&fp);
        fp.close();
        if(ret){
            log_i("failed. 0x%02X", ret);
        }
    }else{
        log_i("failed to open file.");
        ret = 0x20;
    }
    xSemaphoreGive(semaphore_spi);

    return ret;
}

/* その日のログ */
/* date : YYYY/MM/DD もしくはYYYYMMDD */
std::vector<Session> LapTimerLogMgr::loadSessionLogsInADay(std::string date){
    fs::FS &fs = SD;
    std::vector<Session> sessions;
    Session session_tmp;

    String dir_str(SD_LOG_DIR);
    String dir_date_str(date.c_str());

    dir_date_str.replace("/", "");   /* "/"を取り除く */

    dir_str = dir_str + "/" + dir_date_str;

    
    File root = fs.open(dir_str.c_str());
    if(!root){
        return sessions;
    }
    /* ルートがディレクトリでないなら処理終了 */
    if(!root.isDirectory()){
        return sessions;
    }
    File file = root.openNextFile();
    while(file){
        if(!file.isDirectory()){
            String filename = file.name();
            if((10 == filename.length()) && (0<=filename.indexOf(".txt"))){   /* ファイル名チェック */
                
                String filepath = dir_str + "/" + filename;
                log_i("open %s", filepath.c_str());

                File fp = SD.open(filepath, FILE_READ);
                if(fp){
                    session_tmp.loadFile(&fp);
                    fp.close();
                }else{
                    log_i("failed.");
                }
                sessions.push_back(session_tmp);
            }
        }
        
        file = root.openNextFile();
    }

    return sessions;
}

void LapTimerLogMgr::task_saveSensors(void){
    static uint32_t last_saved = 0;
    
    if(LapTimer::TimState::COUNTING == lap_timer.getState()){   /* 計測中 */
        if(last_saved + 30*1000 < millis()){
            write();     /* 一旦保存するために止める */
            last_saved = millis();
        }

        save();
    }else{
        stop();
        last_saved = millis();
    }

}

void LapTimerLogMgr::save(void){
    Session *s;
    Session s_cpy;

    xSemaphoreTake(semaphore_session, portMAX_DELAY);
    s = lap_timer.getSession();
    if(s){
        s_cpy = *s;
    }
    xSemaphoreGive(semaphore_session);

    if(!s){
        return;
    }


    uint32_t a = millis();
    xSemaphoreTake(semaphore_spi, portMAX_DELAY);
    /* GNSS */
    gnss.save(&s_cpy);

    /* ENGINE */
    if(eng){
        eng->save(&s_cpy);
    }

    /* TIRE */
    for(auto itr=Tires.begin(); itr!=Tires.end(); ++itr){
        itr->save(&s_cpy);
    }
    xSemaphoreGive(semaphore_spi);
    log_i("save sens %d", millis()-a);
}

void LapTimerLogMgr::stop(void){
    /* 止める */
    /* 計測止まったか、xx秒ごと */

    /* GNSS */
    gnss.stop();
    /* ENGINE */
    if(eng){
        eng->stop();
    }
    /* TIRE */
    for(auto itr=Tires.begin(); itr!=Tires.end(); ++itr){
        itr->stop();
    }
}

void LapTimerLogMgr::write(void){
    /* SD書き込み実行？ */
    /* xx秒ごと */
    /* 実はやらなくても保存されてる */

    /* GNSS */
    gnss.write();
    /* ENGINE */
    if(eng){
        eng->write();
    }
    /* TIRE */
    for(auto itr=Tires.begin(); itr!=Tires.end(); ++itr){
        itr->write();
    }
}
