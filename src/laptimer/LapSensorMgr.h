#pragma once

#include <stdint.h>
#include <string>

#include "DataMem.h"

#include "LapSensorCan.h"
#include "LapSensorBt.h"
#include "LapSensorWired.h"


/* ラップタイム通知 */
class LapSensorMgr{
public:
    enum TYPE{
        CAN,
        BT,
        GPIO,
        NONE = 255
    };
    static TYPE selectedType(void){
        uint8_t sens_sel = NONE;
        DataMem::read(LAP_SENS_SEL, &sens_sel);
        return (TYPE)sens_sel;
    };

    /* センサオブジェクト取得(引数なしなら選択中のもの) */
    static LapSensor* sensObj(void){
        return sensObj(selectedType());
    }
    static LapSensor* sensObj(LapSensorMgr::TYPE sens_type){
        switch(sens_type){
        case LapSensorMgr::TYPE::BT:
            return &lapsensor_bt;
        case LapSensorMgr::TYPE::GPIO:
            return &lapsensor_wired;
        case LapSensorMgr::TYPE::CAN:
            return &lapsensor_can;
        }

        return nullptr;
    };

};
