
#include "Session.h"

#include <string>
#include "esp_sntp.h"

#include "SD.h"


Session::Session(tm* start_time, uint8_t sector_num)
{
    StartTime = *start_time;
    SectorNum = sector_num;
    StartTimeLocal = millis();
}

Session::~Session(void){

}

/*  */
bool Session::tick(uint32_t time_ms){

    if(Laps.size() >= MAX_LAPS){    /* 最大ラップ数制限 */
        return false;
    }

    CurrentLap.push_back(time_ms); /* 今回の検知時刻を追加 */

    if(SectorNum <= CurrentLap.size()){ /* 一周分完了 */
        Laps.push_back(CurrentLap);     /* 完了したラップを追加 */
        CurrentLap.clear();

        return true;   /* new lap */
    }else{
        return false;
    }
}

void Session::clear(void){
    SectorNum = 0;
    Laps.clear();
    CurrentLap.clear();
}

uint32_t Session::passedTime(void){
    return millis() - StartTimeLocal;
}

bool Session::isempty(void){
    return (SectorNum==0) && (0==CurrentLap.size()) && (0==Laps.size());
}

/* セッション情報の取得 */
void Session::sessionInfo(tm* start_time, uint8_t* sector_num, uint8_t* laps){
    if(start_time){
        *start_time = StartTime;
    }
    if(sector_num){
        *sector_num = SectorNum;
    }
    if(laps){
        *laps = Laps.size();
    }
}

// 特定ラップの情報を取得
// lap =
//      < 0 : 過去ラップ
//      = 0 : 現在ラップ
//      > 0 : 任意ラップ(1インデックス)
// 戻り値
//      0:エラー
//      1～:返したラップ(1インデックス)
uint8_t Session::lapInfo(int16_t lap, uint32_t* lap_time, std::vector<uint32_t>* sector_time){
    int16_t retlap;     // 返すラップ(0インデックス)
    uint32_t ret_lap_time;
    std::vector<uint32_t> ret_sector_time_tmp;
    std::vector<uint32_t> ret_sector_time;

    // 0インデックス
    if(0==lap){  // 今のラップ
        retlap = Laps.size();
    }else if(0<lap){     // ラップ指定
        retlap = lap - 1;   
    }else{  // マイナス指定
        retlap = Laps.size() + lap;
    }

    // 範囲確認
    if( (0 > retlap) || ((Laps.size() <= retlap)&&(0 != lap)) ){
        return 0;
    }

    // 返す値設定
    // 時刻から時間に置き換える
    if(0==lap){  // 今のラップ
        ret_sector_time_tmp = CurrentLap;
        ret_lap_time = 0;      // 計算できないので0
    }else{
        ret_sector_time_tmp = Laps.at(retlap);
        if(ret_sector_time_tmp.size() == SectorNum){    // セクター数分データあり
            if( 0 == retlap){
                ret_lap_time = ret_sector_time_tmp.back() - 0;
            }else{
                ret_lap_time = ret_sector_time_tmp.back() - Laps.at(retlap-1).back();
            }
        }else{
            ret_lap_time = 0;
        }
    }

    // セクタタイムを絶対値からタイムへ変換
    for(int i=0; ret_sector_time_tmp.size()>i; ++i){
        if(0==i){   // 先頭は前のラップの最後との差をとる
            if( (0 == retlap) || (0==Laps.size())){
                ret_sector_time.push_back(ret_sector_time_tmp.at(i) - 0);
            }else{
                ret_sector_time.push_back(ret_sector_time_tmp.at(i) - Laps.at(retlap-1).back());
            }
        }else{
            ret_sector_time.push_back(ret_sector_time_tmp.at(i) - ret_sector_time_tmp.at(i-1));
        }
    }

    if(lap_time){
        *lap_time = ret_lap_time;
    }
    if(sector_time){
        *sector_time = ret_sector_time;
    }

    return retlap + 1;      /* 1インデックスにして返す */
}

// 特定ラップの情報を取得
// 戻り値
//      0:エラー
//      1～:ベストラップ(1インデックス) 複数見つけても最後のやつだけ
uint8_t Session::bestLapInfo(uint32_t* best_time, std::vector<uint32_t>* best_sector_time){
    uint8_t best_lap = 0;
    uint32_t lap_time;
    uint32_t best_time_tmp = 0;
    std::vector<uint32_t> sector_time;
    std::vector<uint32_t> best_sector_time_tmp;

    for(int i=1; Laps.size() >= i; ++i){
        this->lapInfo(i, &lap_time, &sector_time);
        if((lap_time <= best_time_tmp) || (0==best_time_tmp)){   /* ベストもしくは初回 */
            best_time_tmp = lap_time;
            best_lap = i;
            best_sector_time_tmp = sector_time;
        }
    }

    if(best_lap > 0){
        if(best_time){
            *best_time = best_time_tmp;
        }
        if(best_sector_time){
            *best_sector_time = best_sector_time_tmp;
        }
    }

    return best_lap;
}

uint8_t Session::saveFile(File *fp, bool append){
    char strtmp[32];
    int sec;

    /* ヘッダ */
    if(!append){
        strftime(strtmp, 32, "%Y/%m/%d %H:%M:%S", &StartTime );

        fp->printf("Session Log\r\n");
        fp->printf("Create : %s\r\n", strtmp);
        fp->printf("Sector : %d\r\n", this->SectorNum);
        fp->printf("----------\r\n");
        fp->printf("lap,\ttime,\tlaptime");
        for(sec=0; this->SectorNum>sec; ++sec){
            fp->printf(",\tsec%d",sec+1);
        }
        fp->printf("\r\n");
    }

    uint32_t lap_time;
    std::vector<uint32_t> sector_time;

    int lap;
    if(append){
        lap = Laps.size();  /* 直近のラップのみ */
    }else{
        lap = 1;            /* はじめのラップから */
    }

    for(; Laps.size()>=lap; ++lap){
        lapInfo(lap, &lap_time, &sector_time);

        uint32_t time = Laps.at(lap-1).back();

        fp->printf("%3d,\t%02d:%02d.%03d,\t%2.3f,\t", lap, time/1000/60, (time/1000)%60, time%1000, (float)lap_time/1000);

        for(sec=0; sector_time.size()>sec; ++sec){
            fp->printf("%2.3f", (float)sector_time.at(sec)/1000);
            if(sector_time.size()-1 != sec){
                fp->printf(",\t");
            }
        }

        fp->printf("\r\n");
    }

    return 0;
}

uint8_t Session::loadFile(File *fp){
    char str_tmp[30];
    String session_str;
    int pos_start;
    int pos_end;

    tm session_start_time;
    uint8_t sector_num;

    if(!fp){
        return 1;
    }

    Laps.clear();

    /* ヘッダ読み出し */
    while(fp->available()){
        //session_str = session_str + fp.readString();
        String read_tmp = fp->readStringUntil('\n');    /* 検索用の文字はバッファに入らないので注意 */
        if(0 <= read_tmp.indexOf("----------")){  /* ヘッダ読み終わり */
            break;
        }

        session_str = session_str + read_tmp + '\n';
    }
    
    /* 日付部分を抽出 */
    pos_start = session_str.indexOf("Create : ") + 9;
    pos_end = session_str.indexOf("\r\n", pos_start);
    if((0<=pos_start) && (0<=pos_end) && 19<=(pos_end-pos_start)){
        session_start_time.tm_year = atoi(session_str.substring(pos_start, pos_start+4).c_str()) - 1900;
        pos_start += 5;
        session_start_time.tm_mon = atoi(session_str.substring(pos_start, pos_start+2).c_str()) - 1;
        pos_start += 3;
        session_start_time.tm_mday = atoi(session_str.substring(pos_start, pos_start+2).c_str());
        pos_start += 3;
        session_start_time.tm_hour = atoi(session_str.substring(pos_start, pos_start+2).c_str());
        pos_start += 3;
        session_start_time.tm_min = atoi(session_str.substring(pos_start, pos_start+2).c_str());
        pos_start += 3;
        session_start_time.tm_sec = atoi(session_str.substring(pos_start, pos_start+2).c_str());
    }else{
        /* 失敗 */
        session_start_time.tm_year = 0;
        session_start_time.tm_mon = 0;
        session_start_time.tm_mday = 0;
        session_start_time.tm_hour = 0;
        session_start_time.tm_min = 0;
        session_start_time.tm_sec = 0;

        return 2;
    }

    StartTime = session_start_time;

    strftime(str_tmp, 32, "%Y/%m/%d %H:%M:%S", &session_start_time );

    /* セクタ数を抽出 */
    pos_start = session_str.indexOf("Sector : ") + 9;
    pos_end = session_str.indexOf("\r\n", pos_start);
    if((0<=pos_start) && (0<=pos_end) && 1<=(pos_end-pos_start)){
        sector_num = atoi(session_str.substring(pos_start, pos_end).c_str());
    }else{
        /* 失敗 */
        sector_num = 0;
        return 3;
    }

    SectorNum = sector_num;

    /* ラップタイム読み出し */
    uint8_t laps_cnt = 0;       /* 読み出したラップ数(行数) */
    uint8_t laps = 0;           /* 記録されているラップ数 */

    session_str = fp->readStringUntil('\n');   /* ラベル行空読み */
    uint32_t lapend_time_ms = 0;    /* その周の終わりの時刻 */
    uint32_t lapstart_time_ms = 0;  /* その周の始まりの時刻 */
    while(fp->available()){
        /* 1行ずつ処理 */
        session_str = fp->readStringUntil('\n');
        session_str += '\n';
        if(10 > session_str.length()){    /* 変な行 */
            return 4;
        }

        /* 周回数 */
        laps = atoi(session_str.substring(0, 3).c_str());

        /* 周終わりの時刻 */
        pos_start = session_str.indexOf(",\t")+2;
        pos_end = session_str.indexOf(",\t");
        lapend_time_ms = 0;
        lapend_time_ms += atoi(session_str.substring(pos_start, pos_start+2).c_str()) * 60 * 1000;    /* 分 */
        pos_start += 3;
        lapend_time_ms += atoi(session_str.substring(pos_start, pos_start+2).c_str()) * 1000;    /* 秒 */
        pos_start += 3;
        lapend_time_ms += atoi(session_str.substring(pos_start, pos_start+3).c_str());    /* sub秒 */

        /* 各セクタの通過時刻 */
        pos_start = session_str.indexOf(",") + 1;               /* time */
        pos_start = session_str.indexOf(",", pos_start) + 1;    /* laptime */
        pos_start = session_str.indexOf(",", pos_start) + 2;    /* sec1 */

        std::vector<uint32_t> sec_time;
        uint32_t sec_time_tmp = lapstart_time_ms;
        for(int i=0; sector_num > i; ++i){
            if(sector_num > i+1){
                sec_time_tmp += atoi(session_str.substring(pos_start, session_str.indexOf(".", pos_start)).c_str()) * 1000;    /* 秒 */
                sec_time_tmp += atoi(session_str.substring(session_str.indexOf(".", pos_start) + 1, session_str.indexOf(",", pos_start) ).c_str());    /* sub秒 */
                
                pos_start = session_str.indexOf(",", pos_start) + 2;    /* 次のセクター位置 */
            }else{  /* 最後のセクターは周の最後のタイム */
                sec_time_tmp = lapend_time_ms;

                lapstart_time_ms = lapend_time_ms;      /* 次の周の始まりの時刻は今の周の最後の時刻 */
            }
            sec_time.push_back(sec_time_tmp);
        }

        Laps.push_back(sec_time);

        ++laps_cnt;
    }


    return 0;
}

