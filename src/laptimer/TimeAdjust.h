
//#pragma once
#ifndef TIMEADJUST_H
#define TIMEADJUST_H

#include <vector>
#include <string>

#define PREF_FILE "/LapTimer.ini"

class WiFiInfo{
public:
    WiFiInfo(std::string p_file = PREF_FILE);
    uint8_t load(void);
    std::string ssid;
    std::string password;
private:
    std::string pref_file;
};

/* 時刻合わせ */
class TimeAdjust{
public:
    static int init(void);
    static void task(void);

    /* NTPでの時刻合わせ開始 */
    static void startGetTimeNtp(void){ WifiNtpState = 0x00; };
    /* 時刻合わせ状態 */
    static uint8_t getStatus(void){return WifiNtpState;};
    
private:
    static uint8_t WifiNtpState;     /* 0x01:WiFi接続待ち, 0x02:取得待ち, 0xFE:失敗, 0xFF:idle */
    static uint32_t WifiNtpState_time;

    /* WiFi ntp */
    static void SetCurrentTimeNtp(void);
};

#endif
