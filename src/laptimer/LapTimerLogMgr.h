
//#pragma once
#ifndef LAPTIMERLOGMGR_H
#define LAPTIMERLOGMGR_H

#include <Arduino.h>
#include <vector>

#include "Session.h"

/* セクタータイムログ */
/* 該当のセクタ番号とタイムを記憶 */
class SectorTimeLog{
public:
    SectorTimeLog(uint8_t a_sector=0, uint8_t a_sector_num=0, uint32_t a_time_ms=0)
        :sector(a_sector), sector_num(a_sector_num), time_ms(a_time_ms){};
    uint8_t sector;             /* 0インデックス */
    uint8_t sector_num;
    uint32_t time_ms;

    bool isAvailable(void){return (0!=time_ms)&&(sector!=sector_num);};
};

/* ラップタイムログ */
/* セクタ数と各セクタのタイム，全体タイムを記録 */
class LapTimeLog{
public:
    LapTimeLog(uint8_t a_sector_num);
    LapTimeLog(SectorTimeLog& sectortime);
    
    /* セクタータイムをセット */
    uint8_t setSectorTime(uint8_t sector, uint32_t time);       /* 0インデックス */
    uint8_t setSectorTime(SectorTimeLog& sectortime);

    /* ラップタイムを取得 */
    uint32_t getLapTime(void);

    /* 指定セクターのタイムを取得 */
    uint32_t getSectorTime(uint8_t sector);     /* 0インデックス */

    /* セクター数 */
    uint8_t getSectorNum(void){return sector_num;};

    /* このセクター番号使えるか確認 */
    bool checkSector(uint8_t sector);           /* 0インデックス */

    /* 有効なラップタイムか確認 */
    bool isAvailable(void);
private:
    std::vector<uint32_t> sector_time;          /* セクタータイムリスト */
    uint8_t sector_num;

};



/* ラップタイムロギング管理 */
class LapTimerLogMgr{
public:
    static int init(void);
    static void task(void);

    /* SDカードある？ */
    static bool isSdCardAvailable(void){return SdOpened;};

    /* SDカードあるかチェック */
    static bool checkSdCard(void);  /* 1:オープンOK */

    /* SDカードに記録されているログリスト取得 */
    static std::vector<std::string> listSessionLogDate(void);                   /* ログされている日付のリスト */
    static std::vector<std::string> listSession(std::string date);     /* その日のログ一覧 */
    static uint8_t loadSession(std::string date, std::string session, Session *session_ret); /* セッションを読み込み */
    static std::vector<Session> loadSessionLogsInADay(std::string date);        /* その日のログ一括読み込み */
    
private:
    static bool SdOpened;

    /* ログ書き出し文字列生成 */
    static std::string makeWriteLine(uint8_t lap, LapTimeLog* laptimelog);
    
    /* センサ値の記録 */
    static void task_saveSensors(void);
    static void save(void);
    static void stop(void);
    static void write(void);
};

#endif
