
#include "LapSensorWired.h"
#include "LapTimer.h"

#define TICK_MASK_ms (2000)
#define TICK_PLUSE_WIDTH_ms (400)


LapSensorWired lapsensor_wired("WIRED");


int LapSensorWired::pin = 255;
uint8_t LapSensorWired::state = 0;
uint32_t LapSensorWired::tick_time_ms = 0;
uint32_t LapSensorWired::tick_last = 0;

void LapSensorWired::init(int gpio){
    pin = gpio;
    pinMode(pin, INPUT_PULLDOWN);
    attachInterrupt(pin, LapSensorWired::tick, RISING);
    state = 1;

    log_i("LapSensorWired initialized.");
}

void LapSensorWired::deinit(void){
    if(0!=state){
        detachInterrupt(pin);
        state = 0;

        log_i("LapSensorWired deinitialized.");
    }
}

void LapSensorWired::tick(void){
    /* 前回検知からマスク時間以上たってからであれば更新 */
    if(1 == state){
        if(tick_last + TICK_MASK_ms < millis()){
            tick_time_ms = millis();
            state = 2;  /* tick確定待ち */
        }
    }
}

void LapSensorWired::task(void){
    /* パルス幅が1秒以上であれば検知とする */

    if(0==state){
        return;
    }

    /* tick確定待ち状態で検知からxx秒たったらtick */
    if((2==state) && (millis() > tick_time_ms + TICK_PLUSE_WIDTH_ms)){
        if(digitalRead(pin)){   /* パルス幅時間以上経ってまだhighならtick */
            lap_timer.tick(tick_time_ms);        
            tick_last = tick_time_ms;
            log_i("LapSensorWired ticked at %d.", tick_time_ms);
        }
        state = 1;
    }

}

