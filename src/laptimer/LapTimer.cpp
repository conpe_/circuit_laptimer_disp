#include "LapTimer.h"
#include "DataMem.h"
#include "LapTimerLogMgr.h"
#include "Gnss.h"

#include "FS.h"
//#include "SPIFFS.h"
#include "SD.h"


extern SemaphoreHandle_t   semaphore_spi;
extern SemaphoreHandle_t   semaphore_session;

#include "mdlCan.h"
#include "mdlCanDataConfig_B0.h"
extern mdlCan* mdl_can;

LapTimer lap_timer;


#define SECTOR_NUM_DEFAULT mem_sec.Init
#define AUTO_STOP_TIME_S (10*60)             /* 自動で計測停止する時間 */

LapTimer::LapTimer(void):
            sector_num(SECTOR_NUM_DEFAULT),
            session(NULL),
            PitEntryTime(0)
{

    init();

    LastPos.x = 140.228106; /* もてぎピット内 */
    LastPos.y = 36.531768;

    /* ピットエントリーライン(もてぎ) */
    PitEntryLine[0].x = 140.22824;
    PitEntryLine[0].y = 36.53101;
    PitEntryLine[1].x = 140.2286;
    PitEntryLine[1].y = 36.53118;
    /* ピットエンドライン(もてぎ) */
    PitOutLine[0].x = 140.2265;
    PitOutLine[0].y = 36.53315;
    PitOutLine[1].x = 140.2269;
    PitOutLine[1].y = 36.53335;
    
}
LapTimer::~LapTimer(void){
    if(session){
        delete session;
    }
}

int LapTimer::init(void){
    state = TimState::STOP;

    LastDetectTime = 0;

    if(session){
        delete session;
    }


    return 0;
}

/* 周期処理 */
void LapTimer::task(void){

    /* ピット */
    updatePit();

    /* セクタ数読み込み */
    if(DataMem::read(MEM_SEC, &sector_num)){
        sector_num = SECTOR_NUM_DEFAULT;    /* 読めなかったら初期値 */
    }

    /* 一定時間検知がなかったら止める */
    if(LapStartTime + AUTO_STOP_TIME_S*1000 < millis()){
        stop();
    }

}


int LapTimer::start(void){    /* 計測開始 */

    log_i("LapTimer::start");

    return lap(0);      /* ラップ頭出し待ち状態 */
}

int LapTimer::stop(void){     /* 計測停止 */
    if(TimState::STOP != state){
        this->closeCurrentSession();
        log_i("LapTimer::stop");
    }
    
    state = TimState::STOP;

    return 0;
}

int LapTimer::lap(uint8_t mode){      /* ラップ頭出し(次の検知から第1セクター) */
    if(0==mode){    /* 次のセクターから測定開始 */
        state = TimState::STANDBY;

        log_i("LapTimer:: set to STANDBY");
        
    }else if(1==mode){      /* 今のセクターが第1セクター */
        if(TimState::COUNTING == state){

            StartDetectTime = LastDetectTime;

            /* 新しい計測セッションを開始 */
            this->startNewSession();
        }
    }
    return 0;
}

int LapTimer::tick(uint32_t detect_time_ms){      /* ラップ検知 */
    
    log_i("tick(%d)", detect_time_ms);

    if(TimState::COUNTING == state){

        if(detect_time_ms <= StartDetectTime){
        /* 時間が戻った。おかしい */
            log_w("invalid ticktime. StartDetectTime is %d", StartDetectTime);
            stop();
            return 1;
        }
        if(detect_time_ms <= LastDetectTime){
            log_w("invalid ticktime. LastDetectTime is %d", LastDetectTime);
            stop();
            return 1;
        }
        
        log_i("time = %d[ms]", detect_time_ms - LastDetectTime);

        /* 記録 */
        bool new_lap = session->tick(detect_time_ms-StartDetectTime);
        /* ラップ開始時刻を保持(モニタ表示用) */
        if(new_lap){
            LapStartTime = millis();
            
            uint8_t laps;
            session->sessionInfo(NULL, NULL, &laps);
            if(laps >= session->MAX_LAPS){      /* セッションの最大周回数になった */
                log_i("reach max laps. start new session.");
                /* セッション開始時の検知時刻 */
                StartDetectTime = detect_time_ms;
                this->startNewSession();
            }

        }

    }else{  /* 未スタート状態で検知したらスタートする */
    

        LapStartTime = millis();

        /* セッション開始時の検知時刻 */
        StartDetectTime = detect_time_ms;

        /* 新しい計測セッションを開始 */
        if(0 == this->startNewSession()){
            state = TimState::COUNTING;
        }else{
            state = TimState::STOP;
        }
    }

    /* CAN送信 */
    uint8_t lap;
    uint32_t time;
    stMdlCan_B0_TxVal.Tick = 1;
    if(0 == getLap(-1, &lap, &time)){
      stMdlCan_B0_TxVal.Lap = lap;
      stMdlCan_B0_TxVal.LapTime_ms = time;
    }else{
      stMdlCan_B0_TxVal.Lap = 0;
      stMdlCan_B0_TxVal.LapTime_ms = 0xFFFFFFFF;
    }
    mdl_can->send(&CanMsg_B0_Tx[1]);    /* インデックス指定なのが不安 */

    stMdlCan_B0_TxVal.TickTime_ms = detect_time_ms-StartDetectTime;
    mdl_can->send(&CanMsg_B0_Tx[2]);


    LastDetectTime = detect_time_ms;

    return 0;
}

/*  */
uint8_t LapTimer::startNewSession(void){
    this->closeCurrentSession();

    time_t current = time(NULL);
    struct tm* start_time = localtime(&current);

    xSemaphoreTake(semaphore_session, portMAX_DELAY);
    session = new Session(start_time, sector_num);
    xSemaphoreGive(semaphore_session);

    if(session){
        log_i("Start session.");
    }
    return (NULL == session)?1:0;
}

/* セッションを保存して閉じる */
void LapTimer::closeCurrentSession(void){
    if(session){
        /* セッション保存 */
        uint32_t a = millis();
        saveCurrentSession();
        log_i("save %d", millis()-a);
        
        xSemaphoreTake(semaphore_session, portMAX_DELAY);
        
        log_i("Delete current session.");
        delete session;
        session = NULL;
        xSemaphoreGive(semaphore_session);

        log_i("Close session.");
    }
}

void LapTimer::saveCurrentSession(void){
    
    FS &fs = SD;
    File fp;

    char dirname[30];
    char filename[30];
    char filepath[32];

    tm stime;
    session->sessionInfo(&stime);

    sprintf(dirname, "/laps/%04d%02d%02d", stime.tm_year+1900, stime.tm_mon+1, stime.tm_mday);
    sprintf(filename, "%02d%02d%02d.txt", stime.tm_hour, stime.tm_min, stime.tm_sec);
    sprintf(filepath, "%s/%s", dirname, filename);

    xSemaphoreTake(semaphore_spi, portMAX_DELAY);

    /* フォルダなければ作る */
    if(!fs.exists(dirname)){
        log_i("Create dir [%s]... ", dirname);
        if(this->mkdir(fs, dirname)){  /* ディレクトリ名の最後に"/"があるとダメ。サブディレクトリもダメ */
            log_i("ok.");
        }else{
            log_i("failed.");
        }
    }
    /* ファイルオープン */
    fp = fs.open(filepath, FILE_WRITE);
    log_i("Open session file [%s]... ", filepath);
    if(fp){
        log_i("Save Session");
        
        /* セッション保存 */
        session->saveFile(&fp);

        fp.close();

    }else{
        log_i("failed.");
    }

    
    xSemaphoreGive(semaphore_spi);
}

// 0:失敗, 1:成功
bool LapTimer::mkdir(FS &fs, char* dirname){
    std::string str(dirname);
    
    if(str[0] == '/'){   /* 先頭が/ */
        str.replace(0, 1, "");   /* 先頭の/を消す */
    }

    std::vector<std::string> v;             // 分割結果を格納するベクター
    auto first = str.begin();               // 文字列の先頭で初期あk
    while( first != str.end() ) {           // 文字列をすべて処理
        auto last = first;                  // 分割文字列末尾
        while( last != str.end() && *last != '/' )       // 末尾 or セパレータ文字まで進める
            ++last;
        v.push_back(std::string(first, last));       // 分割文字を出力
        if(last != str.end())
            ++last;
        first = last;          // 次の処理のために検索開始位置を更新
    }

    str.clear();
    for(auto itr = v.begin(); itr!=v.end(); ++itr){
        str += '/';
        str += itr->c_str();
        if(!fs.exists(str.c_str())){
            fs.mkdir(str.c_str());
        }
    }

    return 1;
}


uint8_t LapTimer::getLap(int16_t getlap, uint8_t* lap, uint32_t* time){   /* 0:今のラップ，-1:前のラップ，… -99周前のラップ，1:1週目のラップ，2:2周目のラップ…99:99周目のラップ */
    uint32_t lap_time;
    std::vector<uint32_t> sector_time;

    uint8_t ret_lap;
    
    if(session){
        ret_lap = session->lapInfo(getlap, &lap_time, &sector_time);
    }else{
        return 1;   /* セッション未開始 */
    }
    /* エラー */
    if(0 == ret_lap){
        return 2;   /* エラー */
    }
    
    /* 今のラップはラップ開始からの時間を返す */
    if(0==getlap){
        lap_time = (millis() - LapStartTime);    /* 開始時間からの経過時間 */
        if(TimState::STOP == state){
            ret_lap = 0;                /* 停止中は今のラップもなにもないので0を返す */
        }
    }

    if(lap){
        *lap = ret_lap;
    }
    if(time){
        *time = lap_time;
    }
    
    return 0;
}

uint8_t LapTimer::getBestLap(uint8_t* best_lap, uint32_t* best_time){
    uint8_t best_lap_tmp;
    uint32_t best_time_tmp;

    if(session){
        best_lap_tmp = session->bestLapInfo(&best_time_tmp);

        if(0 == best_lap_tmp){  /* エラ－ */
            return 1;
        }

        if(best_lap){
            *best_lap = best_lap_tmp;
        }
        if(best_time){
            *best_time = best_time_tmp;
        }

    }else{
        return 1;
    }

    return 0;
}

/* 計測中のセクター */
/* 1インデックス */
uint8_t LapTimer::getSector(void){
    std::vector<uint32_t> sector_time;

    if(session){
        if(0 != session->lapInfo(0, NULL, &sector_time)){
            return sector_time.size() + 1;
        }
    }

    return 0;
}

/* ピット */
void LapTimer::updatePit(void){
    StPos CurrentPos;

    CurrentPos.x = gnss.currentval.longitude;
    CurrentPos.y = gnss.currentval.latitude;
    
    double tc;
    double td;
    double ta;
    double tb;

    tc = (PitEntryLine[0].x-PitEntryLine[1].x)*(LastPos.y-PitEntryLine[0].y) + (PitEntryLine[0].y-PitEntryLine[1].y)*(PitEntryLine[0].x-LastPos.x);
    td = (PitEntryLine[0].x-PitEntryLine[1].x)*(CurrentPos.y-PitEntryLine[0].y) + (PitEntryLine[0].y-PitEntryLine[1].y)*(PitEntryLine[0].x-CurrentPos.x);
    ta = (LastPos.x-CurrentPos.x)*(PitEntryLine[0].y-LastPos.y) + (LastPos.y-CurrentPos.y)*(LastPos.x-PitEntryLine[0].x);
    tb = (LastPos.x-CurrentPos.x)*(PitEntryLine[1].y-LastPos.y) + (LastPos.y-CurrentPos.y)*(LastPos.x-PitEntryLine[1].x);
    
    bool pit_entry = ta<0 && tb>0 && tc>0 && td<0;

    if(pit_entry){
        PitEntryTime = millis();
        log_i("Pit entry %d", PitEntryTime);
        log_i("cpos %lf, %lf", CurrentPos.x, CurrentPos.y);
        log_i("lpos %lf, %lf", LastPos.x, LastPos.y);
        log_i("p1pos %lf, %lf", PitEntryLine[0].x, PitEntryLine[0].y);
        log_i("p2pos %lf, %lf", PitEntryLine[1].x, PitEntryLine[1].y);
    }

    tc = (PitOutLine[0].x-PitOutLine[1].x)*(LastPos.y-PitOutLine[0].y) + (PitOutLine[0].y-PitOutLine[1].y)*(PitOutLine[0].x-LastPos.x);
    td = (PitOutLine[0].x-PitOutLine[1].x)*(CurrentPos.y-PitOutLine[0].y) + (PitOutLine[0].y-PitOutLine[1].y)*(PitOutLine[0].x-CurrentPos.x);
    ta = (LastPos.x-CurrentPos.x)*(PitOutLine[0].y-LastPos.y) + (LastPos.y-CurrentPos.y)*(LastPos.x-PitOutLine[0].x);
    tb = (LastPos.x-CurrentPos.x)*(PitOutLine[1].y-LastPos.y) + (LastPos.y-CurrentPos.y)*(LastPos.x-PitOutLine[1].x);
    
    bool pit_exit = ta<0 && tb>0 && tc>0 && td<0;

    if(pit_exit){
        log_i("Pit exit %d", millis());
        PitEntryTime = 0;
    }
    /* ピット時間10分超えたらクリア */
    if(((millis()-PitEntryTime) > 10*60*1000) && (0!=PitEntryTime)){
        log_i("Pit timeout %d", millis());
        PitEntryTime = 0;
    }

    LastPos = CurrentPos;
}


