
#include "LapSensorBt.h"
#include "LapTimer.h"
#include "BtSerialCmdMaster.h"
#include "buzz.h"

LapSensorBt lapsensor_bt("BT");

uint8_t LapSensorBt::CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len){
    
    buzz.noteSingle(BUZZ_NOTE::BUZZ_NOTE_C5);

    lap_timer.tick(*(uint32_t*)rcv_msg);

    *ret_len = 0;

    log_i("LapSensorBt ticked at %d.", *(uint32_t*)rcv_msg);
    return 0;
}


bool LapSensorBt::isConnect(void){
    return bt_cmd.isConnected();
}

