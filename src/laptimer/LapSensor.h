#pragma once

#include <stdint.h>
#include <string>

#include "DataMem.h"


/* ラップタイム通知 */
class LapSensor{
public:
    LapSensor(std::string nm):name(nm){};
    virtual void task(void) = 0;
    virtual bool isConnect(void) = 0;
    
    std::string name;
};
