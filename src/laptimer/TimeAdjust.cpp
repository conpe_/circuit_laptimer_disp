
#include "FS.h"
#include "SD.h"
#include <M5EPD.h>     /* RTC */
#include <WiFi.h>
#include <string>
#include "esp_sntp.h"
#include "time.h"

#include "TimeAdjust.h"


uint8_t TimeAdjust::WifiNtpState = 0xFF;     /* Ntpサーバーからの時刻設定状態 */
uint32_t TimeAdjust::WifiNtpState_time;

int TimeAdjust::init(void){
    // SD.beginはM5.begin内で処理されているので不要

    return 0;
}

void TimeAdjust::task(void){
    SetCurrentTimeNtp();
}

void TimeAdjust::SetCurrentTimeNtp(void){
    File fp;
    String config_ini;
    String ssid;
    String password;

    WiFiInfo wifi_info(PREF_FILE);

    tm timeInfo;
    
    rtc_time_t RTCtime;
    rtc_date_t RTCDate;
    
    switch(WifiNtpState){
    case 0x00:
        if(0 == wifi_info.load()){

            WiFi.begin(wifi_info.ssid.c_str(), wifi_info.password.c_str());

            WifiNtpState = 1;
            WifiNtpState_time = millis();
            log_i("settimestate = 0x%02X", WifiNtpState);
        }else{
            /* 失敗 */
            WifiNtpState = 0xFE;
            WifiNtpState_time = millis();
        }
        break;
    case 0x01:     /* WiFi接続待ち */
        if(WiFi.status() == WL_CONNECTED){
            
            /* NTPサーバーから時刻を取得 */
            /* これでファイルシステムに時刻が設定される */
            configTzTime("JST-9", "ntp.nict.jp", "time.google.com", "ntp.jst.mfeed.ad.jp");

            WifiNtpState = 2;
            WifiNtpState_time = millis();
            log_i("settimestate = 0x%02X", WifiNtpState);
        }else if(WifiNtpState_time+10000 < millis()){
            /* 失敗 */
            WifiNtpState = 0xFE;
            WifiNtpState_time = millis();
        }
        break;
    case 0x02:
        //if( getLocalTime(&timeInfo, 100) && (SNTP_SYNC_STATUS_RESET != sntp_get_sync_status())){
        if( SNTP_SYNC_STATUS_RESET != sntp_get_sync_status() ){
            /* 取得完了 */
            WifiNtpState = 0x10;    /* 終了処理へ */
            WifiNtpState_time = millis();

            getLocalTime(&timeInfo, 100);
            log_i("get local time = %04d/%02d/%02d %02d:%02d:%02d", timeInfo.tm_year+1900, timeInfo.tm_mon+1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);

            RTCtime.hour = timeInfo.tm_hour;
            RTCtime.min  = timeInfo.tm_min;
            RTCtime.sec  = timeInfo.tm_sec;
            M5.RTC.setTime(&RTCtime);
            
            RTCDate.year = timeInfo.tm_year+1900;
            RTCDate.mon  = timeInfo.tm_mon+1;
            RTCDate.day  = timeInfo.tm_mday;
            RTCDate.week = timeInfo.tm_wday;
            M5.RTC.setDate(&RTCDate);
            
            log_i("set RTC = %04d/%02d/%02d %02d:%02d:%02d", RTCDate.year, RTCDate.mon, RTCDate.day, RTCtime.hour, RTCtime.min, RTCtime.sec);

            log_i("settimestate = 0x%02X", WifiNtpState);
        }else if(WifiNtpState_time+10000 < millis()){
            /* 失敗 */
            WifiNtpState = 0xFE;
            WifiNtpState_time = millis();
        }
        break;
    case 0x10:
        WiFi.disconnect(true);
        WiFi.mode(WIFI_OFF);

        WifiNtpState = 0xFF;
        WifiNtpState_time = millis();
        log_i("settimestate = 0x%02X", WifiNtpState);
        break;
    case 0xFE:  /* エラー */
        break;
    case 0xFF:  /* idle */
        break;
    }
}


WiFiInfo::WiFiInfo(std::string p_file)
                :pref_file(p_file)
{
    
}

/* 0:読み込み成功, 1:読み込み失敗 */
uint8_t WiFiInfo::load(void){
    File fp;
    String config_ini;
    String str_tmp;

    /* SDカードのiniファイルからWiFi情報を読む */
    fp = SD.open(pref_file.c_str(), FILE_READ);
    if(!fp){
        log_i("failed load %s\n", pref_file.c_str());
        
        return 1;
    }else{
        /* サイズ分ループ */
        while(fp.available()){
            config_ini = config_ini + fp.readString();
        }
        /* ファイルクローズ */   
        fp.close();

        /* SSID取得 */
        config_ini = config_ini.substring(config_ini.indexOf("SSID = ") + 7);
        str_tmp = config_ini.substring(0, config_ini.indexOf("\r\n"));
        ssid = str_tmp.c_str();
        /* パスワード取得 */
        config_ini = config_ini.substring(config_ini.indexOf("SSID_PASS = ") + 12);
        str_tmp = config_ini.substring(0, config_ini.indexOf("\r\n"));
        password = str_tmp.c_str();

        return 0;
    }
}
