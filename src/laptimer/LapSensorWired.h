#pragma once

#include <stdint.h>

#include "LapSensor.h"

class LapSensorWired:public LapSensor{
public:
    LapSensorWired(std::string nm):LapSensor(nm){};
    static void init(int gpio);
    static void deinit(void);
    static void tick(void);     /* 割り込み */
    void task(void);
    bool isConnect(void){return true;};
private:
    static uint8_t state;
    static uint32_t tick_time_ms;
    static uint32_t tick_last;
    static int pin;
};


extern LapSensorWired lapsensor_wired;
