#pragma once

#include <stdint.h>

#include "LapSensor.h"

class LapSensorCan:public LapSensor{
public:
    LapSensorCan(std::string nm):LapSensor(nm){};
    void task(void);
    bool isConnect(void){return true;};
private:
    static uint16_t tick_num;
    static uint32_t tick_time;
};


extern LapSensorCan lapsensor_can;
