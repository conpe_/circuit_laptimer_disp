#pragma once

#include <stdint.h>

#include "LapSensor.h"
#include "CmdSrvDl.h"

/* ラップタイム通知 */
class LapSensorBt:public CmdProc, public LapSensor{
public:
    LapSensorBt(std::string nm):LapSensor(nm){};
    void task(void){ /* none */ };
    uint8_t CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len);
    bool isConnect(void);
};

extern LapSensorBt lapsensor_bt;
