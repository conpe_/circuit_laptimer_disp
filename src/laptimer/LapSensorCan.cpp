
#include "LapSensorCan.h"
#include "LapTimer.h"
#include "mdlCanDataConfig_B0.h"

LapSensorCan lapsensor_can("CAN");

uint32_t LapSensorCan::tick_time = stMdlCan_B0_RxVal.TickTime_ms;
uint16_t LapSensorCan::tick_num = stMdlCan_B0_RxVal.TickNum;

void LapSensorCan::task(void){

    /* 値が変化したらtick */
    if((tick_time != stMdlCan_B0_RxVal.TickTime_ms) || (tick_num != stMdlCan_B0_RxVal.TickNum)){
        tick_time = stMdlCan_B0_RxVal.TickTime_ms;
        tick_num = stMdlCan_B0_RxVal.TickNum;

        log_i("LapSensorCan ticked at %d.", tick_time);
        lap_timer.tick(tick_time);

    }
    
}

