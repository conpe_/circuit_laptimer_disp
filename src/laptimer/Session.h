#pragma once

#include <Arduino.h>
#include <vector>
#include "time.h"
#include "FS.h"
//#include "SPIFFS.h"

#define DEFAULT_LAPS_CAP 50     /* デフォルトで確保するメモリ量(周回数) */


class Session{
public:
    Session(void):SectorNum(0){};
    Session(tm* start_time, uint8_t sector_num);

    virtual ~Session(void);
    void clear(void);
    bool isempty(void);

    bool tick(uint32_t time_ms);     /* 検知 (セッションの開始時刻を0とした時間) */
    uint32_t passedTime(void);       /* セッション始まってからの時間 [ms] */

    void sessionInfo(tm* start_time=nullptr, uint8_t* sector_num=nullptr, uint8_t* laps=nullptr);             /* セッション情報の取得 */
    uint8_t lapInfo(int16_t lap, uint32_t* lap_time, std::vector<uint32_t>* sector_time = nullptr);     /* 特定ラップの情報を取得 lap=0で最新ラップ, lap>0で指定ラップ、lap<0でlap周前のラップ */
    uint8_t bestLapInfo(uint32_t* best_time, std::vector<uint32_t>* best_sector_time = nullptr);

    uint8_t saveFile(File *fp, bool append = false);
    uint8_t loadFile(File *fp);

    static const uint8_t MAX_LAPS = 255;    /* 1セッションの最大周回数 */

private:
    uint32_t StartTimeLocal;    /* セッション開始時刻(内部時間)[ms] */
    tm StartTime;       /* セッション開始時刻 */
    uint8_t SectorNum;  /* セクタ数 */
    std::vector<std::vector<uint32_t>> Laps;   /* 検知時刻 4byte * セクタ数 * ラップ数 */

    std::vector<uint32_t> CurrentLap;           /* 現ラップのセクタごとの検知時刻 */
    
};
