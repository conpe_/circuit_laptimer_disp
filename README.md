# circuit_laptimer_disp

サーキットラップタイマの表示器側ソフト。  
M5Paperに表示する版。  
計測値はCANに流す。

## 概要

* センサ側と組み合わせて使います。
* 99周までのラップタイムを計測します。
* 設定により，セクターが別れているサーキットにも対応。
* センサとはCANまたはBluetoothシリアル(SPP)でやり取りします。


## 機能

* 計測ラインを超えたら計測開始
* 中央ボタンで停止
* ピット入ったらピット時間を表示  
K-TAI用
* 計測データはSDカードに保存される
  * ラップタイム
  * GPS位置、速度情報
  * エンジン回転数
  * アクセル、ブレーキ操作量

## 電源
5V 140mA (実測)


## 環境
* M5Paperを使用
* Arduino環境
  * m5stack-core-esp32 Ver.0.4.3  
https://github.com/m5stack/M5Stack
  * arduino-esp32 Ver.2.06  
https://github.com/espressif/arduino-esp32

